PasswordHandler
===============

A collection of classes related to the handling of passwords in PHP 7+.


Introduction
------------

Pardon the explitive, but password management in the PHP web application
ecosystem is a fucking mess. Recent versions of PHP have the tools that are
needed for better password management but popular web applications often do not
use them because they value compatibility with older versions of PHP more than
they value a clean, easy to maintain and update password management system.

Web applications that do use the modern tools often use the default parameters
which quite frankly suck and should not be deemed secure. It looks like the PHP
core developers may be beginning to address that, for example the default
parameters for Argon2 in PHP 7.4 will be *much much* improved, but it will take
__years__ before PHP 7.4 reaches the enterprise Linux distributions that many
web applications run on.

Part of the problem is human nature. Password handling is largely invisible to
both the end user and the application developer alike. It just is not something
they think about, and often it is not something they care about. It can be hard
to understand the concepts involved, so the web developers just stick with the
PHP defaults in their web applications and then webmasters just stick with the
web application defaults.

My somewhat older CPU *(Intel(R) Xeon(R) CPU E3-1231 v3 @ 3.40GHz - launched in
2014)* can bcrypt hash 1000 passwords in just under a minute using the PHP 7.3
default parameters and can Argon2 hash 1000 passwords in __just under 2 seconds__
using the PHP 7.3 default parameters.

Both are way too fast, the default parameters suck.

Originally I started on better password handling as part of a fork of WordPress
that I am working on, but this is too important to keep it to that fork.

The goal of this project is to provide a unified set of classes that any web
application running PHP 7 can use to both ease the transition of outdated and
often custom and sometimes insecure password handling to modern, safe, and
secure password handling methods - as well as providing high quality default
parameters that increase in computational difficulty as a function of time.

When the project is done I *may* attempt to polyfill support for PHP 5.6. For
example, the `random_bytes()` function is PHP 7+ but an API compatible function
can be defined for earlier versions of PHP that is also a cryptography quality
pRNG. However no such PHP 5.5 or 5.6 compatibility will be attempted until the
project is finished, as PHP 5.6 is deprecated. Some Linux distributions do
still backport security fixes (e.g. RHEL / CentOS 7). Anyone using PHP 5.5/5.6
or PHP 7.0 that is *not* maintained by a *skilled* third party should upgrade
immediately, security fixes for those releases are not provided by the PHP
developers.

An example polyfill that *could* be used in PHP 5.5.x for `hash_equals`:
[christianfutterlieb/hash_equals.php](https://gist.github.com/christianfutterlieb/3cf85bc3fe16c70c0442)


Classes (not yet complete)
--------------------------

Which of these classes a web application uses is of course completely up to the
web application developer. For example, using the __PsfScrypt__ class does not
require that you use the (not yet started) __2FA__ implementation here instead
of your existing (or non-existent) __2FA__ solution.

* __CoreWrapper__ — A drop in replacement for the PHP core password functions
  described in the
  [PHP Manual Password Chapter](https://www.php.net/manual/en/book.password.php).
  The idea is that existing web applications can migrate to the password
  handling here by just inserting `$CW->` in front of the function name
  *e.g. `$CW->password_hash($password)`* and immediately benefit from the
  improved parameters.
* __PsfScrypt__ — A class that implements password hashing via
  [scrypt](https://en.wikipedia.org/wiki/Scrypt). Passwords can be hashed and
  validated using the __CoreWrapper__ class as if __PsfScrypt__ was a standard
  PHP password hashing algorithm.
* __PsfBcrypt__ — A class that implements blowfish password hashing without
  truncating passwords longer than 72 characters. This is a good solution if
  you do not know if Argon or Scrypt will be available but you do not want the
  effective 72 character limit of standard bcrypt.
* __PasswordHandler__ — A class that acts as a bridge between web application
  code and the __CoreWrapper__ class. For example, when used to authenticate a
  user, it will return a freshly generated hash that can be inserted into the
  database if the previous hash was created using an algorithm or parameters
  that are now outdated.
* __StrengthValidator__ — A class that can be used to determine passwords that
  are too weak to be allowed for use.
* __Mnemonic Password Generators__ A collection of classes that can be used
  to generate passwords that are easier for users to remember.
* __2FA__ — Work on this has barely been started, with zero testing of the
  actual code. The plan is to offer easy integration with several common 2FA
  providers that use *proper* 2FA rather than SMS, and at integration with at
  least one SMS service that can either act as an optional backup for when the
  proper 2FA service is down, or for users who do not want to install an app
  for 2FA. Generally speaking, 2FA via SMS should be avoided, but for
  non-financial applications, it is better than nothing.
* __Secure Password Reset__ — No work on this has been done. This will be a
  means by which users can submit a GnuPG or S/MIME key so that when the user
  needs to reset their password, the temporary token used for resetting the
  password can be sent to them with end-to-end encryption, avoiding the issues
  of SMTP which does not guarantee encrypted transmission of the message body
  between every hop.
* __User Authentication Database__ — Most web applications use a single
  database with a single database user for everything, which opens up the
  password hashes to any SQL Injection vulnerability. This will provide code
  for a database table that can either be used with the same database as the
  web application or preferably with a different database than the web
  application that uses a different SQL user so that SQL vulnerabilities in the
  web application (including plugins) do not expose the database hashes to a
  remote dump vulnerability.


Important Password Hashing Concepts
===================================

Most cryptography hash functions are designed to be wicked fast. For encryption
the hashes are used to *authenticate* the message, and if the hash algorithm is
slow, authentication of the message is slow and the encryption is slow and the
users get pissed off and complain.

Password hash functions need to have some of the same characteristics as
cryptography hash functions, such as no known hash collisions ever having been
found and mathematical resistance to hash collision attacks, but unlike hash
algorithms used for cryptography, password hashes need to be computationally
slow. This is because they need to resist brute force attacks even when the
password is weak.

Password hash functions need the ability to adjust the computational difficulty
as a function of time, allowing the computational difficulty needed to hash a
password to increase along with the exponential increase in computing power
that is available to those who try to crack passwords.

Password hash functions need to be resistant to GPU / ASIC attacks.

Password hash functions need to be resistant to side-channel attacks.

Implementations of password hashing functions need to use a unique salt for
every password they create a hash for. The salt should be generated by a
cryptography quality pseudo-Random Number Generator so that the same salt will
never be used with more than password. To that end, the random number should be
at a bare *minimum* 8 bytes, 12 bytes is better for a minimum, 16 bytes is what
most quality password hashing algorithms use. The reason for a unique salt for
every password hashed is to both defend against hash pre-computation attacks
(also known as ‘Rainbow Table’ attacks) and to prevent an attacker from
determining that two or more users have the same password, which is an
indication those particular users are likely using one of the commonly used
passwords the attacker already has in their attack dictionary.

Implementations of password hashing algorithms should allow for variable-length
salts and when the algorithm can produce variable length hashes, the
implementation should accommodate variable-length hashes.

Implementations of password hashing algorithms should normalize the user
submitted password to the printable 7-bit ASCII character set before passing
the password off to the actual hashing algorithm. This is easily accomplished
by using a fast cryptographic hash function (such as SHA-384) to do an initial
hash of the user password and then base64 encoding that initial hash before
sending the password to the actual password hashing algorithm. To understand
why this is necessary, all the variations of the blowfish `crypt()` hashing
algorithms (`2`, `2a`, `2x`, `2y`, `2b`) exist because of bugs in implementing
the blowfish algorithm with either very long passwords, password that contained
control characters (like a NULL byte), or passwords that contained multi-byte
characters beyond the ASCII range. All those variations of blowfish would
consistently produce the same result if the passwords were normalized to
printable 7-bit ASCII before sending them to the blowfish implementation, it
is the corner cases where issues were found, corner cases that would not be an
issue with password normalization, e.g.:

    protected static function normalizePassword(string &$password, string $salt): void
    {
        if (empty($password)) {
            throw new \InvalidArgumentException('The password can not be empty.');
        }
        if (empty($salt)) {
            throw new \InvalidArgumentException('The salt can not be empty.');
        }
        $bindata = hash_hmac('sha384', hash('sha256', $salt, true), hash('sha256', $password, true), true);
        $password = base64_encode($bindata);
    }//end normalizePassword()

Implementations of password hashing algorithms should use
[PHC String Format](https://github.com/P-H-C/phc-string-format/blob/master/phc-sf-spec.md) (PSF)
or it’s predecessor,
[Modular Crypt Format](https://passlib.readthedocs.io/en/stable/modular_crypt_format.html) (MCF).
This is so that it is easy for code that works with multiple different password
hashing algorithms to identify the algorithm and parse the string containing
the salt and parameters needed to validate a password against the hash.


 
A Brief History of Passwords in PHP
===================================

Functions to properly deal with password hashing and validation did not come to
PHP until PHP 5.5.0.

Prior to that version of PHP, web applications had to either use the
[crypt()](https://www.php.net/manual/en/function.crypt.php) function or use a
solution provided by scripts, such as [phpass](https://www.openwall.com/phpass/).

To make matters worse, cryptography quality hash functions were not always
available. If you used the previously mentioned `crypt()` function for handling
passwords in your web application, what algorithms were available varied
greatly from system to system to system.

I take my hat off for OpenWall and the phpass script they provided, they used
blowfish when the server supported it and used a custom `MD5` solution when
blowfish was not available. Today we cringe at `MD5` but at the time, it was
the only cryptography quality hash algorithm that was widely available on every
server that used PHP, and the way they created hashes with `MD5` was better
than many of the solutions, including the `crypt()` implementation of `MD5`.

With PHP 5.5 we finally got a good native solution in PHP with the
`password_hash()` and `password_verify()` functions, but it is difficult for
many platforms to switch to them because of their limited coverage.

With `password_hash()` the only algorithm you can be sure exists on every
server out there is `bcrypt` (an implementation of blowfish) and where the
real problem lies is with `password_verify()`. That function only validates
password hashes generated with `password_hash()` or `crypt()`, it can not be
used to validate hashes created with the previously mentioned phpass script.

Indeed that is a large part of the motivation behind the CoreWrapper class that
I provide here. It supports at least *some* of the other commonly used but now
outdated password hashing methods, making it easier to transition to more
modern password hashing methods.


Bit Rot and Technical Debt
--------------------------

Bit Rot is defined as software that is no longer suitable for the original
intended purpose due to the passage of time changing the landscape that the
software was designed for.

Technical Debt is defined as long term development needed to deal with issues
that arise from the use of software solutions that are easy to implement but
are not the best technical solution to the actual need.

Both of these issues plague password handling solution is PHP web applications.

Initially, Drupal (and WordPress and many others) used a single round unsalted
`MD5` hash for their passwords. Those applications were originally developed
back when PHP just did not have the necessary functions to allow for consistent
password handling that worked on every server.

With Drupal 7 they switched to an in-house modified version of phpass. They
were right to recognize that phpass suffered bit rot, it used `MD5` which was
suitable when phpass was developed but was no longer a good solution.

Drupal modified phpass to use `SHA-512` instead of `MD5`. The technically
better solution would have been for them to require blowfish support for
Drupal 7, then they could have used `crypt()` to create proper password
hashes, but they wanted *(I speculate, I was not involved in the decision)* to
keep compatibility with builds of PHP that did not have support for blowfish.

By taking phpass and modifying it to use `SHA-512` they were able to solve the
present need of replacing their use of a raw single-round `MD5` hash, but the
result was a custom password hash that is not supported by the password
handling capabilities that are now present in PHP.

They avoided complaints from users, but at the cost of technical debt. Had they
required blowfish, the better solution at the time, they could easily have
switched their password handling to `password_validate()` and `password_hash()`
by now, which would have easily facilitated switching to even better password
hashing algorithms like Argon2id.

Their technical debt decision has thus resulted in Drupal 7 now suffering
bit rot as it still uses an `SHA-512` based solution that is very susceptible
to GPU/ASIC cracking.

This project intends to make it easy for software projects like drupal to deal
with their technical debt and bit rot and modernize their password handling to
the needs of today.

Even if the CoreWrapper class does not support the custom password technique a
particular software project uses, it should not be too difficult for the
developers of that project to extend CoreWrapper to include their bit rot
algorithms in what the `$CW->password_verify()` wrapper supports.


The Sad Sad State of PHP Computational Difficulty Defaults
----------------------------------------------------------

Even though as of PHP 5.5, PHP now has built-in functions that make it easier
for web applications to properly deal with passwords, their defaults just plain
suck and in the case of bcrypt, suffer from bitrot.

While their default for bcrypt was decent when they first implemented it, their
default neither adjust for the current year, nor have they updated it in new
releases of PHP.

Most calculate that today in 2019, bcrypt should use a cost factor of 13 or 14.
The PHP default is 10. Keeping in mind that increasing the cost factor by one
*doubles* the cost, that means the PHP defaults are either one eighth what they
should be, or one sixteenth.

In PHP 7.2 and 7.3 the Argon2i/Argon2id defaults are just absolutely abysmal,
far worse than their bcrypt defaults. Fortunately they will be improving that
in PHP 7.4, increasing the default memory cost from 1024 kibibytes to a much
better 262144 kibibytes.

Unfortunately they are reducing the parallelism (threads) to 1 for
compatibility with the libsodium implementation, I feel it is a mistake to make
the default weaker to maintain compatibility with an alternate implementation
but it *probably* is not the end of the world. They are increasing the time
cost to 3.

Fixing the Argon2 defaults in PHP 7.4 however will take a long time to filter
down to deployed servers.

This class allows better defaults now.


Hopes and Dreams
================

It is my hope that the CoreWrapper class and all of the PasswordHandler classes
become a standard component in web applications across the globe, making it
much easier for webmasters and web applications to increase the security of
their users when the password hashes are stolen, and they will be, so that the
web and the world can be a better place.

I am very poor. There are days when I do not eat, there are months when I have
to continue wearing shoes I should have replaced months before.

Due a combination of uncontrolled epilepsy, poverty preventing me from getting
the neurological help I need, social anxiety fueled by autism, and a general
poor look to how I dress - wearing clothes most people would have tossed years
before they became this worn - employment is becoming tougher and tougher to
find especially since much of the kind of work that can be done from home on a
computer is now either outsources overseas or is done by cloud services that
are full of privacy invasive trackers.

This attempt to make it easier for those with corporate sponsors behind their
web applications to update their password handling is my way of trying to make
the world a better place, even though many of those who will benefit look down
on me as a loser.

If you can help make the world a better place for me, I would greatly
appreciate it. Donations and gift cards can be sent to paypal@domblogger.net.

















