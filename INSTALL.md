INSTALL
=======

This collection is not yet production quality.

It needs some classes added to it, and it needs more unit tests.

To play with it now, just clone the gitlab repository.

Eventually I plan to have a composer package but there are some issues with
composer I need to figure out:

The unit tests require some extensions that are optional for using the class.

Specifically, the Sodium extension is needed to test hashing / validation of
passwords on systems that do not have PHP compiled against libargon2 but do
have the sodium extension installed.

Systems that either have PHP compiled against libargon2 or that have no
interest in Argon2 support do not need the sodium extension.

Secondly, the PECL Scrypt extension is needed to test hashing / validation of
passwords using scrypt.

Systems that have no interest in scrypt support do not need to have that PECL
module installed.

They are needed for a devel install because unit tests need them, but those two
extensions are not to use the classes here for those who do not want or need
the functionallity they provide.

I do not understand composer very well, it seems to prevent install on systems
that do not have those modules *even when I do not request a development
install* which is absurd. They are binary modules, they can not be installed
with composer, and even if they could it would be wrong to force them on users
that do not want them.

So... hopefully I'll have that figured out before too long so composer can
install this package.