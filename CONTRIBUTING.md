Contributing
============

If you wish to contribute, pull requests are acceptable but please make sure
the coding standards are followed.

Nutshell: PSR-2 with a few additions:

* `declare(strict_types=1);`
* closing `?>` at end of file
* A few additional stuff from PEAR etc. mostly related to phpdoc

If you do a git clone, you can run `composer install` and it will install
`phpcs` within the `vendor/` directory which you can use to check against the
`phpcs.xml` file for compliance.

The ruleset defined there will not enforce the `strict_types` or closing `?>`
but it will catch most of the stuff I care about.

Also, please use vimeo/psalm to check for issues, which is also installed
when you run `composer install`.

Finally, check with `phpunit` to make sure none of the (currently non-existant)
unit tests break as a result of the changes.

Pull requests should be made against the devel branch which, admittedly has not
yet been created.

If you would rather not do all that, open up an issue or e-mail me, I am very
flexible.
