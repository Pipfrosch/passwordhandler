<?php
declare(strict_types=1);

/**
 * Unit testing for \Pipfrosch\PasswordHandler\CoreWrapper
 *
 * @package    PasswordHandler
 * @subpackage UnitTests
 * @author     Alice Wonder <paypal@domblogger.net>
 * @license    https://opensource.org/licenses/MIT MIT
 * @link       https://gitlab.com/Pipfrosch/swpress
 */

use PHPUnit\Framework\TestCase;
use \Pipfrosch\PasswordHandler\PsfScrypt as PsfScrypt;
use \Pipfrosch\PasswordHandler\PsfBcrypt as PsfBcrypt;
use \Pipfrosch\PasswordHandler\CoreWrapper as CoreWrapper;
use \Pipfrosch\PasswordHandler\Reference\ScryptPassword as ScryptRef;

/**
 * Test class for \Pipfrosch\PasswordHandler
 */
// @codingStandardsIgnoreLine
final class CoreWrapperTests extends TestCase
{
    /**
     * Test validation of standard DES
     *
     * @return void
     */
    public function testStandardDesHashValidation(): void
    {
        $alphabet = '1234567890./qwertyuiopasdfghjklzxcvbnmQAZWSXEDCRFVTGBYHNUJMIKOLP';
        $CW = new CoreWrapper();
        for ($i=0; $i<10; $i++) {
            $salt = '';
            $rnd = random_int(0, 63);
            $salt .= $alphabet[$rnd];
            $rnd = random_int(0, 63);
            $salt .= $alphabet[$rnd];
            $bin = random_bytes(8);
            $password = base64_encode($bin);
            $hash = crypt($password, $salt);
            $actual = strlen($hash);
            $expected = 13;
            $this->assertSame($expected, $actual);
            $rs = $CW->password_verify($password, $hash);
            $this->assertTrue($rs);
            $rs = $CW->password_verify('invalid', $hash);
            $this->assertFalse($rs);
        }
    }//end testStandardDesHashValidation()

    /**
     * Test validation of extended DES
     *
     * @return void
     */
    public function testExtendedDesHashValidation(): void
    {
        $alphabet = '1234567890./qwertyuiopasdfghjklzxcvbnmQAZWSXEDCRFVTGBYHNUJMIKOLP';
        $CW = new CoreWrapper();
        for ($i=0; $i<10; $i++) {
            $salt = '_J9..';
            $rnd = random_int(0, 63);
            $salt .= $alphabet[$rnd];
            $rnd = random_int(0, 63);
            $salt .= $alphabet[$rnd];
            $rnd = random_int(0, 63);
            $salt .= $alphabet[$rnd];
            $rnd = random_int(0, 63);
            $salt .= $alphabet[$rnd];
            $bin = random_bytes(8);
            $password = base64_encode($bin);
            $hash = crypt($password, $salt);
            $actual = strlen($hash);
            $expected = 20;
            $this->assertSame($expected, $actual);
            $rs = $CW->password_verify($password, $hash);
            $this->assertTrue($rs);
            $rs = $CW->password_verify('invalid', $hash);
            $this->assertFalse($rs);
        }
    }//end testExtendedDesHashValidation()

    /**
     * Test validation of crypt md5
     *
     * @return void
     */
    public function testCryptMdfiveHashValidation(): void
    {
        $alphabet = '1234567890./qwertyuiopasdfghjklzxcvbnmQAZWSXEDCRFVTGBYHNUJMIKOLP';
        $CW = new CoreWrapper();
        for ($i=0; $i<10; $i++) {
            $salt = '$1$';
            for ($j=0; $j<9; $j++) {
                $rnd = random_int(0, 63);
                $salt .= $alphabet[$rnd];
            }
            $bin = random_bytes(8);
            $password = base64_encode($bin);
            $hash = crypt($password, $salt);
            $actual = strlen($hash);
            $expected = 34;
            $this->assertSame($expected, $actual);
            $rs = $CW->password_verify($password, $hash);
            $this->assertTrue($rs);
            $rs = $CW->password_verify('invalid', $hash);
            $this->assertFalse($rs);
        }
    }//end testCryptMdfiveHashValidation()

    /**
     * Test validation of crypt blowfish
     *
     * @return void
     */
    public function testCryptBlowfishHashValidation(): void
    {
        $alphabet = '1234567890qwertyuiopasdfghjklzxcvbnmQAZWSXEDCRFVTGBYHNUJMIKOLP';
        $CW = new CoreWrapper();
        for ($i=0; $i<10; $i++) {
            $salt = '$2a$06$';
            for ($j=0; $j<22; $j++) {
                $rnd = random_int(0, 61);
                $salt .= $alphabet[$rnd];
            }
            $bin = random_bytes(8);
            $password = base64_encode($bin);
            $hash = crypt($password, $salt);
            $actual = strlen($hash);
            $expected = 60;
            $this->assertSame($expected, $actual);
            $rs = $CW->password_verify($password, $hash);
            $this->assertTrue($rs);
            $rs = $CW->password_verify('invalid', $hash);
            $this->assertFalse($rs);
        }
    }//end testCryptBlowfishHashValidation()

    /**
     * Test validation of crypt sha-256
     *
     * @return void
     */
    public function testCryptShaTwoFiftySixHashValidation(): void
    {
        $alphabet = '1234567890qwertyuiopasdfghjklzxcvbnmQAZWSXEDCRFVTGBYHNUJMIKOLP';
        $CW = new CoreWrapper();
        for ($i=0; $i<10; $i++) {
            $salt = '$5$rounds=5000$';
            for ($j=0; $j<16; $j++) {
                $rnd = random_int(0, 61);
                $salt .= $alphabet[$rnd];
            }
            $bin = random_bytes(8);
            $password = base64_encode($bin);
            $hash = crypt($password, $salt);
            $actual = strlen($hash);
            $expected = 75;
            $this->assertSame($expected, $actual);
            $rs = $CW->password_verify($password, $hash);
            $this->assertTrue($rs);
            $rs = $CW->password_verify('invalid', $hash);
            $this->assertFalse($rs);
        }
    }//end testCryptShaTwoFiftySixHashValidation()

    /**
     * Test validation of crypt sha-256
     *
     * @return void
     */
    public function testCryptShaFiveTwelveHashValidation(): void
    {
        $alphabet = '1234567890qwertyuiopasdfghjklzxcvbnmQAZWSXEDCRFVTGBYHNUJMIKOLP';
        $CW = new CoreWrapper();
        for ($i=0; $i<10; $i++) {
            $salt = '$6$rounds=5000$';
            for ($j=0; $j<16; $j++) {
                $rnd = random_int(0, 61);
                $salt .= $alphabet[$rnd];
            }
            $bin = random_bytes(8);
            $password = base64_encode($bin);
            $hash = crypt($password, $salt);
            $actual = strlen($hash);
            $expected = 118;
            $this->assertSame($expected, $actual);
            $rs = $CW->password_verify($password, $hash);
            $this->assertTrue($rs);
            $rs = $CW->password_verify('invalid', $hash);
            $this->assertFalse($rs);
        }
    }//end testCryptShaFiveTwelveHashValidation()

    /**
     * Test validation of plain md5
     *
     * @psalm-suppress PossiblyNullArgument
     *
     * @return void
     */
    public function testPlainMdfiveHashValidation(): void
    {
        $CW = new CoreWrapper();
        for ($i=0; $i<10; $i++) {
            $bin = random_bytes(8);
            $password = base64_encode($bin);
            $hash = md5($password);
            $rs = $CW->password_verify($password, $hash);
            $this->assertTrue($rs);
            $rs = $CW->password_verify('invalid', $hash);
            $this->assertFalse($rs);
            $hash = hash('ripemd128', $password, false);
            $rs = $CW->password_verify($password, $hash);
            $this->assertTrue($rs);
            $rs = $CW->password_verify('invalid', $hash);
            $this->assertFalse($rs);
            // base64 encoded hash
            $bin = hash('md5', $password, true);
            $hash = base64_encode($bin);
            $rs = $CW->password_verify($password, $hash);
            $this->assertTrue($rs);
            $bin = hash('ripemd128', $password, true);
            $hash = base64_encode($bin);
            $rs = $CW->password_verify($password, $hash);
            $this->assertTrue($rs);
        }
    }//end testPlainMdfiveHashValidation()

    /**
     * Test validation of plain sha1
     *
     * @psalm-suppress PossiblyNullArgument
     *
     * @return void
     */
    public function testPlainShaOne(): void
    {
        $CW = new CoreWrapper();
        for ($i=0; $i<10; $i++) {
            $bin = random_bytes(8);
            $password = base64_encode($bin);
            $hash = hash('sha1', $password, false);
            $rs = $CW->password_verify($password, $hash);
            $this->assertTrue($rs);
            $rs = $CW->password_verify('invalid', $hash);
            $this->assertFalse($rs);
            $hash = hash('ripemd160', $password, false);
            $rs = $CW->password_verify($password, $hash);
            $this->assertTrue($rs);
            $rs = $CW->password_verify('invalid', $hash);
            $this->assertFalse($rs);
            // base64 encoded hash
            $bin = hash('sha1', $password, true);
            $hash = base64_encode($bin);
            $rs = $CW->password_verify($password, $hash);
            $this->assertTrue($rs);
            $bin = hash('ripemd160', $password, true);
            $hash = base64_encode($bin);
            $rs = $CW->password_verify($password, $hash);
            $this->assertTrue($rs);
        }
    }//end testPlainShaOne()

    /**
     * Test validation of plain sha256
     *
     * @psalm-suppress PossiblyNullArgument
     *
     * @return void
     */
    public function testPlainShaTwoFiftysix(): void
    {
        $CW = new CoreWrapper();
        for ($i=0; $i<10; $i++) {
            $bin = random_bytes(8);
            $password = base64_encode($bin);
            $hash = hash('sha256', $password, false);
            $rs = $CW->password_verify($password, $hash);
            $this->assertTrue($rs);
            $rs = $CW->password_verify('invalid', $hash);
            $this->assertFalse($rs);
            $hash = hash('ripemd256', $password, false);
            $rs = $CW->password_verify($password, $hash);
            $this->assertTrue($rs);
            $rs = $CW->password_verify('invalid', $hash);
            $this->assertFalse($rs);
            // base64 encoded hash
            $bin = hash('sha256', $password, true);
            $hash = base64_encode($bin);
            $rs = $CW->password_verify($password, $hash);
            $this->assertTrue($rs);
            $bin = hash('ripemd256', $password, true);
            $hash = base64_encode($bin);
            $rs = $CW->password_verify($password, $hash);
            $this->assertTrue($rs);
        }
    }//end testPlainShaTwoFiftysix()

    /**
     * Test validation of plain sha384
     *
     * @psalm-suppress PossiblyNullArgument
     *
     * @return void
     */
    public function testPlainShaThreeEightyFour(): void
    {
        $CW = new CoreWrapper();
        for ($i=0; $i<10; $i++) {
            $bin = random_bytes(8);
            $password = base64_encode($bin);
            $hash = hash('sha384', $password, false);
            $rs = $CW->password_verify($password, $hash);
            $this->assertTrue($rs);
            $rs = $CW->password_verify('invalid', $hash);
            $this->assertFalse($rs);
            // base64 encoded hash
            $bin = hash('sha384', $password, true);
            $hash = base64_encode($bin);
            $rs = $CW->password_verify($password, $hash);
            $this->assertTrue($rs);
        }
    }//end testPlainShaThreeEightyFour()

    /**
     * Test validation of plain sha512
     *
     * @psalm-suppress PossiblyNullArgument
     *
     * @return void
     */
    public function testPlainShaFiveTwelve(): void
    {
        $CW = new CoreWrapper();
        for ($i=0; $i<10; $i++) {
            $bin = random_bytes(8);
            $password = base64_encode($bin);
            $hash = hash('sha512', $password, false);
            $rs = $CW->password_verify($password, $hash);
            $this->assertTrue($rs);
            $rs = $CW->password_verify('invalid', $hash);
            $this->assertFalse($rs);
            // base64 encoded hash
            $bin = hash('sha512', $password, true);
            $hash = base64_encode($bin);
            $rs = $CW->password_verify($password, $hash);
            $this->assertTrue($rs);
        }
    }//end testPlainShaFiveTwelve()

    /**
     * Test validation of plain ripemd320
     *
     * @psalm-suppress PossiblyNullArgument
     *
     * @return void
     */
    public function testPlainRipeMdThreeTwenty(): void
    {
        $CW = new CoreWrapper();
        for ($i=0; $i<10; $i++) {
            $bin = random_bytes(8);
            $password = base64_encode($bin);
            $hash = hash('ripemd320', $password, false);
            $rs = $CW->password_verify($password, $hash);
            $this->assertTrue($rs);
            $rs = $CW->password_verify('invalid', $hash);
            $this->assertFalse($rs);
            // base64 encoded hash
            $bin = hash('ripemd320', $password, true);
            $hash = base64_encode($bin);
            $rs = $CW->password_verify($password, $hash);
            $this->assertTrue($rs);
        }
    }//end testPlainRipeMdThreeTwenty()

    /**
     * Test Reference Scrypt hashes
     *
     * @psalm-suppress PossiblyNullArgument
     *
     * @return void
     */
    public function testScryptReferenceVerify(): void
    {
        $CW = new CoreWrapper();
        for ($i=0; $i<12; $i++) {
            $bin = random_bytes(8);
            $password = base64_encode($bin);
            $rem = $i % 4;
            switch ($rem) {
                case 0:
                    $hashString = ScryptRef::hash($password);
                    break;
                case 1:
                    $hashString = ScryptRef::hash($password);
                    $hashString = 'scrypt:' . $hashString;
                    break;
                case 2:
                    $hashString = ScryptRef::hash($password);
                    $arr = explode('$', $hashString);
                    $bin = hex2bin($arr[4]);
                    $arr[4] = base64_encode($bin);
                    $hashString = implode('$', $arr);
                    break;
                default:
                    $hashString = ScryptRef::hash($password);
                    $arr = explode('$', $hashString);
                    $bin = hex2bin($arr[4]);
                    $arr[4] = base64_encode($bin);
                    $hashString = 'scrypt:' . implode('$', $arr);
            }
            $rs = $CW->password_verify($password, $hashString);
            $this->assertTrue($rs);
            $rs = $CW->password_verify('invalid', $hashString);
            $this->assertFalse($rs);
        }
    }//end testScryptReferenceVerify()

    /**
     * Test validation of bcrypt
     *
     * @psalm-suppress PossiblyNullArgument
     *
     * @return void
     */
    public function testBcryptHashValidation(): void
    {
        $CW = new CoreWrapper();
        for ($i=0; $i<10; $i++) {
            $bin = random_bytes(8);
            $password = base64_encode($bin);
            $hash = password_hash($password, 1);
            $expected = '$2y$';
            $actual = substr($hash, 0, 4);
            $this->assertSame($expected, $actual);
            $rs = $CW->password_verify($password, $hash);
            $this->assertTrue($rs);
            $rs = $CW->password_verify('invalid', $hash);
            $this->assertFalse($rs);
        }
    }//end testBcryptHashValidation()

    /**
     * Test validation of Argon2i
     *
     * @psalm-suppress PossiblyNullArgument
     *
     * @return void
     */
    public function testArgonTwoEyeValidation(): void
    {
        $CW = new CoreWrapper();
        for ($i=0; $i<10; $i++) {
            $bin = random_bytes(8);
            $password = base64_encode($bin);
            $hash = password_hash($password, 2);
            $expected = '$argon2i$';
            $actual = substr($hash, 0, 9);
            $this->assertSame($expected, $actual);
            $rs = $CW->password_verify($password, $hash);
            $this->assertTrue($rs);
            $rs = $CW->password_verify('invalid', $hash);
            $this->assertFalse($rs);
        }
    }//end testArgonTwoEyeValidation()

    /**
     * Test validation of Argon2i
     *
     * @psalm-suppress PossiblyNullArgument
     *
     * @return void
     */
    public function testArgonTwoIdValidation(): void
    {
        $CW = new CoreWrapper();
        for ($i=0; $i<10; $i++) {
            $bin = random_bytes(8);
            $password = base64_encode($bin);
            $hash = password_hash($password, 3);
            $expected = '$argon2id$';
            $actual = substr($hash, 0, 10);
            $this->assertSame($expected, $actual);
            $rs = $CW->password_verify($password, $hash);
            $this->assertTrue($rs);
            $rs = $CW->password_verify('invalid', $hash);
            $this->assertFalse($rs);
        }
    }//end testArgonTwoIdValidation()

    /**
     * Test validation of phpass
     *
     * @return void
     */
    public function testPhpassValidation(): void
    {
        $CW = new CoreWrapper();
        $testArray = array(
            'TestPasswordOne'   => '$P$BuKc3O55ism8CQrIoIuYGnHW4sCJeP1',
            'TestPasswordTwo'   => '$P$BBTlADnA0rXP93AYONHAd1O1tLwRk..',
            'TestPasswordThree' => '$P$BJ.TKrVH6KqQT/BvOYs9KEMpyVcJ8v0',
            'TestPasswordFour'  => '$P$BPtrk.ewv1IDVrM1bJgyRorbXAeXHQ1',
            'TestPasswordFive'  => '$P$BcJo8R/COdakNqpwjId8T1bWyWKXyK1',
            'TestPasswordSix'   => '$P$B3ZD238gOjwttmw8y61Q0/8fJnyn4e.',
            'TestPasswordSeven' => '$P$BKv79ZXi6lZvjxFBT374LP5GAsOeyz.',
            'TestPasswordEight' => '$P$BXH1LqOLnjpd1AASfLJ66xUX4lOjkq/',
            'TestPasswordNine'  => '$P$B35qd6NGchDm1ZnRaw4PTWiGj0SGS3.',
            'TestPasswordTen'   => '$P$BtzOOKvF8S9pvsgybWe0OQQkMJ0xmy.',
        );
        foreach ($testArray as $password => $hash) {
            $rs = $CW->password_verify($password, $hash);
            $this->assertTrue($rs);
            $rs = $CW->password_verify('invalid', $hash);
            $this->assertFalse($rs);
        }
    }//end testPhpassValidation()

    /**
     * Test validation of phpass
     *
     * @return void
     */
    public function testDrupalSevenValidation(): void
    {
        $CW = new CoreWrapper();
        $testArray = array(
            'TestPasswordOne'   => '$S$5n.v.OcperYtYBXMsXf1N0kF0Tzb8DHZY8D7QiAMrZfdBOaz2GDe',
            'TestPasswordTwo'   => '$S$5ddEQXH.Qs.ovsb5nseGYXcUu1z1kt21xOQvslhlXuajL07xzJm9',
            'TestPasswordThree' => '$S$5xZcEUU5rlqkr1vIRQkdbfW3Xcnqolmc5m3nsvidcFu8PKlrIAqC',
            'TestPasswordFour'  => '$S$5ACXtxLVgZGZBI0MAqygqa.HTj/u13P5Sc6CvKirQvvggCQAe2se',
            'TestPasswordFive'  => '$S$5DK9hXItZIp/ecMdyDPE3mblL5QMIGTRKBpT1RXNPCSpSdj4WVoR',
            'TestPasswordSix'   => '$S$5Oc4Yo4Ap3lnIoGwKKTLsVq4h/unEWxm1mQj02T0NRc0SyjM4g6i',
            'TestPasswordSeven' => '$S$5x7aoc3zU7/FqsLHqoIJO4iRUt7xZe9XckePrfGNcByDaSXeMq.a',
            'TestPasswordEight' => '$S$54UXKVuCLhH4JH9V9fw96PYNBWuxqZDcJn.bLVyXYYUv1nRKaNTc',
            'TestPasswordNine'  => '$S$5vB7Zcau5ph0bpC57IHQaOGBz7GYGH7.2PUkMczscddGBmPaBekO',
            'TestPasswordTen'   => '$S$5b6XMaZLLUN/F0fmkJc5pff1M16CC3QkRBIqRFJ7cCeAdJSf3iXA',
        );
        foreach ($testArray as $password => $hash) {
            $rs = $CW->password_verify($password, $hash);
            $this->assertTrue($rs);
            $rs = $CW->password_verify('invalid', $hash);
            $this->assertFalse($rs);
        }
    }//end testDrupalSevenValidation()

    /**
     * Test Custom Scrypt validation
     *
     * @return void
     */
    public function testPsfScryptValidation(): void
    {
        $CW = new CoreWrapper();
        for ($i=0; $i<5; $i++) {
            $bin = random_bytes(8);
            $password = base64_encode($bin);
            $hash = PsfScrypt::passwordHasher($password);
            $rs = $CW->password_verify($password, $hash);
            $this->assertTrue($rs);
            $rs = $CW->password_verify('invalid', $hash);
            $this->assertFalse($rs);
        }
    }//end testPsfScryptValidation()

    /**
     * Test Custom Bcrypt validation
     *
     * @return void
     */
    public function testPsfBcryptValidation(): void
    {
        $CW = new CoreWrapper();
        for ($i=0; $i<10; $i++) {
            $bin = random_bytes(8);
            $password = base64_encode($bin);
            $hash = PsfBcrypt::passwordHasher($password, 0, array('cost' => 12));
            $rs = $CW->password_verify($password, $hash);
            $this->assertTrue($rs);
            $rs = $CW->password_verify('invalid', $hash);
            $this->assertFalse($rs);
        }
    }//end testPsfBcryptValidation()

    /**
     * Test Generating Valid Bcrypt
     *
     * @psalm-suppress PossiblyFalseArgument
     * @psalm-suppress PossiblyNullArgument
     *
     * @return void
     */
    public function testGenerateBcryptHash(): void
    {
        $CWA = new CoreWrapper();
        $CWB = new CoreWrapper(1);
        for ($i=0; $i<5; $i++) {
            $bin = random_bytes(8);
            $password = base64_encode($bin);
            $HashA = $CWA->password_hash($password, PASSWORD_BCRYPT);
            $expected = '$2y$14$';
            $actual = substr($HashA, 0, 7);
            $this->assertSame($expected, $actual);
            $rs = password_verify($password, $HashA);
            $this->assertTrue($rs);
            $rs = password_verify('invalid', $HashA);
            $this->assertFalse($rs);
            $HashB = $CWB->password_hash($password);
            $actual = substr($HashB, 0, 7);
            $this->assertSame($expected, $actual);
            $rs = password_verify($password, $HashB);
            $this->assertTrue($rs);
            $rs = password_verify('invalid', $HashB);
            $this->assertFalse($rs);
            $options = array('cost' => 8);
            $HashC = $CWA->password_hash($password, PASSWORD_BCRYPT, $options);
            $expected = '$2y$08$';
            $actual = substr($HashC, 0, 7);
            $this->assertSame($expected, $actual);
            $rs = password_verify($password, $HashC);
            $this->assertTrue($rs);
            $rs = password_verify('invalid', $HashC);
            $this->assertFalse($rs);
        }
    }//end testGenerateBcryptHash()

    /**
     * Test Generating Valid Argon2i
     *
     * @psalm-suppress PossiblyFalseArgument
     * @psalm-suppress PossiblyNullArgument
     *
     * @return void
     */
    public function testGenerateArgonTwoEye(): void
    {
        $CWA = new CoreWrapper();
        $CWB = new CoreWrapper(2, 1);
        for ($i=0; $i<5; $i++) {
            $bin = random_bytes(8);
            $password = base64_encode($bin);
            $HashA = $CWA->password_hash($password, PASSWORD_ARGON2I);
            $expected = '$argon2i$v=19$m=196608,t=4,p=3$';
            $actual = substr($HashA, 0, 31);
            $this->assertSame($expected, $actual);
            $rs = password_verify($password, $HashA);
            $this->assertTrue($rs);
            $rs = password_verify('invalid', $HashA);
            $this->assertFalse($rs);
            $HashB = $CWB->password_hash($password);
            $actual = substr($HashB, 0, 31);
            $expected = '$argon2i$v=19$m=262144,t=6,p=5$';
            $this->assertSame($expected, $actual);
            $rs = password_verify($password, $HashB);
            $this->assertTrue($rs);
            $rs = password_verify('invalid', $HashB);
            $this->assertFalse($rs);
            $options = array(
                'memory_cost' => 131072,
                'time_cost' => 2,
                'threads' => 2
            );
            $HashC = $CWA->password_hash($password, PASSWORD_ARGON2I, $options);
            $expected = '$argon2i$v=19$m=131072,t=2,p=2$';
            $actual = substr($HashC, 0, 31);
            $this->assertSame($expected, $actual);
            $rs = password_verify($password, $HashC);
            $this->assertTrue($rs);
            $rs = password_verify('invalid', $HashC);
            $this->assertFalse($rs);
        }
    }//end testGenerateArgonTwoEye()

    /**
     * Test Generating Valid Argon2id
     *
     * @psalm-suppress PossiblyFalseArgument
     * @psalm-suppress PossiblyNullArgument
     *
     * @return void
     */
    public function testGenerateArgonTwoId(): void
    {
        $CWA = new CoreWrapper();
        $CWB = new CoreWrapper(3, 2);
        for ($i=0; $i<5; $i++) {
            $bin = random_bytes(8);
            $password = base64_encode($bin);
            $HashA = $CWA->password_hash($password, PASSWORD_ARGON2ID);
            $expected = '$argon2id$v=19$m=196608,t=4,p=3$';
            $actual = substr($HashA, 0, 32);
            $this->assertSame($expected, $actual);
            $rs = password_verify($password, $HashA);
            $this->assertTrue($rs);
            $rs = password_verify('invalid', $HashA);
            $this->assertFalse($rs);
            $HashB = $CWB->password_hash($password);
            $expected = '$argon2id$v=19$m=327680,t=8,p=7$';
            $actual = substr($HashB, 0, 32);
            $this->assertSame($expected, $actual);
            $rs = password_verify($password, $HashB);
            $this->assertTrue($rs);
            $rs = password_verify('invalid', $HashB);
            $this->assertFalse($rs);
            $options = array(
                'memory_cost' => 131072,
                'time_cost' => 2,
                'threads' => 2
            );
            $HashC = $CWA->password_hash($password, PASSWORD_ARGON2ID, $options);
            $expected = '$argon2id$v=19$m=131072,t=2,p=2$';
            $actual = substr($HashC, 0, 32);
            $this->assertSame($expected, $actual);
            $rs = password_verify($password, $HashC);
            $this->assertTrue($rs);
            $rs = password_verify('invalid', $HashC);
            $this->assertFalse($rs);
        }
    }//end testGenerateArgonTwoId()

    /**
     * Test Generating Valid PIPFROSCH Scrypt
     *
     * @psalm-suppress PossiblyFalseArgument
     * @psalm-suppress PossiblyNullArgument
     *
     * @return void
     */
    public function testGeneratePsfScrypt(): void
    {
        $CWA = new CoreWrapper();
        $CWB = new CoreWrapper(51);
        for ($i=0; $i<5; $i++) {
            $bin = random_bytes(8);
            $password = base64_encode($bin);
            $HashA = $CWA->password_hash($password, 51);
            $expected = '$pipfrosch-scrypt$m=24,t=4,p=2$';
            $actual = substr($HashA, 0, 31);
            $this->assertSame($expected, $actual);
            $rs = PsfScrypt::passwordVerify($password, $HashA);
            $this->assertTrue($rs);
            $rs = PsfScrypt::passwordVerify('invalid', $HashA);
            $this->assertFalse($rs);
            $HashB = $CWB->password_hash($password);
            $actual = substr($HashB, 0, 31);
            $this->assertSame($expected, $actual);
            $rs = PsfScrypt::passwordVerify($password, $HashB);
            $this->assertTrue($rs);
            $rs = PsfScrypt::passwordVerify('invalid', $HashB);
            $this->assertFalse($rs);
            $options = array(
                'memory_cost' => 26624,
                'time_cost' => 2,
                'threads' => 2
            );
            $HashC = $CWA->password_hash($password, 51, $options);
            $expected = '$pipfrosch-scrypt$m=26,t=2,p=2$';
            $actual = substr($HashC, 0, 31);
            $this->assertSame($expected, $actual);
            $rs = PsfScrypt::passwordVerify($password, $HashC);
            $this->assertTrue($rs);
            $rs = PsfScrypt::passwordVerify('invalid', $HashC);
            $this->assertFalse($rs);
        }
    }//end testGeneratePsfScrypt()

    /**
     * Test Generating Valid PIPFROSCH Bcrypt
     *
     * @psalm-suppress PossiblyFalseArgument
     * @psalm-suppress PossiblyNullArgument
     *
     * @return void
     */
    public function testGeneratePsfBcrypt(): void
    {
        $CWA = new CoreWrapper();
        $CWB = new CoreWrapper(52);
        for ($i=0; $i<5; $i++) {
            $bin = random_bytes(8);
            $password = base64_encode($bin);
            $HashA = $CWA->password_hash($password, 52);
            $expected = '$pipfrosch-bcrypt$t=14$';
            $actual = substr($HashA, 0, 23);
            $this->assertSame($expected, $actual);
            $rs = PsfBcrypt::passwordVerify($password, $HashA);
            $this->assertTrue($rs);
            $rs = PsfBcrypt::passwordVerify('invalid', $HashA);
            $this->assertFalse($rs);
            $HashB = $CWB->password_hash($password);
            $actual = substr($HashB, 0, 23);
            $this->assertSame($expected, $actual);
            $rs = PsfBcrypt::passwordVerify($password, $HashB);
            $this->assertTrue($rs);
            $rs = PsfBcrypt::passwordVerify('invalid', $HashB);
            $this->assertFalse($rs);
            $options = array(
                'cost' => 12
            );
            $HashC = $CWA->password_hash($password, 52, $options);
            $expected = '$pipfrosch-bcrypt$t=12$';
            $actual = substr($HashC, 0, 23);
            $this->assertSame($expected, $actual);
            $rs = PsfBcrypt::passwordVerify($password, $HashC);
            $this->assertTrue($rs);
            $rs = PsfBcrypt::passwordVerify('invalid', $HashC);
            $this->assertFalse($rs);
        }
    }//end testGeneratePsfBcrypt()
}//end class

?>