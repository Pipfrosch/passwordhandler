<?php
declare(strict_types=1);

/**
 * Unit testing for \Pipfrosch\PasswordHandler\CoreWrapper
 *
 * @package    PasswordHandler
 * @subpackage UnitTests
 * @author     Alice Wonder <paypal@domblogger.net>
 * @license    https://opensource.org/licenses/MIT MIT
 * @link       https://gitlab.com/Pipfrosch/swpress
 */

use PHPUnit\Framework\TestCase;
use \Pipfrosch\PasswordHandler\CoreWrapper as CoreWrapper;

/**
 * Test class for \Pipfrosch\PasswordHandler\CoreWrapper
 */
// @codingStandardsIgnoreLine
final class ArgonViaSodiumTests extends TestCase
{
    /**
     * Test Argon2i hashing strength 0
     *
     * @psalm-suppress PossiblyFalseArgument
     * @psalm-suppress PossiblyNullArgument
     *
     * @return void
     */
    public function testArgonTwoEyeViaSodiumStrengthZero(): void
    {
        $CW = new CoreWrapper(2, 0, true);
        $ranbin = random_bytes(8);
        $password = base64_encode($ranbin);
        $hashString = $CW->password_hash($password);
        $expected = '$argon2i$v=19$m=196608,t=5,p=1$';
        $actual = substr($hashString, 0, 31);
        $this->assertSame($expected, $actual);
        $rs = $CW->password_verify($password, $hashString);
        $this->assertTrue($rs);
        $rs = $CW->password_verify('bogus', $hashString);
        $this->assertFalse($rs);
    }//end testArgonTwoEyeViaSodiumStrengthZero()

    /**
     * Test Argon2i hashing strength 1
     *
     * @psalm-suppress PossiblyFalseArgument
     * @psalm-suppress PossiblyNullArgument
     *
     * @return void
     */
    public function testArgonTwoEyeViaSodiumStrengthOne(): void
    {
        $CW = new CoreWrapper(2, 1, true);
        $ranbin = random_bytes(8);
        $password = base64_encode($ranbin);
        $hashString = $CW->password_hash($password);
        $expected = '$argon2i$v=19$m=262144,t=7,p=1$';
        $actual = substr($hashString, 0, 31);
        $this->assertSame($expected, $actual);
        $rs = $CW->password_verify($password, $hashString);
        $this->assertTrue($rs);
        $rs = $CW->password_verify('bogus', $hashString);
        $this->assertFalse($rs);
    }//end testArgonTwoEyeViaSodiumStrengthOne()

    /**
     * Test Argon2i hashing strength 2
     *
     * @psalm-suppress PossiblyFalseArgument
     * @psalm-suppress PossiblyNullArgument
     *
     * @return void
     */
    public function testArgonTwoEyeViaSodiumStrengthTwo(): void
    {
        $CW = new CoreWrapper(2, 2, true);
        $ranbin = random_bytes(8);
        $password = base64_encode($ranbin);
        $hashString = $CW->password_hash($password);
        $expected = '$argon2i$v=19$m=327680,t=9,p=1$';
        $actual = substr($hashString, 0, 31);
        $this->assertSame($expected, $actual);
        $rs = $CW->password_verify($password, $hashString);
        $this->assertTrue($rs);
        $rs = $CW->password_verify('bogus', $hashString);
        $this->assertFalse($rs);
    }//end testArgonTwoEyeViaSodiumStrengthTwo()

    /**
     * Test Argon2id hashing strength 0
     *
     * @psalm-suppress PossiblyFalseArgument
     * @psalm-suppress PossiblyNullArgument
     *
     * @return void
     */
    public function testArgonTwoIdViaSodiumStrengthZero(): void
    {
        $CW = new CoreWrapper(3, 0, true);
        $ranbin = random_bytes(8);
        $password = base64_encode($ranbin);
        $hashString = $CW->password_hash($password);
        $expected = '$argon2id$v=19$m=196608,t=5,p=1$';
        $actual = substr($hashString, 0, 32);
        $this->assertSame($expected, $actual);
        $rs = $CW->password_verify($password, $hashString);
        $this->assertTrue($rs);
        $rs = $CW->password_verify('bogus', $hashString);
        $this->assertFalse($rs);
    }//end testArgonTwoIdViaSodiumStrengthZero()

    /**
     * Test Argon2i hashing strength 1
     *
     * @psalm-suppress PossiblyFalseArgument
     * @psalm-suppress PossiblyNullArgument
     *
     * @return void
     */
    public function testArgonTwoIdViaSodiumStrengthOne(): void
    {
        $CW = new CoreWrapper(3, 1, true);
        $ranbin = random_bytes(8);
        $password = base64_encode($ranbin);
        $hashString = $CW->password_hash($password);
        $expected = '$argon2id$v=19$m=262144,t=7,p=1$';
        $actual = substr($hashString, 0, 32);
        $this->assertSame($expected, $actual);
        $rs = $CW->password_verify($password, $hashString);
        $this->assertTrue($rs);
        $rs = $CW->password_verify('bogus', $hashString);
        $this->assertFalse($rs);
    }//end testArgonTwoIdViaSodiumStrengthOne()

    /**
     * Test Argon2i hashing strength 2
     *
     * @psalm-suppress PossiblyFalseArgument
     * @psalm-suppress PossiblyNullArgument
     *
     * @return void
     */
    public function testArgonTwoIdViaSodiumStrengthTwo(): void
    {
        $CW = new CoreWrapper(3, 2, true);
        $ranbin = random_bytes(8);
        $password = base64_encode($ranbin);
        $hashString = $CW->password_hash($password);
        $expected = '$argon2id$v=19$m=327680,t=9,p=1$';
        $actual = substr($hashString, 0, 32);
        $this->assertSame($expected, $actual);
        $rs = $CW->password_verify($password, $hashString);
        $this->assertTrue($rs);
        $rs = $CW->password_verify('bogus', $hashString);
        $this->assertFalse($rs);
    }//end testArgonTwoIdViaSodiumStrengthTwo()

    /**
     * Argon2i generated by core validated by sodium
     *
     * @psalm-suppress PossiblyFalseArgument
     * @psalm-suppress PossiblyNullArgument
     *
     * @return void
     */
    public function testArgonTwoEyeCoreGeneratedSodiumVerified(): void
    {
        $options = array(
            'memory_cost' => 2048,
            'time_cost'   => 3,
            'threads'     => 1
        );
        $ranbin = random_bytes(8);
        $password = base64_encode($ranbin);
        $hashString = password_hash($password, 2, $options);
        $CW = new CoreWrapper(2, 0, true);
        $rs = $CW->password_verify($password, $hashString);
        $this->assertTrue($rs);
        $rs = $CW->password_verify('bogus', $hashString);
        $this->assertFalse($rs);
    }//end testArgonTwoEyeCoreGeneratedSodiumVerified()

    /**
     * Argon2id generated by core validated by sodium
     *
     * @psalm-suppress PossiblyFalseArgument
     * @psalm-suppress PossiblyNullArgument
     *
     * @return void
     */
    public function testArgonTwoIdCoreGeneratedSodiumVerified(): void
    {
        $options = array(
            'memory_cost' => 2048,
            'time_cost'   => 2,
            'threads'     => 1
        );
        $ranbin = random_bytes(8);
        $password = base64_encode($ranbin);
        $hashString = password_hash($password, 3, $options);
        $CW = new CoreWrapper(3, 0, true);
        $rs = $CW->password_verify($password, $hashString);
        $this->assertTrue($rs);
        $rs = $CW->password_verify('bogus', $hashString);
        $this->assertFalse($rs);
    }//end testArgonTwoIdCoreGeneratedSodiumVerified()

    /**
     * Argon2i generated by sodium validated by core
     *
     * @psalm-suppress PossiblyFalseArgument
     * @psalm-suppress PossiblyNullArgument
     *
     * @return void
     */
    public function testArgonTwoEyeSodiumGeneratedCoreVerified(): void
    {
        $CW = new CoreWrapper(2, 0, true);
        $ranbin = random_bytes(8);
        $password = base64_encode($ranbin);
        $hashString = $CW->password_hash($password);
        $rs = password_verify($password, $hashString);
        $this->assertTrue($rs);
        $rs = password_verify('bogus', $hashString);
        $this->assertFalse($rs);
    }//end testArgonTwoEyeSodiumGeneratedCoreVerified()

    /**
     * Argon2id generated by sodium validated by core
     *
     * @psalm-suppress PossiblyFalseArgument
     * @psalm-suppress PossiblyNullArgument
     *
     * @return void
     */
    public function testArgonTwoIdSodiumGeneratedCoreVerified(): void
    {
        $CW = new CoreWrapper(3, 0, true);
        $ranbin = random_bytes(8);
        $password = base64_encode($ranbin);
        $hashString = $CW->password_hash($password);
        $rs = password_verify($password, $hashString);
        $this->assertTrue($rs);
        $rs = password_verify('bogus', $hashString);
        $this->assertFalse($rs);
    }//end testArgonTwoIdSodiumGeneratedCoreVerified()
}//end class

?>