<?php
declare(strict_types=1);

/**
 * This file contains an example helper classes for the php-scrypt extension.
 *
 * As with all cryptographic code; it is recommended that you use a tried and
 * tested library which uses this library; rather than rolling your own.
 *
 * Ported to PHP7 by Alice Wonder
 *
 * @category Security
 * @package  Scrypt
 * @author   Dominic Black <thephenix@gmail.com>
 * @license  http://www.opensource.org/licenses/BSD-2-Clause BSD 2-Clause License
 * @link     http://github.com/DomBlack/php-scrypt
 *
 * For PHP 5 original:
 *
 * @see https://raw.githubusercontent.com/DomBlack/php-scrypt/master/scrypt.php
 */

namespace Pipfrosch\PasswordHandler\Reference;

/**
 * This class abstracts away from scrypt module, allowing for easy use.
 *
 * You can create a new hash for a password by calling Password::hash($password)
 *
 * You can check a password by calling Password::check($password, $hash)
 *
 * @category Security
 * @package  Scrypt
 * @author   Dominic Black <thephenix@gmail.com>
 * @license  http://www.opensource.org/licenses/BSD-2-Clause BSD 2-Clause License
 * @link     http://github.com/DomBlack/php-scrypt
 */
class ScryptPassword
{
    /**
     *
     * @var int The key length
     */
    private static $keyLength = 32;

    /**
     * Get the byte-length of the given string
     *
     * @param string $str Input string.
     *
     * @return int
     */
    protected static function strlen($str)
    {
        static $isShadowed = null;

        if ($isShadowed === null) {
            $isShadowed = extension_loaded('mbstring') &&
                intval(ini_get('mbstring.func_overload')) & 2;
        }

        if ($isShadowed) {
            return mb_strlen($str, '8bit');
        } else {
            return strlen($str);
        }
    }//end strlen()

    /**
     * Generates a random salt
     *
     * @param int $length The length of the salt.
     *
     * @return string The salt
     */
    public static function generateSalt($length = 8)
    {
        $buffer = random_bytes($length);
        $buffer_valid = true;

        $salt = str_replace(array('+', '$'), array('.', ''), base64_encode($buffer));

        return $salt;
    }//end generateSalt()

    /**
     * Create a password hash.
     *
     * @param string      $password The clear text password.
     * @param string|null $salt     The salt to use, or null to generate a random one.
     * @param int         $N        The CPU difficultly (must be a power of 2, > 1).
     * @param int         $r        The memory difficultly.
     * @param int         $p        The parallel difficultly.
     *
     * @return string The hashed password
     */
    public static function hash(string $password, $salt = null, $N = 16384, $r = 8, $p = 1)
    {
        if ($N == 0 || ($N & ($N - 1)) != 0) {
            throw new \InvalidArgumentException("N must be > 0 and a power of 2");
        }

        if ($N > PHP_INT_MAX / 128 / $r) {
            throw new \InvalidArgumentException("Parameter N is too large");
        }

        if ($r > PHP_INT_MAX / 128 / $p) {
            throw new \InvalidArgumentException("Parameter r is too large");
        }

        if (empty($salt)) {
            $salt = self::generateSalt();
        } else {
            // Remove dollar signs from the salt, as we use that as a separator.
            $salt = str_replace(array('+', '$'), array('.', ''), base64_encode($salt));
        }

        $hash = scrypt($password, $salt, $N, $r, $p, self::$keyLength);

        return $N . '$' . $r . '$' . $p . '$' . $salt . '$' . $hash;
    }//end hash()

    /**
     * Check a clear text password against a hash
     *
     * @param string $password The clear text password.
     * @param string $hash     The hashed password.
     *
     * @return boolean If the clear text matches
     */
    public static function check($password, $hash)
    {
        // Is there actually a hash?
        if (!$hash) {
            return false;
        }

        list ($N, $r, $p, $salt, $hash) = explode('$', $hash);

        // No empty fields?
        if (empty($N) or empty($r) or empty($p) or empty($salt) or empty($hash)) {
            return false;
        }

        // Are numeric values numeric?
        if (!is_numeric($N) or !is_numeric($r) or !is_numeric($p)) {
            return false;
        }

        $calculated = scrypt($password, $salt, $N, $r, $p, self::$keyLength);

        // Use hash_equals to avoid timeing attacks
        return hash_equals($hash, $calculated);
    }//end check()
}//end class

?>