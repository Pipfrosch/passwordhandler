Reference Classes
=================

The `ScryptPassword` class is a port of the `scrypt.php` file at
https://raw.githubusercontent.com/DomBlack/php-scrypt/master/scrypt.php

It has been ported to PHP7 with PHP CodeSniffer and vimeo/psalm cleanup.

It is here for the purpose of unit tests, to make sure `$CW->password_verify()`
can validate passwords hashed in this manner. It should not be used to create
new password hashes for production systems, as it does not use a standard
hash format.