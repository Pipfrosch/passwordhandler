CREATE DATABASE somedatabase;

GRANT ALL PRIVILEGES ON somedatabase.* TO 'authenticationuser'@'localhost' IDENTIFIED BY 'changme';

USE somedatabase;

CREATE TABLE userauth (
userid       INT          NOT NULL AUTO_INCREMENT PRIMARY KEY,
disabled     TINYINT      NOT NULL DEFAULT 0,
username     VARCHAR(20)  NOT NULL UNIQUE,
email        VARCHAR(64)  NOT NULL UNIQUE,
passhash     VARCHAR(255) NOT NULL UNIQUE,
pgpkey       VARCHAR(255),
metadata     TEXT
) DEFAULT CHARACTER SET utf8_mb4;

CREATE TABLE passreset (
userid       INT          UNIQUE NOT NULL PRIMARY KEY,
resetsalt    CHAR(24),
resethash    CHAR(64),
resetexpires TIMESTAMP
) DEFAULT CHARACTER SET utf8_mb4;

CREATE TABLE devicelog (
deviceid         INT          NOT NULL AUTO_INCREMENT PRIMARY KEY,
fingerprint      VARCHAR(96)  NOT NULL UNIQUE,
twofactorexpire  TIMESTAMP
) DEFAULT CHARACTER SET utf8_mb4;

CREATE TABLE twofactor (
userid       INT              UNIQUE NOT NULL PRIMARY KEY,
phonenumber  VARCHAR(24)      UNIQUE NOT NULL,
2fauserid    VARCHAR(32),
authmethod   VARCHAR(12),
pinsalt      CHAR(24),
pinhash      CHAR(64),
pinexpires   DATETIME
) DEFAULT CHARACTER SET utf8_mb4;

-- reset logic - 32 byte random_bytes is base64 encoded using a URI query safe alphabet.
--  16 byte random_bytes() is base64_encode() encoded and put in the database
--  The 32 random bytes for reset code has the salt bytes appended and is sha384 hashed.
--  That sha384 hash is then base64_encode encoded and stuck in the database.
--
-- When user does a password reset, the actual reset code is not in the database but the
--  actual code is used to reconstruct the hash from the salt, which is in the database,
--  and then the resulting hash can be compared.
--
-- The pgp column may not need to be varchar(255)
--  It is for encrypting e-mail to the end user requesting a password reset.
--
-- Table for 2FA is missing 