SQL Readme
==========

Subject to radical change. Nothing in this directory is actually used by any of
the PHP scripts at this time. I am thinking out-loud.

The point of this: Most web applications are poorly designed using a single
database with a single database user for everything, including user
authentication. This means any bug in the web application itself or in a plugin
that has a [SQL Injection](https://www.owasp.org/index.php/SQL_Injection)
(SQLi) vulnerability allows the authentication information to be remotely
retrieved.

A better design is to use a separate database table for user authentication,
with a different database user, so that the SQL connection used for the rest of
the web application does not have permission to even read the database with
authentication information and any SQLi vulnerabilities will not result in a
dump of user authentication information.


Table `userauth`
----------------

The `ueserauth` table will not depend upon the other tables being created. It
serves a single purpose, password authentication of a single user.

The `userid` field should correspond with the user id used elsewhere in the
web application.

The `disabled` column makes an easy way to disable the account. Simply set the
value to 1 instead of 0 and the account is disabled.

The `username` column is for the login username.

The `email` address is for the e-mail address associated with the account, or
at least the address used for login and/or password resets, there is no reason
why it has to be the same as the e-mail address used by the rest of the web
application but I suspect it will usually be set up that way.

The `passhash` column is for the hash string corresponding to the user
password. I use `VARCHAR(255)` which is presently excessive but it allows for
easy transition to alternative password hashing formats and algorithms that may
require larger columns than current hashing formats (e.g. bcrypt only uses 60
characters but Argon2id uses variable length often close to 100).

The `pgpkey` field is for users who use GnuPG (or another PGP implementation).
When these users have entered their key, e-mails related to account
authentication can be encrypted.

The `metadata` column will not be used by any of the scripts here but it is
intended as a column where serialized information about the user can be stored
if your web application has need for such information but you do not want it in
the same database as the main web application.


Table `passreset`
-----------------

The `passreset` table is only used when a user has initiated a password reset.

The `userid` field corresponds with the `userid` column in the `userauth`
field.

The `resetsalt` field is for a base64 encoded 16-byte salt.

The `resethash` field is for a base64 encoded 48-byte hash of the reset code
and the salt.

The idea is the reset code is generated and sent to the user. The reset code
itself is not stored in the table, but rather, a salt and a hash generated from
the salt and the actual reset code is stored in the database.

When the user clicks on the link in their e-mail to generate a new password,
the link has the reset code as a GET variable. The server then hashes it using
the salt from the database and compares the result with the hash in the
database and only allows the password reset if they match.

The `resetexpires` field is for an expiration on the password reset. I would
set it to 10 minutes from the time the password reset was requested.


Table `devicelog`
-----------------

For those who implement Two-Factor Authentication, generally it is a good idea
to require 2FA every time a user logs in from a new device or 60 days have
passed since they last used 2FA to authenticate on that device.

This table is intended to help enforce that.

The `deviceid` column is just a unique identifier for the device itself.

The `fingerprint` column is for a device fingerprint, which should include the
`userid` followed by a colon followed by a random generated string. It gets
stored in the session data for the user. When a user logs out, the session
cookie should not be deleted, but rather, just indicate the user is no longer
authenticated. That way the fingerprint will remain and the device will not be
seen as a new device unless the user clears their cookies. Fingerprint may not
be an accurate description, as it will simply be a random number assigned to
the device rather than based on a characteristic of the device.

The `twofactorexpire` field is set to 60 days (or whatever) after that device
has been authenticated by the user with 2FA. When that timestamp is reached,
the user will be required to authenticate the device again.


Table `twofactor`
-----------------

For those who implement Two-Factor Authentication, this table will contain the
needed information for the web application to connect to the 2FA service.

The columns here are *very* preliminary.

The `userid` column corresponds with the `userid` in the `userauth` table.

The `phonenumber` column will contain the phone number for the user, for SMS
2FA though I *highly* recommend SMS 2FA only be used as a last resort.

The `2fauserid` column is for how the 2FA service used identifies the user.

The `authmethod` column is for identifying which 2FA service is being used, I
plan to have code for several.

The `pinsalt` column is for a base64 encoded 16-byte salt generated by the
web application.

The `pinhash` column is for a base64 encoded 48-byte hash of pin needed by for
the 2FA to validate (salted with the salt), the actual pin itself should not
be stored in the database.

The `pinexpires` column specifies the time when the pin is no longer considered
valid even if it validates.




















