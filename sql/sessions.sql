CREATE DATABASE phpsessions;

GRANT ALL PRIVILEGES ON phpsessions.* TO 'dbuser'@'localhost' IDENTIFIED BY 'dbpass';

USE phpsessions;

-- a userid of 0 is used for users that are not logged in
-- expires gets set to very near future by destroy method of class
CREATE TABLE sessiondb (
sid         CHAR(32)     UNIQUE NOT NULL PRIMARY KEY,
userid      INT          DEFAULT NULL,
accounttype VARCHAR(24)  DEFAULT NULL,
serialized  TEXT         NOT NULL DEFAULT '',
created     TIMESTAMP    NOT NULL DEFAULT CURRENT_TIMESTAMP,
accessed    TIMESTAMP    NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
expires     DATETIME     NOT NULL DEFAULT '2038-01-19 03:14:07',
location    VARCHAR(45)  NOT NULL DEFAULT 'unknown'
) DEFAULT CHARACTER SET utf8_mb4;