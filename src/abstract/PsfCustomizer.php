<?php
declare(strict_types=1);

/**
 * @package PasswordHandler
 * @author  Alice Wonder <paypal@domblogger.net>
 * @license https://opensource.org/licenses/MIT MIT
 * @link    https://gitlab.com/Pipfrosch/passwordhandler
 *
 * This is an abstract class that can be extended to creat custom PHC String Format
 * classes.
 */

namespace Pipfrosch\PasswordHandler\Template;

/**
 * Abstract PHC String Format wrapper for password hashing algorithms.
 */
abstract class PsfCustomizer
{
    /**
     * The algo identifier.
     * Replace this when extending the class
     *
     * @var string
     */
    protected static $algoId = 'vendor-algo';
    
    /**
     * The default byte length when creating a hash
     *
     * @var int
     */
    protected static $hashLength = 48;

    /**
     * The minimum hash byte length
     *
     * @var int
     */
    protected static $minHashLength = 32;

    /**
     * The maximum hash byte length
     *
     * @var int
     */
    protected static $maxHashLength = 64;

    /**
     * The size in bytes of the salt to generate
     *
     * @var int
     */
    protected static $saltLength = 16;

    /**
     * The minimum salt size
     *
     * @var int
     */
    protected static $minSaltLength = 12;

    /**
     * The maximum salt size
     *
     * @var int
     */
    protected static $maxSaltLength = 24;

    /**
     * Grabs the current year for use with cost factor adjustment
     * while ensuring the date() comment is not being exploited.
     *
     * @return int
     */
    protected static function getCurrentYear(): int
    {
        $minYear = 2019;
        $currYear = intval(date('Y'));
        if ($currYear <= $minYear) {
            return $minYear;
        }
        if (date('m') > 3) {
            error_log('Please update the PsfCustomizer class file, minYear is old.');
        }
        return $currYear;
    }//end getCurrentYear()


    /**
     * Generate a salt
     *
     * @param int $size Optional. The size in bytes for the salt.
     *
     * @return string The base64 encoded salt
     */
    protected static function generateSalt($size = 0): string
    {
        if ($size < self::$minSaltLength || $size > self::$maxSaltLength) {
            $size = self::$saltLength;
        }
        $random = random_bytes($size);
        return rtrim(base64_encode($random), '=');
    }//end generateSalt()

    /**
     * Normalizes a user password to printable 7-bit ASCII using the supplied salt.
     *
     * This should only be used inside the generateHash function.
     *
     * @param string $password The password to normalize.
     * @param string $salt     The salt to use when normalizing the password.
     *
     * @return void
     *
     * @throws \InvalidArgumentException
     */
    protected static function normalizePassword(string &$password, string $salt): void
    {
        if (empty($password)) {
            throw new \InvalidArgumentException('The password can not be empty.');
        }
        if (empty($salt)) {
            throw new \InvalidArgumentException('The salt can not be empty.');
        }
        $bindata = hash_hmac('sha384', hash('sha256', $salt, true), hash('sha256', $password, true), true);
        $password = base64_encode($bindata);
    }//end normalizePassword()


    /**
     * The method used to create the actual hash string. ALWAYS throw exception if a hash can not
     *  be generated from it.
     *
     * @param string $password   The password to be hashed.
     * @param string $salt       The salt to be used by the hashing algorithm.
     * @param array  $parameters An array of parameters needed by the hashing algorithm.
     *
     * @return string The hash of the password.
     */
    abstract protected static function generateHash(string $password, string $salt, array $parameters);

    /**
     * Generates the PSF formatted string
     *
     * @param string $algoId     The algorithm identifier.
     * @param array  $parameters The parameters that belong in the PSF hash string.
     * @param string $salt       The salt.
     * @param string $hash       The hash.
     *
     * @return string
     *
     * @throws \InvalidArgumentException
     */
    protected static function formatHashString($algoId, array $parameters, string $salt, string $hash): string
    {
        if (empty($parameters) || empty($salt) || empty($hash)) {
            throw new \InvalidArgumentException('Incomplete PSF Information');
        }
        if ($algoId === self::$algoId) {
            throw new \InvalidArgumentException('Hash Algorithm ID string not properly defined.');
        }
        $return = array('');
        $return[] = $algoId;
        $return[] = implode(',', $parameters);
        $return[] = $salt;
        $return[] = $hash;
        return implode('$', $return);
    }//end formatHashString()

    /**
     * Returns the default parameters for a specified $strengthFactor.
     *
     * @param int $strengthFactor Optional. 0, 1, or 2.
     *
     * @return array
     */
    abstract public static function getDefaults(int $strengthFactor = 0);

    /**
     * Generate a password hash string. MUST throw an exception when it can not create a valid string.
     *
     * @param string $password       The password to hash.
     * @param int    $strengthFactor Optional. An integer to increase the default computational cost
     *                               when security requires it. 0 is paranoid, 1 is really paranoid,
     *                               2 is you have reason to believe the NSA may try to crack your
     *                               passwords if they get a dump of your hashes.
     *                               Defaults to 0.
     * @param array  $options        Optional. An array of parameters to override the class defaults.
     *
     * @return string A PHC String Format hash string.
     */
    abstract public static function passwordHasher(string $password, int $strengthFactor = 0, array $options = []);

    /**
     * Verify a password against the provided hash string.
     *
     * @param string $password   The password to validate.
     * @param string $hashString The hash string to validate against.
     *
     * @return bool True if the password validates, otherwise false.
     */
    abstract public static function passwordVerify(string $password, string $hashString);

}//end class

?>