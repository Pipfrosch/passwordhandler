<?php
declare(strict_types=1);

/**
 * Not finished, do not use
 *
 * @package PasswordHandler
 * @author  Alice Wonder <paypal@domblogger.net>
 * @license https://opensource.org/licenses/MIT MIT
 * @link    https://gitlab.com/Pipfrosch/passwordhandler
 *
 * This class provides an API compatible wrapper to the core PHP
 * password_hash() and password_verify() functions.
 *
 * When the core functions support the requested algorithm, this class simply passes the parameters to those
 * functions. However when the core functions do not support the requested algorithm, this class will polyfill
 * the functionallity when it can.
 *
 * This class does update the Argon2 default parameters to what is coming in PHP 7.4.
 * @see     https://wiki.php.net/rfc/sodium.argon.hash
 *
 * This class also updates the bcrypt default cost value to 14.
 *
 * This class does NOT update parameters as time passes. The PasswordHandler class (not yet imported into this
 * repository) does that.
 *
 * The password_hash() wrapper adds support for Argon2i and Argon2id when the sodium extension is available but
 * the core PHP was not built against libargon2. It also adds support for scrypt via the PsfScrypt class if the
 * PECL scrypt module is available.
 *
 * The password_verify() wrapper adds hash verification support for the above mentioned algorithms as well as
 * for phpass and Drupal 7 hashes.
 *
 * I am open to adding additional algorithms to the password_hash() wrapper as long as they are high quality
 * password hashing algorithms *and* use either the PHC String Format (PSF) or the Modular Crypt Format (MCF).
 *
 * Other than the few crypt() hashes that do not use PSF or MCF that core password_verify() already supports, the
 * password_verify() wrapper will not support hash string formats that do not use PSF or MCF.
 *
 * @see https://github.com/P-H-C/phc-string-format/blob/master/phc-sf-spec.md
 * @see https://passlib.readthedocs.io/en/stable/modular_crypt_format.html
 */

namespace Pipfrosch\PasswordHandler;

/**
 * Wrapper for PHP core password_hash() and password_verify() functions
 */
class CoreWrapper
{
    /**
     * Set by constructor. The current Gregorian calendar year.
     *
     * @var int
     */
    protected $year = 2019;

    /**
     * Set by the constructor. Determines the computational difficulty of the default
     * parameters.
     *
     * 0 - standard paranoia.
     * 1 - black helicopter paranoia.
     * 2 - the NSA is out to get me paranoia.
     *
     * @var int
     */
    protected $paranoiaLevel = 0;

    /**
     * Whether or not Argon2 support is via the sodium extension. Set by constructor if true.
     *
     * @var bool
     */
    protected $sodiumArgon = false;

    /**
     * Algorithms supported by PHP core password_hash(). Added to by constructor.
     *
     * @var array
     */
    protected $nativeHash = array('bcrypt');

    /**
     * Algorithms supported by PHP core password_verify(). Added to by constructor.
     *
     * @see https://www.php.net/manual/en/function.crypt.php
     *
     * @var array
     */
    protected $nativeVerify = array(
        'CRYPT_STD_DES',  // -- does not use PSF or MCF
        'CRYPT_EXT_DES',  // -- does not use PSF or MCF
        'CRYPT_MD5',      // -- MCF
        'CRYPT_BLOWFISH', // -- MCF
        'CRYPT_SHA256',   // -- MCF
        'CRYPT_SHA512',   // -- MCF
        'bcrypt'          // -- MCF
    );

    /**
     * Additional polyfill password_hash() algorithms. Added to by constructor.
     *
     * @var array
     */
    protected $polyHash = array();

    /**
     * Additional polyfill password_verify() algorithms. Added to by constructor.
     *
     * @var array
     */
    protected $polyVerify = array(
        'phpass',       // phpass         -- MCF
        'drupal7',      // Drupal 7       -- MCF
        'plain-md5',    // -- Single round no salt or other metadata. Weak.
        'plain-sha1',   // -- single round no salt or other metadata. Weak.
        'plain-sha256', // -- single round no salt or other metadata. Weak.
        'plain-sha384', // -- single round no salt or other metadata. Weak.
        'plain-sha512', // -- single round no salt or other metadata. Weak.
    );

    /**
     * Defines whether or not ripemd hash algos are available.
     *
     * @var bool
     */
    protected $ripemd = false;

    /**
     * Default password hash algorithm. Set by the constructor.
     *
     * 1  = bcrypt
     * 2  = Argon2i
     * 3  = Argon2id
     * 51 = Pipfrosch Scrypt
     * 52 = Pipfrisch Bcrypt
     *
     * @var int
     */
    protected $passwordDefault = 1;

    /**
     * Default bcrypt cost. Reset by calculateBcryptCost() which is called by the constructor.
     *
     * @var int
     */
    protected $bcryptCost = 14;

    /**
     * Default argon2 kikibytes. Reset by calculateArgonMemoryCost() which is called by the constructor.
     *
     * @var int
     */
    protected $argonMemCost = 196608;

    /**
     * Default argon2 time cost. Reset by calculateArgonTimeCost() which is called by the constructor.
     *
     * @var int
     */
    protected $argonTimeCost = 4;

    /**
     * Default argon2 threads. Reset by calculateArgonThreads() which is called by the constructor.
     *
     * @var int
     */
    protected $argonThreads = 3;

    /**
     * Base64 encoding alphabet for phpass / drupal 7 hashes. Do not change or the hashes will
     *  not validate.
     *
     * @var string
     */
    protected $itoa64 = './0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';

    /* protected methods */

    /**
     * Calculates the bcrypt cost based on year and paranoiaLevel. Every other year
     *  (odd years) the bcrypt cost increases by one (doubling the rounds required)
     *
     * @return void
     */
    protected function calculateBcryptCost(): void
    {
        $cost = $this->paranoiaLevel + (($this->year - 1991) >> 1);
        if ($cost > 31) {
            $this->bcryptCost = 31;
            return;
        }
        if ($cost > $this->bcryptCost) {
            $this->bcryptCost = $cost;
        }
    }//end calculateBcryptCost()

    /**
     * Calculates the Argon2 memory cost based on the calendar year and paranoiaLevel.
     * It increases every years such that every three years it has doubled.
     *
     * @return void
     */
    protected function calculateArgonMemoryCost(): void
    {
        $floatYearFactor = ($this->year - 2019) / 3;
        switch ($this->paranoiaLevel) {
            case 1:
                $base = 64;
                break;
            case 2:
                $base = 80;
                break;
            default:
                $base = 48;
                break;
        }
        $memcost = intval($base * (2 ** $floatYearFactor)) << 12;
        if ($memcost > $this->argonMemCost) {
            $this->argonMemCost = $memcost;
        }
    }//end calculateArgonMemoryCost()

    /**
     * Calculates the Argon2 time cost value to use based on paranoiaLevel, adding one every two years
     *
     * @return void
     */
    protected function calculateArgonTimeCost(): void
    {
        $timecost = ($this->paranoiaLevel << 1) + (($this->year - 2011) >> 1);
        if ($timecost > $this->argonTimeCost) {
            $this->argonTimeCost = $timecost;
        }
    }//end calculateArgonTimeCost()

    /**
     * Calculates the Argon2 threads, taking paranoiaLevel into account.
     *
     * @return void
     */
    protected function calculateArgonThreads(): void
    {
        if ($this->sodiumArgon) {
            $this->argonThreads = 1;
            // compensate for lack of multiple threads
            $this->argonTimeCost = $this->argonTimeCost + 1;
        } else {
            $threads = 3 + (2 * $this->paranoiaLevel);
            if ($threads > $this->argonThreads) {
                $this->argonThreads = $threads;
            }
        }
    }//end calculateArgonThreads()

    /**
     * Determines if a hash string is standard DES
     *
     * @param string $hashString The hash string to test.
     *
     * @return bool True if standard DES, otherwise false.
     */
    protected function isStandardDes(string $hashString): bool
    {
        if (strlen($hashString) !== 13) {
            return false;
        }
        $test = preg_match('/[^A-Za-z0-9\.\/]/', $hashString);
        if (is_int($test) && $test === 0) {
            return true;
        }
        return false;
    }//end isStandardDes()

    /**
     * Determines if a hash string is extended DES
     *
     * @param string $hashString The hash string to test.
     *
     * @return bool True if extended DES, otherwise false.
     */
    protected function isExtendedDes(string $hashString): bool
    {
        if (strlen($hashString) !== 20) {
            return false;
        }
        if (substr($hashString, 0, 1) !== "_") {
            return false;
        }
        $hashString = substr($hashString, 1);
        $test = preg_match('/[^A-Za-z0-9\.\/]/', $hashString);
        if (is_int($test) && $test === 0) {
            return true;
        }
        return false;
    }//end isExtendedDes()

    /**
     * Determine if a hash uses reference scrypt format
     *
     * @param string $hashString The hash string to test.
     *
     * @return bool True if reference script, otherwise false.
     */
    protected function isReferenceScrypt(string $hashString): bool
    {
        $count = substr_count($hashString, '$');
        if ($count !== 4) {
            return false;
        }
        list ($N, $r, $p, $salt, $hash) = explode('$', $hashString);
        $N = str_ireplace('scrypt:', '', $N);
        if (empty($N) || ! is_numeric($N)) {
            return false;
        }
        $N = intval($N);
        if (($N & ($N - 1)) != 0) {
            return false;
        }
        if (empty($r) || ! is_numeric($r)) {
            return false;
        }
        if (empty($p) || ! is_numeric($p)) {
            return false;
        }
        if (empty($salt)) {
            return false;
        }
        if (empty($hash)) {
            return false;
        }
        if (! ctype_xdigit($hash)) {
            $bin = base64_decode($hash);
            if (! is_string($bin)) {
                return false;
            }
            $hash = bin2hex($bin);
        }
        $len = strlen($hash);
        $bytes = $len >> 1;
        if ($bytes < 24) {
            return false;
        } elseif ($bytes > 48) {
            return false;
        }
        return true;
    }//end isReferenceScrypt()

    /**
     * Converts base64 encoded to hex.
     *
     * Of course assumes base64_encode() alphabet used.
     *
     * @param string $base64 The base64 encoded string.
     *
     * @return void
     */
    protected function convertBaseSixtyFourToHex(string &$base64): void
    {
        $test = base64_decode($base64);
        if (is_string($test)) {
            $base64 = bin2hex($test);
        }
    }//end convertBaseSixtyFourToHex()

    /**
     * Identifies the algo from a hash string and returns it
     *
     * @param string $hashString The hash string.
     *
     * @return string The algorithm name.
     */
    protected function getAlgoNameFromHashString(string $hashString): string
    {
        $algoCode = 'invalid';
        if (substr($hashString, 0, 1) === '$') {
            $arr = explode('$', $hashString);
            if (isset($arr[2]) && strlen($arr[1]) > 0) {
                $algoCode = $arr[1];
            }
        } else {
            //may be one of the DES algos or scrypt
            if ($this->isStandardDes($hashString)) {
                $algoCode = 'std-des';
            } elseif ($this->isExtendedDes($hashString)) {
                $algoCode = 'ext-des';
            } elseif ($this->isReferenceScrypt($hashString)) {
                $algoCode = 'reference-scrypt';
            }
        }
        // last ditch effort
        if ($algoCode === 'invalid' && ! ctype_xdigit($hashString)) {
            $this->convertBaseSixtyFourToHex($hashString);
        }
        if (ctype_xdigit($hashString)) {
            $len = strlen($hashString);
            switch ($len) {
                case 32:
                    $algoCode = 'plain-md5';
                    break;
                case 40:
                    $algoCode = 'plain-sha1';
                    break;
                case 64:
                    $algoCode = 'plain-sha256';
                    break;
                case 80:
                    $algoCode = 'plain-ripemd320';
                    break;
                case 96:
                    $algoCode = 'plain-sha384';
                    break;
                case 128:
                    $algoCode = 'plain-sha512';
                    break;
            }
        }
        switch ($algoCode) {
            case 'std-des':
                return 'CRYPT_STD_DES';
                break;
            case 'ext-des':
                return 'CRYPT_EXT_DES';
                break;
            case '1':
                return 'CRYPT_MD5';
                break;
            case '2a':
            case '2x':
                return 'CRYPT_BLOWFISH';
                break;
            case '2y':
                return 'bcrypt';
                break;
            case '5':
                return 'CRYPT_SHA256';
                break;
            case '6':
                return 'CRYPT_SHA512';
                break;
            case 'H':
            case 'P':
                return 'phpass';
                break;
            case 'S':
                return 'drupal7';
                break;
            case 'argon2i':
            case 'argon2id':
            case 'pipfrosch-bcrypt':
            case 'pipfrosch-scrypt':
            case 'plain-md5':
            case 'plain-sha1':
            case 'plain-sha256':
            case 'plain-sha384':
            case 'plain-sha512':
            case 'plain-ripemd320':
            case 'reference-scrypt':
                return $algoCode;
                break;
        }
        return 'unknown';
    }//end getAlgoNameFromHashString()

    /**
     * Checks for incompatibility with the parameters and the sodium implementation
     * of libargon2.
     *
     * @see https://wiki.php.net/rfc/sodium.argon.hash
     *
     * @param string $algoCode   The variant of Argon2.
     * @param string $parameters The Argon2 parameters.
     *
     * @return bool True if the parameters will work.
     */
    protected function sodiumArgonParameterCheck(string $algoCode, string $parameters): bool
    {
        if ($algoCode === 'argon2i') {
            $minOPS = 3;
            // special case for PHP 7.2
            if (in_array('argon2i', $this->nativeVerify) && in_array('argon2id', $this->polyVerify)) {
                return true;
            }
        } else {
            $minOPS = SODIUM_CRYPTO_PWHASH_OPSLIMIT_INTERACTIVE;
        }
        list ($m, $t, $p) = explode(',', $parameters);
        $p = intval(substr($p, 2));
        if ($p !== 1) {
            return false;
        }
        $t = intval(substr($t, 2));
        if ($t < $minOPS) {
            return false;
        }
        return true;
    }//end sodiumArgonParameterCheck()

    /* protected methods for phpass and drupal7 style hashes */

    /**
     * From phpass 0.5 - which is public domain. Modified by Alice Wonder. My mods
     *  were purely stylistic and to (in my opinion) aid in readability.
     *
     * Encodes the input in base64 using the phpass alphabet.
     *
     * This function makes heavy use of bitwise operators.
     *  The 0x3f is hex for decimal 63 - the highest value in a singe base64 digit.
     *
     * @author Solar Designer <solar@openwall.com>
     * @author Alice Wonder <paypal@domblogger.net>
     *
     * @see https://www.openwall.com/phpass/
     * @see https://www.php.net/manual/en/language.operators.bitwise.php
     *
     * @param string $input The raw input.
     * @param int    $count The count.
     *
     * @return string The custom base64 encoded string
     */
    protected function phpassEncode64(string $input, int $count): string
    {
        $output = '';
        $i = 0;
        while ($i < $count) {
            $value = ord($input[$i++]);
            $output .= $this->itoa64[$value & 0x3f];
            if ($i < $count) {
                $value |= ord($input[$i]) << 8;
            }
            $output .= $this->itoa64[($value >> 6) & 0x3f];
            if ($i++ >= $count) {
                break;
            }
            if ($i < $count) {
                $value |= ord($input[$i]) << 16;
            }
            $output .= $this->itoa64[($value >> 12) & 0x3f];
            if ($i++ >= $count) {
                break;
            }
            $output .= $this->itoa64[($value >> 18) & 0x3f];
        }
        return $output;
    }//end phpassEncode64()

    /**
     * From phpass 0.5 - which is public domain. Modified by Alice Wonder. My mods
     *  were purely stylistic and to (in my opinion) aid in readability.
     *
     * Generates a phpass portable hash for use in validating password against existing
     *  hash.
     *
     * @author Solar Designer <solar@openwall.com>
     * @author Alice Wonder <paypal@domblogger.net>
     *
     * @see https://www.openwall.com/phpass/
     *
     * @param string $password The password.
     * @param string $setting  The hash string (which includes the setting).
     *
     * @return string The resulting hash
     */
    protected function phpassCryptPrivate(string $password, string $setting): string
    {
        // guarantees a hash mismatch unless malformed setting starts with *0
        $output = '*0';
        if (substr($setting, 0, 2) === $output) {
            // guarantees a hash mismatch if malformed $setting starts with *0
            $output = '*1';
        }
        $id = substr($setting, 0, 3);
        # phpass uses "$P$", phpBB3 uses "$H$" for the same thing
        if (! in_array($id, array('$P$', '$H$'))) {
            if (function_exists('sodium_memzero')) {
                sodium_memzero($password);
            }
            //will trigger false validation
            return $output;
        }
        $count_log2 = strpos($this->itoa64, $setting[3]);
        if (! is_int($count_log2)) {
            if (function_exists('sodium_memzero')) {
                sodium_memzero($password);
            }
            //will trigger false validation
            return $output;
        }
        if ($count_log2 < 7 || $count_log2 > 30) {
            if (function_exists('sodium_memzero')) {
                sodium_memzero($password);
            }
            //will trigger false validation
            return $output;
        }
        $count = 1 << $count_log2;
        $salt = substr($setting, 4, 8);
        if (strlen($salt) !== 8) {
            if (function_exists('sodium_memzero')) {
                sodium_memzero($password);
            }
            //will trigger false validation
            return $output;
        }
        /*
         * phpass was forced to use md5 because at the time it was the only cryptographic
         * hash algorithm that was widely supported in every build of PHP
         */
        $hash = md5($salt . $password, true);
        while ($count > 0) {
            $hash = md5($hash . $password, true);
            $count--;
        }
        // now generate the real output
        $output = substr($setting, 0, 12);
        $output .= $this->phpassEncode64($hash, 16);
        if (function_exists('sodium_memzero')) {
            sodium_memzero($password);
        }
        return $output;
    }//end phpassCryptPrivate

    /**
     * Generates Drupal 7 password hash
     *
     * Modeled after phpassCryptPrivate() as Drupal 7 hash seems to be modeled after phpass.
     *
     * @see https://api.drupal.org/api/drupal/includes%21password.inc/function/_password_crypt/7.x
     *
     * @param string $password The password to validate.
     * @param string $setting  The password hash, which contains the actual settings.
     *
     * @return array Array of hashes. Drupal 7 allows for multiple hash algorithms however it
     *               also allows truncating hash so there is no way to determine which hash
     *               algorithm was used, so we have to test against as many as make sense.
     *               It appears that SHA-512 is used by default but the API indicates others
     *               can be used.
     */
    protected function drupalSevenPasswordHash(string $password, string $setting): array
    {
        $return = array();
        $algos = array('sha512');
        $outputLength = strlen($setting);
        if ($outputLength < 12) {
            if (function_exists('sodium_memzero')) {
                sodium_memzero($password);
            }
            //will trigger false validation
            return $return;
        }
        if ($outputLength <= 55) {
            $algos[] = 'sha256';
        }
        if ($outputLength <= 76) {
            $algos[] = 'sha384';
        }
        if ($outputLength <= 39) {
            $algos[] = 'sha1';
        }
        if ($outputLength <= 34) {
            $algos[] = 'md5';
        }
        $setting = substr($setting, 0, 12);
        $id = substr($setting, 0, 3);
        # drupal 7 uses $s$
        if ($id !== '$S$') {
            if (function_exists('sodium_memzero')) {
                sodium_memzero($password);
            }
            //will trigger false validation
            return $return;
        }
        $salt = substr($setting, 4, 8);
        $count_log2 = strpos($this->itoa64, $setting[3]);
        if (! is_int($count_log2)) {
            if (function_exists('sodium_memzero')) {
                sodium_memzero($password);
            }
            //will trigger false validation
            return $return;
        }
        $count = 1 << $count_log2;
        foreach ($algos as $algo) {
            $hash = hash($algo, $salt . $password, true);
            while ($count > 0) {
                $hash = hash($algo, $hash . $password, true);
                $count--;
            }
            $len = strlen($hash);
            $encoded = $setting . $this->phpassEncode64($hash, $len);
            if (strlen($encoded) >= $outputLength) {
                $return[] = substr($encoded, 0, $outputLength);
            }
        }
        if (function_exists('sodium_memzero')) {
            sodium_memzero($password);
        }
        return $return;
    }//end drupalSevenPasswordHash()

    /* end protected methods for phpass and drupal7 style hashes */

    /**
     * Reference scrypt
     *
     * @param string $password   The password to check.
     * @param string $hashString The hash string to check against.
     *
     * @return bool True if validates, otherwise false.
     */
    protected function referenceScryptPasswordValidate(string $password, string $hashString): bool
    {
        list ($N, $r, $p, $salt, $hash) = explode('$', $hashString);
        $N = str_ireplace('scrypt:', '', $N);
        if (empty($N) || ! is_numeric($N)) {
            return false;
        }
        $N = intval($N);
        if (($N & ($N - 1)) != 0) {
            return false;
        }
        if (empty($r) || ! is_numeric($r)) {
            return false;
        }
        $r = intval($r);
        if (empty($p) || ! is_numeric($p)) {
            return false;
        }
        $p = intval($p);
        if (empty($salt)) {
            return false;
        }
        if (empty($hash)) {
            return false;
        }
        if (! ctype_xdigit($hash)) {
            $bin = base64_decode($hash);
            if (! is_string($bin)) {
                return false;
            }
            $hash = bin2hex($bin);
        }
        $len = strlen($hash);
        $bytes = $len >> 1;
        if ($bytes < 24) {
            return false;
        } elseif ($bytes > 48) {
            return false;
        }
        $testHash = scrypt($password, $salt, $N, $r, $p, $bytes);
        return hash_equals($hash, $testHash);
    }//end referenceScryptPasswordValidate()

    /* sodium polyfill protected methods */

    /**
     * Polyfill for Argon2 password_hash via sodium
     *
     * @param string $password The password to hash.
     * @param string $algoName The algorithm.
     * @param array  $options  The options array.
     *
     * @return false|string The result of hash attempt.
     */
    protected function polyfillArgonHash(string $password, string $algoName, array $options)
    {
        if ($algoName === 'argon2i' && defined('SODIUM_CRYPTO_PWHASH_ALG_ARGON2I13')) {
            $algo = SODIUM_CRYPTO_PWHASH_ALG_ARGON2I13;
            $algoRealName = 'argon2i';
        } elseif (defined('SODIUM_CRYPTO_PWHASH_ALG_ARGON2ID13')) {
            $algo = SODIUM_CRYPTO_PWHASH_ALG_ARGON2ID13;
            $algoRealName = 'argon2id';
        } else {
            // theoretically should not happen w/ libsodium >= 1.0.15 but...
            sodium_memzero($password);
            return false;
        }
        $salt = random_bytes(SODIUM_CRYPTO_PWHASH_SALTBYTES);
        if (isset($options['memory_cost']) && is_numeric($options['memory_cost'])) {
            $memlimit = abs(intval($options['memory_cost'])) << 10;
        }
        if (! isset($memlimit) || $memlimit < SODIUM_CRYPTO_PWHASH_MEMLIMIT_INTERACTIVE) {
            $memlimit = SODIUM_CRYPTO_PWHASH_MEMLIMIT_INTERACTIVE;
        }
        if (isset($options['time_cost']) && is_numeric($options['time_cost'])) {
            $opslimit = abs(intval($options['time_cost']));
        }
        if (! isset($opslimit) || $opslimit < 3) {
            $opslimit = 3;
        }
        $binhash = sodium_crypto_pwhash(32, $password, $salt, $opslimit, $memlimit, $algo);
        sodium_memzero($password);
        $return = array('');
        $return[] = $algoRealName;
        $return[] = 'v=19'; // fixme ???
        $return[] = 'm=' . $options['memory_cost'] . ',t=' . $options['time_cost'] . ',p=1';
        $return[] = rtrim(base64_encode($salt), '=');
        $return[] = rtrim(base64_encode($binhash), '=');
        return implode('$', $return);
    }//end polyfillArgonHash()

    /**
     * Compares argon hash with sodium
     *
     * @param string $password   The password to check.
     * @param string $hashString The hash to check.
     *
     * @return bool True if valid
     */
    protected function polyfillArgonVerify(string $password, string $hashString): bool
    {
        $arr = explode('$', $hashString);
        if (count($arr) !== 6) {
            return false;
        }
        $algoName = $arr[1];
        switch ($algoName) {
            case 'argon2i':
                $algo = SODIUM_CRYPTO_PWHASH_ALG_ARGON2I13;
                break;
            case 'argon2id':
                $algo = SODIUM_CRYPTO_PWHASH_ALG_ARGON2ID13;
                break;
            default:
                return false;
        }
        $params = explode(',', $arr[3]);
        if (count($params) !== 3) {
            return false;
        }
        $memlimit = intval(substr($params[0], 2)) << 10;
        $opslimit = intval(substr($params[1], 2));
        $threads  = intval(substr($params[2], 2));
        if ($threads !== 1) {
            return false;
        }
        $salt = base64_decode($arr[4]);
        try {
            $testHashBin = sodium_crypto_pwhash(32, $password, $salt, $opslimit, $memlimit, $algo);
        } catch (\Exception $ignored) {
        }
        if (! isset($testHashBin) || ! is_string($testHashBin)) {
            return false;
        }
        $testHash = rtrim(base64_encode($testHashBin), '=');
        sodium_memzero($password);
        return hash_equals($arr[5], $testHash);
    }//end polyfillArgonVerify()

    /**
     * Compares parameters in two array.
     *
     * @param array $reference The reference array.
     * @param array $array     The array being checked.
     *
     * @return bool True if any value in a key=>value pair in $array is either
     *              missing or lower version than reference.
     */
    protected function parameterArrayCheck(array $reference, array $array): bool
    {
        foreach ($reference as $key => $value) {
            if (! isset($array[$key]) || ! is_numeric($array[$key])) {
                return true;
            }
            $checkme = intval($array[$key]);
            if ($checkme < $value) {
                return true;
            }
        }
        return false;
    }//end parameterArrayCheck()

    /* end sodium polyfull protected methods */

    /**
     * Export supported hash algos as an array.
     *
     * @return array Array of algorithms that will work with the
     *               password_hash() wrapper
     */
    public function getPasswordHashAlgos(): array
    {
        return array_merge($this->nativeHash, $this->polyHash);
    }//end getPasswordHashAlgos()

    /**
     * Export supported verify algos as an array
     *
     * @return array Array of algorithms that will work with the
     *               password_verify() wrapper
     */
    public function getPasswordVerifyAlgos(): array
    {
        return array_merge($this->nativeVerify, $this->polyVerify);
    }//end getPasswordVerifyAlgos()

    /**
     * Wrapper for password_get_info()
     *
     * @param string $hash The hash string to get info from.
     *
     * @return array
     */
    public function password_get_info(string $hash): array
    {
        $return = array();
        $algoName = $this->getAlgoNameFromHashString($hash);
        switch ($algoName) {
            case 'bcrypt':
                $return['algo'] = 1;
                break;
            case 'argon2i':
                $return['algo'] = 2;
                break;
            case 'argon2id':
                $return['algo'] = 3;
                break;
            case 'pipfrosch-scrypt':
                $return['algo'] = 51;
                break;
            case 'pipfrosch-bcrypt':
                $return['algo'] = 52;
                break;
            default:
                $return['algo'] = 0;
                break;
        }
        $return['algoName'] = $algoName;
        $options = array();
        if ($return['algo'] > 0) {
            $arr = explode('$', $hash);
            switch ($algoName) {
                case 'bcrypt':
                    $options['cost'] = intval($arr[2]);
                    break;
                case 'pipfrosch-bcrypt':
                    $options['cost'] = intval(substr($arr[2], 2));
                    break;
                case 'argon2i':
                case 'argon2id':
                    $brr = explode(',', $arr[3]);
                    $options['memory_cost'] = intval(substr($brr[0], 2));
                    $options['time_cost'] = intval(substr($brr[1], 2));
                    $options['threads'] = intval(substr($brr[2], 2));
                    break;
                case 'pipfrosch-scrypt':
                    $brr = explode(',', $arr[2]);
                    $options['memory_cost'] = intval(substr($brr[0], 2));
                    $options['time_cost'] = intval(substr($brr[1], 2));
                    $options['threads'] = intval(substr($brr[2], 2));
                    break;
            }
        }
        $return['options'] = $options;
        return $return;
    }//end password_get_info()

    /**
     * Wrapper for password_needs_rehash
     *
     * The core version, second argument is required. In this class, it will use the
     * class default if not defined and/or not a supported algorithm.
     *
     * @param string $hash    The password hash string.
     * @param int    $algo    Optional. The password algorithm number.
     * @param array  $options Optional. Array of minimum hashing options.
     *
     * @return bool True if rehash needed, otherwise false.
     */
    public function password_needs_rehash(string $hash, int $algo = 0, array $options = []): bool
    {
        if (! in_array($algo, array(1, 2, 3, 51, 52))) {
            $algo = $this->passwordDefault;
        }
        switch ($algo) {
            case 2:
                if (! in_array('argon2i', $this->nativeHash) && ! in_array('argon2i', $this->polyHash)) {
                    $algo = $this->passwordDefault;
                }
                break;
            case 3:
                if (! in_array('argon2id', $this->nativeHash) && ! in_array('argon2id', $this->polyHash)) {
                    $algo = $this->passwordDefault;
                }
                break;
            case 51:
                if (! in_array('pipfrosch-scrypt', $this->polyHash)) {
                    $algo = $this->passwordDefault;
                }
                break;
            case 52:
                if (! in_array('pipfrosch-bcrypt', $this->polyHash)) {
                    $algo = $this->passwordDefault;
                }
                break;
        }
        $passwordHashInfo = $this->password_get_info($hash);
        if ($passwordHashInfo['algo'] !== $algo) {
            return true;
        }
        // okay now compare options
        
        switch ($algo) {
            case 1:
            case 52:
                if ($algo === 1) {
                    $reference = array();
                    $reference['cost'] = $this->bcryptCost;
                } else {
                    $reference = PsfBcrypt::getDefaults($this->paranoiaLevel);
                }
                if (isset($options['cost']) && is_numeric($options['cost'])) {
                    $reference['cost'] = intval($options['cost']);
                }
                return $this->parameterArrayCheck($reference, $passwordHashInfo['options']);
                break;
            case 2:
            case 3:
            case 51:
                if ($algo === 2 || $algo === 3) {
                    $reference = array();
                    $reference['memory_cost'] = $this->argonMemCost;
                    $reference['time_cost'] = $this->argonTimeCost;
                    // never trigger on threads due to sodium
                    $reference['threads'] = 1;
                } else {
                    $reference = PsfScrypt::getDefaults($this->paranoiaLevel);
                }
                if (isset($options['memory_cost']) && is_numeric($options['memory_cost'])) {
                    $reference['memory_cost'] = intval($options['memory_cost']);
                }
                if (isset($options['time_cost']) && is_numeric($options['time_cost'])) {
                    $reference['time_cost'] = intval($options['time_cost']);
                }
                if (isset($options['threads']) && is_numeric($options['threads'])) {
                    $reference['threads'] = intval($options['threads']);
                }
                return $this->parameterArrayCheck($reference, $passwordHashInfo['options']);
                break;
        }
        return false;
    }//end password_needs_rehash()

    /**
     * Wrapper for password_hash()
     *
     * @param string $password The password to hash.
     * @param int    $algo     The desired algorithm.
     * @param array  $options  The options array.
     *
     * @return string|false|null A password hash on success, boolean false on failure, or
     *                           null for an unsupported algorithm.
     */
    public function password_hash(string $password, int $algo = 0, array $options = [])
    {
        $sodiumArgon = $this->sodiumArgon;
        if ($algo === 0) {
            $algo = $this->passwordDefault;
        }
        switch ($algo) {
            case '1':
                $algoName = 'bcrypt';
                break;
            case '2':
                $algoName = 'argon2i';
                if ($sodiumArgon) {
                    // for php 7.2
                    if (in_array('argon2i', $this->nativeHash) && in_array('argon2id', $this->polyHash)) {
                        $sodiumArgon = false;
                    }
                }
                break;
            case '3':
                $algoName = 'argon2id';
                break;
            case '51':
                $algoName = 'pipfrosch-scrypt';
                break;
            case '52':
                $algoName = 'pipfrosch-bcrypt';
                break;
            default:
                trigger_error('Unknown password hashing algorithm: ' . $algo, E_USER_ERROR);
                return null;
        }
        if (! in_array($algoName, $this->nativeHash) && ! in_array($algoName, $this->polyHash)) {
            trigger_error('Your PHP environment does not support ' . $algoName, E_USER_ERROR);
            return null;
        }
        $optionArray = array();
        switch ($algoName) {
            case 'bcrypt':
                if (isset($options['salt'])) {
                    trigger_error('Specifying salt is deprecated, ignoring.', E_USER_DEPRECATED);
                }
                if (isset($options['cost']) && is_numeric($options['cost'])) {
                    $cost = abs(intval($options['cost']));
                }
                if (! isset($cost)) {
                    $cost = $this->bcryptCost;
                }
                if ($cost < 4) {
                    $cost = 4;
                } elseif ($cost > 31) {
                    $cost = 31;
                }
                $optionArray['cost'] = $cost;
                break;
            case 'argon2i':
            case 'argon2id':
                if (isset($options['memory_cost']) && is_numeric($options['memory_cost'])) {
                    $memoryCost = abs(intval($options['memory_cost']));
                }
                if (! isset($memoryCost) || $memoryCost === 0) {
                    $memoryCost = $this->argonMemCost;
                }
                $optionArray['memory_cost'] = $memoryCost;
                if (isset($options['time_cost']) && is_numeric($options['time_cost'])) {
                    $timeCost = abs(intval($options['time_cost']));
                }
                if (! isset($timeCost) || $timeCost === 0) {
                    $timeCost = $this->argonTimeCost;
                }
                if ($sodiumArgon && $timeCost < 3) {
                    $timeCost = 3;
                }
                $optionArray['time_cost'] = $timeCost;
                if (isset($options['threads']) && is_numeric($options['threads'])) {
                    $threads = abs(intval($options['threads']));
                }
                if (! isset($threads) || $threads === 0) {
                    $threads = $this->argonThreads;
                }
                if ($sodiumArgon) {
                    $threads = 1;
                }
                $optionArray['threads'] = $threads;
                break;
            case 'pipfrosch-scrypt':
                if (isset($options['memory_cost']) && is_numeric($options['memory_cost'])) {
                    $memoryCost = abs(intval($options['memory_cost']));
                    $optionArray['memory_cost'] = $memoryCost;
                }
                if (isset($options['time_cost']) && is_numeric($options['time_cost'])) {
                    $optionArray['time_cost'] = abs(intval($options['time_cost']));
                }
                if (isset($options['threads']) && is_numeric($options['threads'])) {
                    $optionArray['threads'] = abs(intval($options['threads']));
                }
                break;
            case 'pipfrosch-bcrypt':
                if (isset($options['cost']) && is_numeric($options['cost'])) {
                    $cost = abs(intval($options['cost']));
                }
                if (isset($cost)) {
                    if ($cost < 12) {
                        $cost = 12;
                    } elseif ($cost > 31) {
                        $cost = 31;
                    }
                    $optionArray['cost'] = $cost;
                }
                break;
        }
        // native support
        if (in_array($algoName, $this->nativeHash)) {
            $return = password_hash($password, $algo, $optionArray);
            if (function_exists('sodium_memzero')) {
                sodium_memzero($password);
            }
            return $return;
        }
        // if still here...
        $return = false;
        switch ($algoName) {
            case 'pipfrosch-scrypt':
                //todo - exception handle
                $return = PsfScrypt::passwordHasher(
                    $password,
                    $this->paranoiaLevel,
                    $optionArray
                );
                break;
            case 'pipfrosch-bcrypt':
                //todo - exception handle
                $return = PsfBcrypt::passwordHasher(
                    $password,
                    $this->paranoiaLevel,
                    $optionArray
                );
                break;
            case 'argon2i':
            case 'argon2id':
                $return = $this->polyfillArgonHash($password, $algoName, $optionArray);
                break;
        }
        if (function_exists('sodium_memzero')) {
            sodium_memzero($password);
        }
        return $return;
    }//end password_hash()

    /**
     * Wrapper for password_verify()
     *
     * @param string $password The password to validate.
     * @param string $hash     The hash string to validate against.
     *
     * @return bool True if password validates against the hash string, False if it fails
     *              validation.
     */
    public function password_verify(string $password, string $hash):bool
    {
        $return = false;
        $algoName = $this->getAlgoNameFromHashString($hash);
        
        // special case for Argon2
        if ($algoName === 'argon2id' || $algoName === 'argon2i') {
            $arr = explode('$', $hash);
            if ($this->sodiumArgon && isset($arr[3])) {
                if (! $this->sodiumArgonParameterCheck($algoName, $arr[3])) {
                    sodium_memzero($password);
                    throw new \InvalidArgumentException(
                        'This hash is not compatible with sodium. Please upgrade to PHP linked against libargon2.'
                    );
                }
            }
        }
        if (in_array($algoName, $this->nativeVerify)) {
            $return = password_verify($password, $hash);
        } elseif (in_array($algoName, $this->polyVerify)) {
            // okay a polyfill
            switch ($algoName) {
                case 'phpass':
                    $testHash = $this->phpassCryptPrivate($password, $hash);
                    $return = hash_equals($hash, $testHash);
                    break;
                case 'drupal7':
                    $arr = $this->drupalSevenPasswordHash($password, $hash);
                    foreach ($arr as $testHash) {
                        if (hash_equals($hash, $testHash)) {
                            $return = true;
                            break;
                        }
                    }
                    break;
                case 'pipfrosch-scrypt':
                    $return = PsfScrypt::passwordVerify($password, $hash);
                    break;
                case 'pipfrosch-bcrypt':
                    $return = PsfBcrypt::passwordVerify($password, $hash);
                    break;
                case 'argon2i':
                case 'argon2id':
                    //$return = sodium_crypto_pwhash_str_verify($password, $hash);
                    $return = $this->polyfillArgonVerify($password, $hash);
                    break;
                case 'plain-md5':
                    if (! ctype_xdigit($hash)) {
                        $this->convertBaseSixtyFourToHex($hash);
                    }
                    $testHash = md5($password);
                    $return = hash_equals($hash, $testHash);
                    // may be ripemd128
                    if (! $return && $this->ripemd) {
                        $testHash = hash('ripemd128', $password, false);
                        $return = hash_equals($hash, $testHash);
                    }
                    break;
                case 'plain-sha1':
                    if (! ctype_xdigit($hash)) {
                        $this->convertBaseSixtyFourToHex($hash);
                    }
                    $testHash = hash('sha1', $password, false);
                    $return = hash_equals($hash, $testHash);
                    // may be ripemd160, I have seen that
                    if (! $return && $this->ripemd) {
                        $testHash = hash('ripemd160', $password, false);
                        $return = hash_equals($hash, $testHash);
                    }
                    break;
                case 'plain-sha256':
                    if (! ctype_xdigit($hash)) {
                        $this->convertBaseSixtyFourToHex($hash);
                    }
                    $testHash = hash('sha256', $password, false);
                    $return = hash_equals($hash, $testHash);
                    // may be ripemd256
                    if (! $return && $this->ripemd) {
                        $testHash = hash('ripemd256', $password, false);
                        $return = hash_equals($hash, $testHash);
                    }
                    break;
                case 'plain-ripemd320':
                    if (! ctype_xdigit($hash)) {
                        $this->convertBaseSixtyFourToHex($hash);
                    }
                    $testHash = hash('ripemd320', $password, false);
                    $return = hash_equals($hash, $testHash);
                    break;
                case 'plain-sha384':
                    if (! ctype_xdigit($hash)) {
                        $this->convertBaseSixtyFourToHex($hash);
                    }
                    $testHash = hash('sha384', $password, false);
                    $return = hash_equals($hash, $testHash);
                    break;
                case 'plain-sha512':
                    if (! ctype_xdigit($hash)) {
                        $this->convertBaseSixtyFourToHex($hash);
                    }
                    $testHash = hash('sha512', $password, false);
                    $return = hash_equals($hash, $testHash);
                    break;
                case 'reference-scrypt':
                    $return = $this->referenceScryptPasswordValidate($password, $hash);
                    break;
            }
        }
        if (function_exists('sodium_memzero')) {
            sodium_memzero($password);
        }
        return $return;
    }//end password_verify()

    /**
     * The constructor
     *
     * @param int  $passwordDefault Optional. The default password algorithm to use. 1 for bcrypt, 2 for argon2i,
     *                              3 for argon2id, 51 for pipfrosch-scrypt, 52 for pipfrosch-bscrypt. If the
     *                              selected algorithm is not supported it will default to PASSWORD_DEFAULT.
     * @param int  $paranoiaLevel   Optional. An integer value between 0 and 2 inclusive. Defaults to 0.
     *                              0 - standard paranoia.
     *                              1 - black helicopter paranoia.
     *                              2 - the NSA is out to get me paranoia.
     * @param bool $unitTesting     Optional. Only set to true when unit testing the argon2 via sodium polyfill.
     */
    public function __construct(int $passwordDefault = 0, int $paranoiaLevel = 0, bool $unitTesting = false)
    {
        $year = intval(date('Y'));
        if ($year > $this->year) {
            // we check before replacing just in case an attacker has a means of manipulating the
            //  output of date(). Basically it is input validation.
            $this->year = $year;
        }
        if ($paranoiaLevel < 0) {
            $paranoiaLevel = 0;
        } elseif ($paranoiaLevel > 2) {
            $paranoiaLevel = 2;
        }
        $this->paranoiaLevel = $paranoiaLevel;

        if (extension_loaded('scrypt') && class_exists('\Pipfrosch\PasswordHandler\PsfScrypt')) {
            $this->polyHash[]   = 'pipfrosch-scrypt';
            $this->polyVerify[] = 'pipfrosch-scrypt';
            $this->polyVerify[] = 'reference-scrypt';
        }
        if (class_exists('\Pipfrosch\PasswordHandler\PsfBcrypt')) {
            $this->polyHash[]   = 'pipfrosch-bcrypt';
            $this->polyVerify[] = 'pipfrosch-bcrypt';
        }
        if ($unitTesting && extension_loaded('sodium')) {
            $this->polyHash[]   = 'argon2i';
            $this->polyVerify[] = 'argon2i';
            $this->polyHash[]   = 'argon2id';
            $this->polyVerify[] = 'argon2id';
            $this->sodiumArgon  = true;
        } else {
            if (extension_loaded('sodium')) {
                if (defined('SODIUM_CRYPTO_PWHASH_ALG_ARGON2ID13') && ! defined('PASSWORD_ARGON2ID')) {
                    $this->polyHash[]   = 'argon2id';
                    $this->polyVerify[] = 'argon2id';
                    $this->sodiumArgon  = true;
                }
                if (defined('SODIUM_CRYPTO_PWHASH_ALG_ARGON2I13') && ! defined('PASSWORD_ARGON2I')) {
                    $this->polyHash[]   = 'argon2i';
                    $this->polyVerify[] = 'argon2i';
                    $this->sodiumArgon  = true;
                }
                // for php 7.4
                if (defined('PASSWORD_ARGON2_PROVIDER') && (PASSWORD_ARGON2_PROVIDER === 'sodium')) {
                    $this->sodiumArgon  = true;
                }
            }
            if (defined('PASSWORD_ARGON2I')) {
                $this->nativeHash[]   = 'argon2i';
                $this->nativeVerify[] = 'argon2i';
            }
            if (defined('PASSWORD_ARGON2ID')) {
                $this->nativeHash[]   = 'argon2id';
                $this->nativeVerify[] = 'argon2id';
            }
        }
        $ha = hash_algos();
        if (in_array('ripemd320', $ha)) {
            $this->ripemd = true;
            $this->polyVerify[] = 'plain-ripemd320';
        }
        $this->calculateBcryptCost();
        $this->calculateArgonMemoryCost();
        $this->calculateArgonTimeCost();
        $this->calculateArgonThreads();
        switch ($passwordDefault) {
            case 1:
                $this->passwordDefault = 1;
                break;
            case 2:
                if (in_array('argon2i', $this->nativeHash) || in_array('argon2i', $this->polyHash)) {
                    $this->passwordDefault = 2;
                } else {
                    $this->passwordDefault = PASSWORD_DEFAULT;
                }
                break;
            case 3:
                if (in_array('argon2id', $this->nativeHash) || in_array('argon2id', $this->polyHash)) {
                    $this->passwordDefault = 3;
                } else {
                    $this->passwordDefault = PASSWORD_DEFAULT;
                }
                break;
            case 51:
                if (in_array('pipfrosch-scrypt', $this->polyHash)) {
                    $this->passwordDefault = 51;
                } else {
                    $this->passwordDefault = PASSWORD_DEFAULT;
                }
                break;
            case 52:
                if (in_array('pipfrosch-bcrypt', $this->polyHash)) {
                    $this->passwordDefault = 52;
                } else {
                    $this->passwordDefault = PASSWORD_DEFAULT;
                }
                break;
            default:
                $this->passwordDefault = PASSWORD_DEFAULT;
        }
    }//end __construct()
}//end class

?>