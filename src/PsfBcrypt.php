<?php
declare(strict_types=1);

/**
 * @package PasswordHandler
 * @author  Alice Wonder <paypal@domblogger.net>
 * @license https://opensource.org/licenses/MIT MIT
 * @link    https://gitlab.com/Pipfrosch/passwordhandler
 *
 * This class implements the bcrypt password hash algorithm using password normalization.
 *
 * As a result of password normalization, the 72-character limit of standard bcrypt does not apply.
 *
 * This class uses the PHC String Format.
 *
 * @see https://github.com/P-H-C/phc-string-format/blob/master/phc-sf-spec.md
 *
 * The <id> string is the string `pipfrosch-bcrypt`
 *
 * The <param> is a , delimited key=value set of parameters containing the following *required* parameters:
 *  t=timefactor      The exponent for CPU difficulty (2^t is the CPU difficulty). This is equivalent to the
 *                    cost factor in a standard bcrypt MCF hash string.
 *
 * Example <param>: t=14
 *
 * Time factors below 12 and above 31 are not valid.
 *
 * The <salt> is a variable-length salt. When the class defaults, it will be 16 byte hash
 *  The salt must be between 12 and 24 bytes base64 encoded or this class will refuse to hash (including just for
 *  password validation)
 *
 * The <hash> is a variable-length base64 encoded hash and must be at least 32 bytes but no larger than 64 bytes.
 *  The class defaults to 48 bytes.
 *
 * The actual bcrypt hashing is performed by the crypt() function.
 *
 * @see https://www.php.net/manual/en/function.crypt.php
 */

namespace Pipfrosch\PasswordHandler;

/**
 * PHC String Format wrapper to the crypt() blowfish password hashing algorithm.
 */
class PsfBcrypt extends \Pipfrosch\PasswordHandler\Template\PsfCustomizer
{
    /**
     * The algo identifier.
     *
     * @var string
     */
    protected static $algoId = 'pipfrosch-bcrypt';

    /**
     * The default time cost factor to use in 2019 with a $costFactor of 0
     *
     * @var int
     */
    protected static $defaultCost = 14;

    /**
     * Calculates the default time factor. Doubles every two years.
     *
     * @param int $strengthFactor An integer to increase the default time factor
     *                            when security requires it. 0 is standard,
     *                            1 is moderate, 2 is high.
     *
     * @return int The time factor for calculating CPU cost.
     */
    protected static function defaultCostValue(int $strengthFactor = 0): int
    {
        $yearFactor = self::getCurrentYear() - 2019;
        return self::$defaultCost + $strengthFactor + ($yearFactor >> 1);
    }//end defaultCostValue()

    /**
     * The method used to create the actual hash string. ALWAYS throws exception if a hash can not
     *  be generated from it.
     *
     * @param string $password   The password to be hashed.
     * @param string $salt       The salt to be used by the hashing algorithm.
     * @param array  $parameters An array of parameters needed by the hashing algorithm.
     *
     * @return string The hash of the password.
     *
     * @throws \InvalidArgumentException
     */
    protected static function generateHash(string $password, string $salt, array $parameters): string
    {
        if (empty($password) || empty($salt)) {
            if (function_exists('sodium_memzero')) {
                sodium_memzero($password);
            }
            throw new \InvalidArgumentException('Neither the password not salt can be empty.');
        }

        if (! isset($parameters['t'])) {
            if (function_exists('sodium_memzero')) {
                sodium_memzero($password);
            }
            throw new \InvalidArgumentException('The t parameter (time cost factor) was not defined.');
        }
        if (! is_numeric($parameters['t'])) {
            if (function_exists('sodium_memzero')) {
                sodium_memzero($password);
            }
            throw new \InvalidArgumentException('The t parameter must be an integer between 12 and 31 inclusive.');
        }
        $t = intval($parameters['t']);
        if ($t < 12 || $t > 31) {
            if (function_exists('sodium_memzero')) {
                sodium_memzero($password);
            }
            throw new \InvalidArgumentException('The t parameter must be an integer between 12 and 31 inclusive.');
        }

        if (! isset($parameters['hashLength'])) {
            if (function_exists('sodium_memzero')) {
                sodium_memzero($password);
            }
            throw new \InvalidArgumentException('The hashLength parameter was not defined.');
        }
        $exceptionMessage = 'The hashLength parameter must be an integer between ';
        $exceptionMessage .= self::$minHashLength;
        $exceptionMessage .= ' and ';
        $exceptionMessage .= self::$maxHashLength;
        $exceptionMessage .= ' inclusive.';
        if (! is_numeric($parameters['hashLength'])) {
            if (function_exists('sodium_memzero')) {
                sodium_memzero($password);
            }
            throw new \InvalidArgumentException($exceptionMessage);
        }
        $length = intval($parameters['hashLength']);
        if ($length < self::$minHashLength || $length > self::$maxHashLength) {
            if (function_exists('sodium_memzero')) {
                sodium_memzero($password);
            }
            throw new \InvalidArgumentException($exceptionMessage);
        }

        $binsalt = base64_decode($salt);
        if (! is_string($binsalt)) {
            if (function_exists('sodium_memzero')) {
                sodium_memzero($password);
            }
            throw new \InvalidArgumentException('The salt must be a base64 encode string.');
        }
        if (strlen($binsalt) < self::$minSaltLength) {
            if (function_exists('sodium_memzero')) {
                sodium_memzero($password);
            }
            throw new \InvalidArgumentException('The salt must be at least ' . self::$minSaltLength . ' bytes.');
        }
        if (strlen($binsalt) > self::$maxSaltLength) {
            if (function_exists('sodium_memzero')) {
                sodium_memzero($password);
            }
            throw new \InvalidArgumentException('The salt must be at most ' . self::$maxSaltLength . ' bytes.');
        }

        /*
         * NOTE - using the first 11 bytes of an md5 of the salt as the crypt() friendly salt
         *  is a reduction in effective salt size. However this reduction is offset by the fact
         *  that the actual 16 (or whatever >= 12) byte salt is used during the normalization of
         *  the user password.
         */
        $cryptsalt = '$2a$' . $t . '$' . substr(hash('md5', $binsalt, false), 0, 22);

        self::normalizePassword($password, $salt);
        // append the length so same password and salt but different length result in different hash.
        $prehash = crypt($password, $cryptsalt) . $length;
        if (strlen($prehash) < 60) {
            //something went wrong
            if (function_exists('sodium_memzero')) {
                sodium_memzero($password);
            }
            throw new \InvalidArgumentException('An unknown error occurred.');
        }
        $binhash = hash('sha512', $prehash);
        $binhash = substr($binhash, 0, $length);
        return rtrim(base64_encode($binhash), '=');
    }//end generateHash()

    /**
     * Returns the default parameters for a specified $strengthFactor.
     *
     * @param int $strengthFactor Optional. 0, 1, or 2.
     *
     * @return array
     */
    public static function getDefaults(int $strengthFactor = 0): array
    {
        if ($strengthFactor < 0) {
            $strengthFactor = 0;
        } elseif($strengthFactor > 2) {
            $strengthFactor = 2;
        }
        $return = array();
        $return['cost'] = self::defaultCostValue($strengthFactor);
        return $return;
    }//end getDefaults()

    /**
     * Generate a password hash string. Throws an exception when it can not create a valid string.
     *
     * @param string $password       The password to hash.
     * @param int    $strengthFactor Optional. An integer to increase the default computational cost
     *                               when security requires it. 0 is paranoid, 1 is really paranoid,
     *                               2 is you have reason to believe the NSA may try to crack your
     *                               passwords if they get a dump of your hashes.
     *                               Defaults to 0.
     * @param array  $options        Optional. An array of parameters to override the class defaults.
     *                               cost => integer between 12 and 31 inclusive. Determines number
     *                                of rounds.
     *                               saltsize => integer between 12 and 24 inclusive. Determines the
     *                                byte size of the salt.
     *                               hashsize => integer between 32 and 64 inclusive. Determines the
     *                                byte size of the generated hash.
     *
     * @return string A PHC String Format hash string.
     */
    public static function passwordHasher(string $password, int $strengthFactor = 0, array $options = [])
    {
        if ($strengthFactor < 0) {
            $strengthFactor = 0;
        }
        if ($strengthFactor > 2) {
            $strengthFactor = 2;
        }
        $parameters = array();
        $psfParams  = array();

        $parameters['t'] = self::defaultCostValue($strengthFactor);
        if (isset($options['cost']) && is_numeric($options['cost'])) {
            $cost = intval($options['cost']);
            if ($cost >= 12 && $cost <= 31) {
                $parameters['t'] = $cost;
            }
        }
        $psfParams[] = 't=' . $parameters['t'];

        $parameters['hashLength'] = self::$hashLength;
        if (isset($options['hashsize']) && is_numeric($options['hashsize'])) {
            $hashLength = intval($options['hashsize']);
            if ($hashLength >= self::$minHashLength && $hashLength <= self::$maxHashLength) {
                $parameters['hashLength'] = $hashLength;
            }
        }

        $saltsize = self::$saltLength;
        if (isset($options['saltsize']) && is_numeric($options['saltsize'])) {
            $saltLength = intval($options['saltsize']);
            if ($saltLength >= self::$minSaltLength && $saltLength <= self::$maxSaltLength) {
                $saltsize = $saltLength;
            }
        }
        $salt = self::generateSalt($saltsize);
        try {
            $hash = self::generateHash($password, $salt, $parameters);
        } catch (\InvalidArgumentException $e) {
            if (function_exists('sodium_memzero')) {
                sodium_memzero($password);
            }
            throw new \InvalidArgumentException($e->getMessage());
        }
        return self::formatHashString(self::$algoId, $psfParams, $salt, $hash);
    }//end passwordHasher()

    /**
     * Verify a password against the provided hash string.
     *
     * @param string $password   The password to validate.
     * @param string $hashString The hash string to validate against.
     *
     * @return bool True if the password validates, otherwise false.
     */
    public static function passwordVerify(string $password, string $hashString): bool
    {
        if (empty($password)) {
            return false;
        }
        $hashString = substr($hashString, 1);
        list ($algo, $params, $salt, $hash) = explode('$', $hashString);
        if ($algo !== self::$algoId) {
            if (function_exists('sodium_memzero')) {
                sodium_memzero($password);
            }
            return false;
        }
        if (empty($params) || empty($salt) || empty($hash)) {
            if (function_exists('sodium_memzero')) {
                sodium_memzero($password);
            }
            return false;
        }
        $binsalt = base64_decode($salt);
        if (! is_string($binsalt)) {
            if (function_exists('sodium_memzero')) {
                sodium_memzero($password);
            }
            return false;
        }
        if (strlen($binsalt) < self::$minSaltLength || strlen($binsalt) > self::$maxSaltLength) {
            if (function_exists('sodium_memzero')) {
                sodium_memzero($password);
            }
            return false;
        }
        $binhash = base64_decode($hash);
        if (! is_string($binhash)) {
            if (function_exists('sodium_memzero')) {
                sodium_memzero($password);
            }
            return false;
        }
        $length = strlen($binhash);
        if ($length < self::$minHashLength || $length > self::$maxHashLength) {
            if (function_exists('sodium_memzero')) {
                sodium_memzero($password);
            }
            return false;
        }
        $parameters = array();
        $options = explode(',', $params);
        foreach ($options as $option) {
            list ($key, $value) = explode('=', $option);
            if (! empty($key) && ! empty($value)) {
                $parameters[$key] = $value;
            }
        }
        $parameters['hashLength'] = $length;
        try {
            $testHash = self::generateHash($password, $salt, $parameters);
        } catch (\InvalidArgumentException $e) {
            if (function_exists('sodium_memzero')) {
                sodium_memzero($password);
            }
            return false;
        }
        return hash_equals($hash, $testHash);
    }//end passwordVerify()
}//end class

?>