<?php
declare(strict_types=1);

/**
 * This class tests the strength of a submitted password using the
 *  /usr/bin/pwscore executable.
 *
 * I would prefer PECL module interfaces to that utility but the
 *  only ones I could find do not appear to be currently maintained
 *  which makes them dangerous.
 *
 * This class does not use cracklib-check as I am not aware of any
 *  passwords that score at least 50 with pwscore yet would fail
 *  cracklib-check. I wanted to limit the number of php exec() calls
 *  that were made, calling cracklib-check when the class already
 *  requires pwscore >= 40 seemed redundant and unnecessary use of
 *  the exec() function.
 *
 * The philosophy behind this class is a binary philosophy. Either the
 *  password is good enough or it isn't. It does not report the quality
 *  of the password on any kind of scale, such scales are easily
 *  misinterpreted.
 *
 * Unit tests not yet written
 *
 * @package PasswordHandler
 * @author  Alice Wonder <paypal@domblogger.net>
 * @license https://opensource.org/licenses/MIT MIT
 * @link    https://gitlab.com/Pipfrosch/passwordhandler
 */

namespace Pipfrosch\PasswordHandler;

/**
 * The class
 */
class StrengthValidator
{
    /**
     * Custom blacklist for what should fail but doesn't.
     *
     * @var array
     */
    protected $blacklist = array(
        'supersecretpassword',
    );

    /**
     * Path to pwscore
     *
     * @var string
     */
    protected $pwscore = '/usr/bin/pwscore';

    /**
     * Array of errors with the password
     *
     * @var array
     */
    protected $error = [];

    /**
     * Minimum Length
     *
     * @var int
     */
    protected $minimumLength = 9;

    /**
     * Maximum Length - this is somewhat arbitrary. Some hashing algorithms do truncate
     *  passwords beyond a certain length, and honestly beyond 127 characters is kind of
     *  pointless IMHO but feel free to bump it up.
     *
     * @var int
     */
    protected $maximumLength = 127;

    /**
     * Minimum pwscore - 40 may seem low but it should only be considered low for
     *  applications like banking where bumping it to 50 might be warranted.
     *
     * @var int
     */
    protected $minPasswordScore = 40;

    /**
     * Checks the length of the password.
     *
     * @param string $password The password to inspect the length of.
     *
     * @return void
     */
    protected function checkPasswordLength(string $password): void
    {
        $n = strlen($password);
        if ($n < $this->minimumLength) {
            $this->error[] = 'Password too short, minimum length: ' . $this->minimumLength;
        }
        if ($n > $this->maximumLength) {
            $this->error[] = 'Password too long, maximum length: ' . $this->maximumLength;
        }
        if (function_exists('sodium_memzero')) {
            sodium_memzero($password);
        }
    }//end checkPasswordLength

    /**
     * Catches some dictionary based passwords that pwscore does not catch.
     *
     * Should not be run on really long passwords as it will catch passphrases
     *  that are safe. Exits if pspell not available.
     *
     * @param string $password The password to check.
     *
     * @return void
     */
    protected function stringToArrayDictionaryCheck(string $password): void
    {
        if (! function_exists('pspell_new')) {
            return;
        }
        $string = preg_replace('/(?<=\\w)(?=[A-Z])/', "_$1", $password);
        $string = preg_replace('/\_/', ' ', $string);
        $string = preg_replace('/\-/', ' ', $string);
        $string = preg_replace('/,/', ' ', $string);
        $string = preg_replace('/;/', ' ', $string);
        $string = preg_replace('/\s+/', ' ', $string);
        $string = trim($string);
        $arr = explode(' ', $string);
        $n = count($arr);
        if ($n > 1) {
            $pspell_link = pspell_new('en');
            $i = 0;
            foreach ($arr as $word) {
                if (strlen($word) > 2) {
                    if (pspell_check($pspell_link, $word)) {
                        $i++;
                    }
                }
            }
            $m = intval(($i * 100) / $n);
            if ($m > 70) {
                $this->error[] = 'Based on dictionary words.';
            }
        }
        if (function_exists('sodium_memzero')) {
            sodium_memzero($password);
            sodium_memzero($string);
        }
    }//end stringToArrayDictionaryCheck()

    /**
     * Checks the pwscore of the submitted password.
     *
     * @param string $password The password to check.
     * @param string $username Optional. The username that the password is to be used with.
     *
     * @return void
     *
     * @throws \InvalidArgumentException if the pwscore executable does not exist.
     */
    protected function checkPasswordScore(string $password, string $username = ''): void
    {
        if (! file_exists($this->pwscore)) {
            throw new \InvalidArgumentException('The executable ' . $this->pwscore . ' does not exist.');
        }
        setlocale(LC_ALL, 'en_US.utf-8');
        $password = escapeshellarg($password);
        if (empty($username)) {
            $shellcommand = 'echo ' . $password . ' |' . $this->pwscore . ' 2>&1';
        } else {
            $username = escapeshellarg($username);
            $shellcommand = 'echo ' . $password . ' |' . $this->pwscore . ' ' . $username . ' 2>&1';
        }
        exec($shellcommand, $out, $ret);
        if ($ret !== 0) {
            foreach ($out as $err) {
                $this->error[] = trim($err);
            }
            $this->error[] = $this->pwscore . ' rejected the password as not safe.';
            return;
        }
        if (! is_numeric($out[0])) {
            $this->error[] = trim($out[0]);
            return;
        }
        $score = intval($out[0]);
        if ($score < $this->minPasswordScore) {
            $this->error[] = 'Password score below the minimum value of ' . $this->minPasswordScore;
        }
        if (function_exists('sodium_memzero')) {
            sodium_memzero($password);
        }
    }//end checkPasswordScore()

    /**
     * Add either a string or array of strings to blacklist
     *
     * @param string|array $input The string or array of strings to add.
     *
     * @return void
     */
    public function addToBlacklist($input): void
    {
        if (is_string($input)) {
            $input = strtolower(trim($input));
            $input = preg_replace('/\s+/', '', $input);
            if (is_string($input) && (! empty($input))) {
                $this->blacklist[] = $input;
            }
            return;
        }
        // it's an array
        foreach ($input as $arguement) {
            $string = (string) $arguement;
            $string = strtolower(trim($string));
            $string = preg_replace('/\s+/', '', $string);
            if (is_string($string) && (! empty($string))) {
                $this->blacklist[] = $string;
            }
        }
    }//end addToBlacklist()

    /**
     * Checks password with optional username.
     *
     * @param string $password The password to validate strength of.
     * @param string $username The optional username.
     *
     * @return bool|array Returns True if the password passes all checks, an
     *                    array of issues if the password fails some checks.
     */
    public function validatePasswordStrength(string $password, string $username = '')
    {
        $this->error = array();
        $this->checkPasswordLength($password);
        if (! empty($this->error)) {
            return $this->error;
        }
        if (PasswordBlacklist::check($password)) {
            $this->error[] = 'Password is pop culture reference.';
        }
        $username = trim($username);
        try {
            $this->checkPasswordScore($password, $username);
        } catch (\Exception $e) {
            $this->error[] = $e->getMessage();
        }
        if (count($this->error) > 0) {
            return $this->error;
        }
        // trim check
        $firstLetter = substr($password, 0, 1);
        $lastLetter = substr($password, -1);
        if ($firstLetter === $lastLetter) {
            $trimmed = trim($password, $firstLetter);
            try {
                $this->checkPasswordScore($trimmed, $username);
            } catch (\Exception $e) {
                $this->error[] = $e->getMessage();
            }
            if (count($this->error) > 0) {
                return $this->error;
            }
        }
        $tmp = preg_replace('/\s+/', '', $password);
        try {
            $this->checkPasswordScore($tmp, $username);
        } catch (\Exception $e) {
            $this->error[] = $e->getMessage();
        }
        if (count($this->error) > 0) {
            return $this->error;
        }
        $tmp = strtolower($tmp);
        if (in_array($tmp, $this->blacklist)) {
            $this->error[] = $tmp . ' is blacklisted word.';
            return $this->error;
        }
        if (strlen($password) < 36) {
            $this->stringToArrayDictionaryCheck($password);
            if (count($this->error) > 0) {
                return $this->error;
            }
        }
        // passed all checks
        if (function_exists('sodium_memzero')) {
            sodium_memzero($password);
            sodium_memzero($tmp);
        }
        return true;
    }//end validatePasswordStrength()

    /**
     * Checks password with optional username.
     *
     * This function calls validatePasswordStrength and returns a password hash
     *  if password of sufficient strength, array of issues if it is not of
     *  sufficient strength.
     *
     * @param string $password      The password to verify the strength of.
     * @param string $username      Optional. The username associated with the password.
     * @param int    $algo          Optional. Algorithm ID integer. Defaults to 1 (standard bcrypt).
     *                              All options:
     *                              1  -> standard bcrypt.
     *                              2  -> Argon2i.
     *                              3  -> Argon2id.
     *                              51 -> pipfrosch-scrypt.
     *                              52 -> pipfrosch-bcrypt.
     * @param int    $paranoiaLevel Optional. The level of paranoia to use when hashing.
     *                              0  -> standard paranoia.
     *                              1  -> black helicopter paranoia.
     *                              2  -> the NSA is out to get me paranoia.
     *
     * @return string|array A string with the password hash if password is strong enough, otherwise it
     *                      returns an array with error messages in it.
     */
    public function returnHashIfPasswordStrong(string $password, string $username = '', int $algo = 1, int $paranoiaLevel = 0)
    {
        $test = $this->validatePasswordStrength($password, $username);
        if (is_array($test)) {
            return $test;
        }
        // password is valid strength
        $CW = new CoreWrapper($algo, $paranoiaLevel);
        try {
            $hash = $CW->password_hash($password);
        } catch (\Exception $e) {
            // In theory should never happen
            $return = array();
            $return[] = $e->getMessage();
            return $return;
        }
        if (function_exists('sodium_memzero')) {
            sodium_memzero($password);
        }
        if (is_string($hash)) {
            return $hash;
        }
        $return = array('Error is generating hash');
        return $return;
    }//end returnHashIfPasswordStrong()

    /**
     * The constructor.
     *
     * @param bool $admin Optional. Whether or not this is a password being set for an admin
     *                    account. If true, the password checking is stricter.
     */
    public function __construct($admin = false)
    {
        if ($admin) {
            $this->minimumLength = 12;
            $this->minPasswordScore = 65;
        }
    }//end __construct()
}//end class

?>