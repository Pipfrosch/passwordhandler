<?php
declare(strict_types=1);

/**
 * This class provides a static method for detecting whether or not a password
 *  is a pop culture reference. This class is Creative Commons CC0 as the bulk
 *  of the content belongs to pop culture.
 *
 * Stargate SG1
 *    would fail this test.
 * Stargate#SG1 or Stargate@SG1 or Stargate SG1#
 *    would pass this test, and appropriately so as they are not the naked pop reference.
 *
 * @package PasswordHandler
 * @author  Alice Wonder <paypal@domblogger.net>
 * @license https://creativecommons.org/publicdomain/zero/1.0/ CC0
 * @link    https://gitlab.com/Pipfrosch/passwordhandler
 */

namespace Pipfrosch\PasswordHandler;

/**
 * The class
 */
class PasswordBlacklist
{
    /**
     * Custom blacklist containing pop culture references. The list will grow.
     *
     * @var array
     */
    protected static $blacklist = array(
        'alfredhitchcockpresents',
        'alfredhitchcock',
        'allinthefamily',
        'americans',
        'andygriffithshow',
        'andyfriffithshow',
        'arresteddevelopment',
        'avatarthelastairbender',
        'bandofbrothers',
        'batmantheanimatedseries',
        'battlestargalactica',
        'bettercallsaul',
        'beverlyhills90210',
        'boardwalkempire',
        'breakingbad',
        'buffythevampireslayer',
        'chapelle\'sshow',
        'chapellesshow',
        'cosbyshow',
        'cowboybebop',
        'curbyourenthusiasm',
        'dailyshow',
        'deadwood',
        'doctorwho',
        'downtownabbey',
        'dr.katz,professionaltherapist',
        'drkatzprofessionaltherapist',
        'fawltytowers',
        'freaks&geeks',
        'freaksandgeeks',
        'fridaynightlights',
        'futurama',
        'gameofthrones',
        'goldengirls',
        'goodtimes',
        'goodwife',
        'hannibal',
        'happydays',
        'hillstreetblues',
        'honeymooners',
        'houseofcards',
        'i,claudius',
        'iclaudius',
        'ilovelucy',
        'inlivingcolor',
        'it\'salwayssunnyinphiladelphia',
        'itsalwayssunnyinphiladelphia',
        'larrysandersshow',
        'lateshowwithdavidletterman',
        'law&order',
        'lawandorder',
        'leftovers',
        'madmen',
        'marytylermooreshow',
        'marytylermooreshow',
        'montypython\'sflyingcircus',
        'montypythonsflyingcircus',
        'mr.showwithbobanddavid',
        'mr.showwithbob&david',
        'mrshowwithbobanddavid',
        'mrshowwithbob&david',
        'muppetshow',
        'mysterysciencetheater3000',
        'mysterysciencetheater',
        'nypdblue',
        'office',
        'parksandrecreation',
        'prisoner',
        'realworld',
        'rickandmorty',
        'roseanne',
        'sanford&son',
        'sanfordandson',
        'saturdaynightlive',
        'sesamestreet',
        'sexandthecity',
        'shield',
        'simpsons',
        'sixfeetunder',
        'sopranos',
        'southpark',
        'stargateatlantis',
        'stargatesg1',
        'startrekdeepspacenine',
        'startrekds9',
        'startrekthenextgeneration',
        'startrektng',
        'startrektheoriginalseries',
        'startrektos',
        'tonightshow',
        'twenty-four',
        'twentyfour',
        'twilightzone',
        'twinpeaks',
        'westwing',
        'willandgrace',
        'will&grace',
        'wonderyears',
        'x-files',
        'xfiles',
    );

    /**
     * Check if password in blacklist
     *
     * @param string $password The password to check.
     *
     * @return bool True if in blacklist.
     */
    public static function check(string $password): bool
    {
        $tmp = preg_replace('/\s+/', '', $password);
        $tmp = preg_replace('/^the/i', '', $tmp);
        $tmp = strtolower($tmp);
        $return = false;
        if (in_array($tmp, self::$blacklist)) {
            $return = true;
        }
        if (function_exists('sodium_memzero')) {
            sodium_memzero($password);
            sodium_memzero($tmp);
        }
        return $return;
    }//end check()
}//end class

?>