<?php
declare(strict_types=1);

/**
 * @package    PasswordHandler
 * @subpackage TwoFactorAuthentication
 * @author     Alice Wonder <paypal@domblogger.net>
 * @license    https://opensource.org/licenses/MIT MIT
 * @link       https://gitlab.com/Pipfrosch/passwordhandler
 *
 * Static methods used with two-factor authentication.
 * Subject to radical change, this class is me thinking out loud.
 *
 * I repeat - subject to radical change, I'm still researching 2FA.
 *
 * First I am implementing 2FA via SMS even though I disdain that
 *  method. It's important though to have it as an option because
 *  some users will not want to install an application on their
 *  phone. But note that SMS was not designed to be a secure
 *  authentication mechanism.
 */

namespace Pipfrosch\PasswordHandler;

/**
 * Static methods used with two-factor authentication.
 */
class TwoFactorStatic
{
    /**
     * Generates 16-byte base64 encoded salt.
     *
     * @return string A 24-character string.
     */
    protected static function generateSalt(): string
    {
        $bin = random_bytes(16);
        return rtrim(base64_encode($bin), '=');
    }//end generateSalt()

    /**
     * Generates 48-byte base64 encoded hash.
     *
     * @param string $pin  The 2FA pin.
     * @param string $salt A base64 encoded salt.
     *
     * @return string
     */
    protected static function generateHashFromPIN(string $pin, string $salt): string
    {
        $pin = trim($pin);
        $salt = trim($salt);
        if (empty($pin)) {
            throw new \InvalidArgumentException('The pin argument can not be empty.');
        }
        if (strlen($salt) !== 24) {
            // all salts are base64 encoded 16 byte and will have 24 character length
            throw new \InvalidArgumentException('The salt is not a valid salt.');
        }
        $salt = base64_decode($salt);
        $hashbin = hash_hmac('sha384', hash('sha256', $salt, true), hash('sha256', $pin, true), true);
        return rtrim(base64_encode($hashbin), '=');
    }//end generateHashFromPIN()

    /**
     * Compare a pin against the hash
     *
     * @param string $pin  The user entered pin to validate.
     * @param string $salt The salt from the database.
     * @param string $hash The hash from the database.
     *
     * @return bool True if pin validates, otherwise false.
     */
    protected static function compareUserPIN(string $pin, string $salt, string $hash): bool
    {
        if (empty($pin) || empty($salt) || empty($hash)) {
            return false;
        }
        try {
            $checkHash = self::generateHashFromPIN($pin, $salt);
        } catch (\Exception $e) {
            error_log($e->getMessage());
            return false;
        }
        return hash_equals($hash, $checkHash);
    }//end compareUserPIN()

    /**
     * Insert 2FA validation information into database
     *
     * @param \PDO   $pdo    The PDO database object.
     * @param int    $userid The user ID number.
     * @param string $pin    The 2FA authentication pin.
     *
     * @return void
     */
    public static function insertPinHash(\PDO $pdo, int $userid, string $pin): void
    {
        $pin = trim($pin);
        if (empty($pin)) {
            throw new \InvalidArgumentException('The pin argument can not be empty.');
        }
        $t = time() + 600;
        $expires = date('Y-m-d h:i:s', $t);
        $salt = self::generateSalt();
        $hash = self::generateHashFromPIN($pin, $salt);
        $sql = 'UPDATE twofactor SET pinsalt=? AND pinhash=? AND pinexpires=? WHERE userid=?';
        $q = $pdo->prepare($sql);
        $arr = array($salt, $hash, $expires, $userid);
        $q->execute($arr);
    }//end insertPinHash()

    /**
     * Validate a user PIN
     *
     * TODO - increment a counter with each failed attempt.
     *
     * @param \PDO   $pdo    The PDO database object.
     * @param int    $userid The user ID.
     * @param string $pin    The user submitted PIN.
     *
     * @return bool True if validates, otherwise false.
     */
    public static function validateUserPIN(\PDO $pdo, int $userid, string $pin): bool
    {
        $pin = trim($pin);
        if (empty($pin)) {
            throw new \InvalidArgumentException('The pin argument can not be empty.');
        }
        $now = date('Y-m-d h:i:s');
        $sql = 'SELECT pinsalt,pinhash FROM twofactor WHERE userid=? AND pinexpires > ?';
        $q = $pdo->prepare($sql);
        $arr = array($userid, $now);
        $q->execute($arr);
        if ($rs = $q->fetchAll()) {
            if (isset($rs[0]->pinsalt)) {
                $salt = $rs[0]->pinsalt;
                if (strlen($salt) !== 24) {
                    return false;
                }
            } else {
                return false;
            }
            if (isset($rs[0]->pinhash)) {
                $hash = $rs[0]->pinhash;
            } else {
                return false;
            }
        } else {
            return false;
        }
        if (self::compareUserPIN($pin, $salt, $hash)) {
            $sql = 'UPDATE twofactor SET pinhash="" WHERE userid=?';
            $pdo->query($sql);
            return true;
        }
        return false;
    }//end validateUserPIN()

    /* SMS should only be a fall-back option */

    /**
     * Send an SMS via nexmo.
     *
     * As of 2019-07-01 cost is under a penny to any number in United States.
     *
     * @see https://developer.nexmo.com/messaging/sms/overview
     *
     * Completely untested.
     *
     * @param \PDO $pdo    The PDO object.
     * @param int  $userid The userid.
     *
     * @return bool True if the message sent, otherwise false.
     */
    public static function twoFactorViaNexmoSMS(\PDO $pdo, int $userid): bool
    {
        if (! defined('NEXMO_API_KEY')) {
            return false;
        }
        if (! defined('NEXMO_API_SECRET')) {
            return false;
        }
        if (! defined('SMS_FROM')) {
            return false;
        }
        $sql = 'SELECT phonenumber FROM twofactor WHERE userid=?';
        $q = $pdo->prepare($sql);
        $arr = array($userid);
        $q->execute($arr);
        if ($rs = $q->fetchAll()) {
            if (isset($rs[0]->phonenumber)) {
                //TODO validate the phone number is valid
                $number = $rs[0]->phonenumber;
            } else {
                return false;
            }
        } else {
            return false;
        }
        // generate the SMS code
        $rnd = (string) random_int(0, 999999);
        $pin = str_pad($rnd, 6, '0', STR_PAD_LEFT);
        self::insertPinHash($pdo, $userid, $pin);
        // POST via curl
        $url = 'https://rest.nexmo.com/sms/json';
        $fields = array(
            'from' => SMS_FROM,
            'text' => 'Your six digit Two-Factor Authentication code: ' . $pin,
            'to' => $number,
            'api_key' => NEXMO_API_KEY,
            'api_secret' => NEXMO_API_SECRET
        );
        $fields_string = http_build_query($fields);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
        $rs = curl_exec($ch);
        curl_close($ch);
        // TODO evaluate $rs and return false if sending not actually successful.
        return true;
    }//end twoFactorViaNexmoSMS()
}//end class

?>