<?php
declare(strict_types=1);

/**
 * @package PasswordHandler
 * @author  Alice Wonder <paypal@domblogger.net>
 * @license https://opensource.org/licenses/MIT MIT
 * @link    https://gitlab.com/Pipfrosch/passwordhandler
 *
 * This class implements the scrypt password hash algorithm using password normalization.
 *
 * This class uses the PHC String Format.
 *
 * @see https://github.com/P-H-C/phc-string-format/blob/master/phc-sf-spec.md
 *
 * The <id> string is the string `pipfrosch-scrypt`
 *
 * The <param> is a , delimited key=value set of parameters containing the following *required* parameters:
 *  m=integer         The memory difficulty in mb (multiply by 1024 to get kikibytes)
 *  t=timefactor      The exponent for CPU difficulty (2^t is the CPU difficulty)
 *  p=threads         The parallel difficulty
 *
 * Example <param>: m=24,t=4,p=2
 *
 * The <salt> is a variable-length salt. When the class defaults, it will be 16 byte hash
 *  The salt must be between 12 and 24 bytes base64 encoded or this class will refuse to hash (including just for
 *  password validation)
 *
 * The <hash> is a variable-length base64 encoded hash and must be at least 32 bytes but no larger than 64 bytes.
 *  The class defaults to 48 bytes.
 *
 * The actual scrypt hashing is performed by the PECL scrypt module.
 *
 * @see https://pecl.php.net/package/scrypt
 */

namespace Pipfrosch\PasswordHandler;

/**
 * PHC String Format wrapper to the scrypt password hashing algorithm.
 */
class PsfScrypt extends \Pipfrosch\PasswordHandler\Template\PsfCustomizer
{
    /**
     * The algo identifier.
     *
     * @var string
     */
    protected static $algoId = 'pipfrosch-scrypt';

    /**
     * The default time factor to use in 2019 with a $costFactor of 0
     *
     * @var int
     */
    protected static $defaultTimeFactor = 4;

    /**
     * The default memory factor to use in 2019 with a $costFactor of 0
     *
     * @var int
     */
    protected static $defaultMemoryFactor = 24;

    /**
     * Calculates the default memory cost. Doubles every three years.
     *
     * @param int $strengthFactor An integer to increase the default memory cost
     *                            when security requires it. 0 is standard,
     *                            1 is moderate, 2 is high.
     *
     * @return int The megabyte cost factor.
     */
    protected static function defaultMemoryCost(int $strengthFactor = 0): int
    {
        $floatYearFactor = (self::getCurrentYear() - 2019) / 3;
        switch ($strengthFactor) {
            case 2:
                $baseCost = self::$defaultMemoryFactor + (self::$defaultMemoryFactor >> 1);
                break;
            case 1:
                $baseCost = self::$defaultMemoryFactor + (self::$defaultMemoryFactor >> 2);
                break;
            default:
                $baseCost = self::$defaultMemoryFactor;
        }
        $floatAdjust = $baseCost * (2 ** $floatYearFactor);
        // We want to return an integer divisible by 4, well, I want that anyway...
        $n = intval($floatAdjust / 4);
        return $n << 2;
    }//end defaultMemoryCost()

    /**
     * Calculates the default time factor. Doubles every two years.
     *
     * @param int $strengthFactor An integer to increase the default time factor
     *                            when security requires it. 0 is standard,
     *                            1 is moderate, 2 is high.
     *
     * @return int The time factor for calculating CPU cost.
     */
    protected static function defaultTimeFactor(int $strengthFactor = 0): int
    {
        $yearFactor = self::getCurrentYear() - 2019;
        return self::$defaultTimeFactor + $strengthFactor + ($yearFactor >> 1);
    }//end defaultTimeFactor()

    /**
     * The method used to create the actual hash string. ALWAYS throws exception if a hash can not
     *  be generated from it.
     *
     * @param string $password   The password to be hashed.
     * @param string $salt       The salt to be used by the hashing algorithm.
     * @param array  $parameters An array of parameters needed by the hashing algorithm.
     *
     * @return string The hash of the password.
     *
     * @throws \InvalidArgumentException
     */
    protected static function generateHash(string $password, string $salt, array $parameters): string
    {
        if (! extension_loaded('scrypt')) {
            if (function_exists('sodium_memzero')) {
                sodium_memzero($password);
            }
            throw new \InvalidArgumentException('This class requires the PECL scrypt module be loaded.');
        }
        if (empty($password) || empty($salt)) {
            if (function_exists('sodium_memzero')) {
                sodium_memzero($password);
            }
            throw new \InvalidArgumentException('Neither the password not salt can be empty.');
        }

        if (! isset($parameters['p'])) {
            if (function_exists('sodium_memzero')) {
                sodium_memzero($password);
            }
            throw new \InvalidArgumentException('The p parameter (parallelism) was not defined.');
        }
        if (! is_numeric($parameters['p'])) {
            if (function_exists('sodium_memzero')) {
                sodium_memzero($password);
            }
            throw new \InvalidArgumentException('The p parameter must be an integer value of at least 1.');
        }
        $p = intval($parameters['p']);
        if ($p < 0) {
            if (function_exists('sodium_memzero')) {
                sodium_memzero($password);
            }
            throw new \InvalidArgumentException('The p parameter must be an integer value of at least 1.');
        }

        if (! isset($parameters['m'])) {
            if (function_exists('sodium_memzero')) {
                sodium_memzero($password);
            }
            throw new \InvalidArgumentException('The m parameter (memory difficulty factor) was not defined.');
        }
        if (! is_numeric($parameters['m'])) {
            if (function_exists('sodium_memzero')) {
                sodium_memzero($password);
            }
            throw new \InvalidArgumentException(
                'The memory difficulty parameter (m) must be an integer value of at least 1.'
            );
        }
        $m = intval($parameters['m']);
        if ($m < 0) {
            if (function_exists('sodium_memzero')) {
                sodium_memzero($password);
            }
            throw new \InvalidArgumentException(
                'The memory difficulty parameter (m) must be an integer value of at least 1.'
            );
        }
        $r = $m << 10;
        if ($r > PHP_INT_MAX / 128 / $p) {
            if (function_exists('sodium_memzero')) {
                sodium_memzero($password);
            }
            throw new \InvalidArgumentException('The memory difficulty is too large.');
        }

        if (! isset($parameters['t'])) {
            if (function_exists('sodium_memzero')) {
                sodium_memzero($password);
            }
            throw new \InvalidArgumentException('The t parameter (time cost factor) was not defined.');
        }
        if (! is_numeric($parameters['t'])) {
            if (function_exists('sodium_memzero')) {
                sodium_memzero($password);
            }
            throw new \InvalidArgumentException('The time factor (t) must be an integer value of at least 1.');
        }
        $t = intval($parameters['t']);
        if ($t < 0) {
            if (function_exists('sodium_memzero')) {
                sodium_memzero($password);
            }
            throw new \InvalidArgumentException('The time factor (t) must be an integer value of at least 1.');
        }
        $N = 1 << $t;
        if ($N > PHP_INT_MAX / 128 / $r) {
            if (function_exists('sodium_memzero')) {
                sodium_memzero($password);
            }
            throw new \InvalidArgumentException('The time factor is too large.');
        }

        if (! isset($parameters['hashLength'])) {
            if (function_exists('sodium_memzero')) {
                sodium_memzero($password);
            }
            throw new \InvalidArgumentException('The hashLength parameter was not defined.');
        }
        $exceptionMessage = 'The hashLength parameter must be an integer between ';
        $exceptionMessage .= self::$minHashLength;
        $exceptionMessage .= ' and ';
        $exceptionMessage .= self::$maxHashLength;
        $exceptionMessage .= ' inclusive.';
        if (! is_numeric($parameters['hashLength'])) {
            if (function_exists('sodium_memzero')) {
                sodium_memzero($password);
            }
            throw new \InvalidArgumentException($exceptionMessage);
        }
        $length = intval($parameters['hashLength']);
        if ($length < self::$minHashLength || $length > self::$maxHashLength) {
            if (function_exists('sodium_memzero')) {
                sodium_memzero($password);
            }
            throw new \InvalidArgumentException($exceptionMessage);
        }

        $binsalt = base64_decode($salt);
        if (! is_string($binsalt)) {
            if (function_exists('sodium_memzero')) {
                sodium_memzero($password);
            }
            throw new \InvalidArgumentException('The salt must be a base64 encode string.');
        }
        if (strlen($binsalt) < self::$minSaltLength) {
            if (function_exists('sodium_memzero')) {
                sodium_memzero($password);
            }
            throw new \InvalidArgumentException('The salt must be at least ' . self::$minSaltLength . ' bytes.');
        }
        if (strlen($binsalt) > self::$maxSaltLength) {
            if (function_exists('sodium_memzero')) {
                sodium_memzero($password);
            }
            throw new \InvalidArgumentException('The salt must be at most ' . self::$maxSaltLength . ' bytes.');
        }

        self::normalizePassword($password, $salt);
        $hash = scrypt($password, $salt, $N, $r, $p, $length);
        if (function_exists('sodium_memzero')) {
            sodium_memzero($password);
        }
        if (! ctype_xdigit($hash) || (strlen($hash) >> 1) !== $length) {
            throw new \InvalidArgumentException('Something went wrong, the hash could not be generated.');
        }
        $binhash = hex2bin($hash);
        return rtrim(base64_encode($binhash), '=');
    }//end generateHash()

    /**
     * Returns the default parameters for a specified $strengthFactor.
     *
     * @param int $strengthFactor Optional. 0, 1, or 2.
     *
     * @return array
     */
    public static function getDefaults(int $strengthFactor = 0): array
    {
        if ($strengthFactor < 0) {
            $strengthFactor = 0;
        } elseif($strengthFactor > 2) {
            $strengthFactor = 2;
        }
        $return = array();
        $return['memory_cost'] = self::defaultMemoryCost($strengthFactor) << 10;
        $return['time_cost'] = self::defaultTimeFactor($strengthFactor);
        $return['threads'] = 2 + $strengthFactor;
        return $return;
    }//end getDefaults()

    /**
     * Generate a password hash string. MUST throw an exception when it can not create a valid string.
     *
     * @param string $password       The password to hash.
     * @param int    $strengthFactor Optional. An integer to increase the default computational cost
     *                               when security requires it. 0 is paranoid, 1 is really paranoid,
     *                               2 is you have reason to believe the NSA may try to crack your
     *                               passwords if they get a dump of your hashes.
     *                               Defaults to 0.
     * @param array  $options        Optional. An array of parameters to override the class defaults.
     *                               memory_cost => The memory cost in kikibytes. When not defined, the
     *                                class default based on $strengthFactor and calendar year will
     *                                be used.
     *                               time_cost => The time factor. Increasing by one doubles the CPU
     *                                cost. When not defined, the class default based on
     *                                $strengthFactor and the calendaryear will be used.
     *                               threads => Integer defining the amount of parallelism. When not
     *                                defined, the class default based on the $strengthFactor is used.
     *                               saltsize => integer between 12 and 24 inclusive. Determines the
     *                                byte size of the salt.
     *                               hashsize => integer between 32 and 64 inclusive. Determines the
     *                                byte size of the generated hash.
     *
     * @return string A PHC String Format hash string.
     */
    public static function passwordHasher(string $password, int $strengthFactor = 0, array $options = []): string
    {
        if ($strengthFactor < 0) {
            $strengthFactor = 0;
        }
        if ($strengthFactor > 2) {
            $strengthFactor = 2;
        }
        $parameters = array();
        $psfParams  = array();

        $parameters['m'] = self::defaultMemoryCost($strengthFactor);
        if (isset($options['memory_cost']) && is_numeric($options['memory_cost'])) {
            $kikiMemoryCost = intval($options['memory_cost']);
            if ($kikiMemoryCost > 0) {
                $memcost = $kikiMemoryCost >> 10;
                if ($memcost > 0) {
                    $parameters['m'] = $memcost;
                }
            }
        }
        $psfParams[] = 'm=' . $parameters['m'];

        $parameters['t'] = self::defaultTimeFactor($strengthFactor);
        if (isset($options['time_cost']) && is_numeric($options['time_cost'])) {
            $timeCost = intval($options['time_cost']);
            if ($timeCost > 0) {
                $parameters['t'] = $timeCost;
            }
        }
        $psfParams[] = 't=' . $parameters['t'];

        $parameters['p'] = 2 + $strengthFactor;
        if (isset($options['threads']) && is_numeric($options['threads'])) {
            $threads = intval($options['threads']);
            if ($threads > 0) {
                $parameters['p'] = $threads;
            }
        }
        $psfParams[] = 'p=' . $parameters['p'];

        $parameters['hashLength'] = self::$hashLength;
        if (isset($options['hashsize']) && is_numeric($options['hashsize'])) {
            $hashLength = intval($options['hashsize']);
            if ($hashLength >= self::$minHashLength && $hashLength <= self::$maxHashLength) {
                $parameters['hashLength'] = $hashLength;
            }
        }

        $saltsize = self::$saltLength;
        if (isset($options['saltsize']) && is_numeric($options['saltsize'])) {
            $saltLength = intval($options['saltsize']);
            if ($saltLength >= self::$minSaltLength && $saltLength <= self::$maxSaltLength) {
                $saltsize = $saltLength;
            }
        }
        $salt = self::generateSalt($saltsize);
        try {
            $hash = self::generateHash($password, $salt, $parameters);
        } catch (\InvalidArgumentException $e) {
            if (function_exists('sodium_memzero')) {
                sodium_memzero($password);
            }
            throw new \InvalidArgumentException($e->getMessage());
        }
        return self::formatHashString(self::$algoId, $psfParams, $salt, $hash);
    }//end passwordHasher()

    /**
     * Verify a password against the provided hash string.
     *
     * @param string $password   The password to validate.
     * @param string $hashString The hash string to validate against.
     *
     * @return bool True if the password validates, otherwise false.
     */
    public static function passwordVerify(string $password, string $hashString): bool
    {
        if (empty($password)) {
            return false;
        }
        $hashString = substr($hashString, 1);
        list ($algo, $params, $salt, $hash) = explode('$', $hashString);
        if ($algo !== self::$algoId) {
            if (function_exists('sodium_memzero')) {
                sodium_memzero($password);
            }
            return false;
        }
        if (empty($params) || empty($salt) || empty($hash)) {
            if (function_exists('sodium_memzero')) {
                sodium_memzero($password);
            }
            return false;
        }
        $binsalt = base64_decode($salt);
        if (! is_string($binsalt)) {
            if (function_exists('sodium_memzero')) {
                sodium_memzero($password);
            }
            return false;
        }
        if (strlen($binsalt) < self::$minSaltLength || strlen($binsalt) > self::$maxSaltLength) {
            if (function_exists('sodium_memzero')) {
                sodium_memzero($password);
            }
            return false;
        }
        $binhash = base64_decode($hash);
        if (! is_string($binhash)) {
            if (function_exists('sodium_memzero')) {
                sodium_memzero($password);
            }
            return false;
        }
        $length = strlen($binhash);
        if ($length < self::$minHashLength || $length > self::$maxHashLength) {
            if (function_exists('sodium_memzero')) {
                sodium_memzero($password);
            }
            return false;
        }
        $parameters = array();
        $options = explode(',', $params);
        foreach ($options as $option) {
            list ($key, $value) = explode('=', $option);
            if (! empty($key) && ! empty($value)) {
                $parameters[$key] = $value;
            }
        }
        $parameters['hashLength'] = $length;
        try {
            $testHash = self::generateHash($password, $salt, $parameters);
        } catch (\InvalidArgumentException $e) {
            if (function_exists('sodium_memzero')) {
                sodium_memzero($password);
            }
            return false;
        }
        return hash_equals($hash, $testHash);
    }//end passwordVerify()
}//end class

?>