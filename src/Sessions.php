<?php
declare(strict_types=1);

/**
 * PHP session handling. Not yet ready for use.
 *
 * @package PasswordHandler
 * @author  Alice Wonder <paypal@domblogger.net>
 * @license https://opensource.org/licenses/MIT MIT
 * @link    https://gitlab.com/Pipfrosch/passwordhandler
 *
 * @see https://www.php.net/manual/en/class.sessionhandlerinterface.php
 * @see https://www.php.net/manual/en/class.sessionidinterface.php
 *
 * This class is probably suitable for the typical web site in the scale of most
 *  small businesses, but I doubt it is suitable for something with the scale of,
 *  say, Twitter. I would LOVE to have an open source PHP session manager that
 *  works well on large scale sites, maybe if your site is too large for this
 *  class you can fund a programmer to develop one.
 *
 * This class uses a database backend and requires the PDO driver.
 * This class allows the optional use of a PSR-16 cache for reading session data,
 *  which will improve performance as it means database queries are typically only
 *  needed when session data needs to be written.
 *
 * Unit tests not yet written
 */

namespace Pipfrosch\PasswordHandler;

/**
 * The class
 */
class Sessions implements \SessionHandlerInterface, \SessionIdInterface, \Pipfrosch\PasswordHandler\SessionAccountInterface
{
    /**
     * Object with database connection parameters.
     *
     * @var \stdClass
     */
    protected $dsn;

    /**
     * The PDO object. Set by the open() method.
     *
     * @var null|\PDO
     */
    protected $pdo = null;

    /**
     * Optional PSR-16 cache object.
     *
     * @see https://www.php-fig.org/psr/psr-16/
     *
     * @var null|\Psr\SimpleCache\CacheInterface
     */
    protected $cache = null;

    /**
     * Encryption key
     *
     * @var null|string
     */
    protected $encryptionKey = null;

    /**
     * Whether or not AES should be used. Set by the open() method if sodium
     *  extension installed and CPU supports AES-NI.
     *
     * @var bool
     */
    protected $aesgcm = false;

    /**
     * The encipherment nonce
     *
     * @var null|string
     */
    protected $nonce = null;

    /**
     * The cache prefix to use. The cache key is this prefix followed by Session ID.
     *
     * @var string
     */
    protected $cachePrefix = 'sessioncache-';

    /**
     * Maximum days a session can be valid for before it is rejected as too old.
     *
     * @var int
     */
    protected $maxDays = 14;

    /**
     * IP address location of client. Set by constructor.
     *
     * @var string
     */
    protected $location = 'unknown';

    /**
     * Validate the Session ID.
     *
     * This class requires hex encoded 16-byte Session ID. Thus it will always be
     *  32 character hex encoded if valid.
     *
     * @param string $sid The session ID to validate.
     *
     * @return bool
     */
    protected function validateSessionId(string $sid): bool
    {
        if (strlen($sid) !== 32 || ! ctype_xdigit($sid)) {
            return false;
        }
        return true;
    }//end validateSessionId()

    /**
     * Encrypts the session data that is to be written.
     *
     * @param string $data The data to be enciphered.
     *
     * @return string The enciphered data.
     */
    protected function encryptData(string $data): string
    {
        if (is_null($this->encryptionKey)) {
            return $data;
        }
        $oldnonce = $this->nonce;
        if (is_null($this->nonce)) {
            $this->nonce = random_bytes(16);
        } else {
            sodium_increment($this->nonce);
        }
        if ($oldnonce === $this->nonce) {
            throw new \InvalidArgumentException('Duplicate nonce use');
        }
        $obj = new \stdClass;
        $obj->nonce = $this->nonce;
        if ($this->aesgcm) {
            $obj->cipher = 'aesgcm';
            $obj->ciphertext = sodium_crypto_aead_aes256gcm_encrypt(
                $data,
                $obj->nonce,
                $obj->nonce,
                $this->encryptionKey
            );
        } else {
            $obj->cipher = 'chacha20';
            $obj->ciphertext = sodium_crypto_aead_chacha20poly1305_ietf_encrypt(
                $data,
                $obj->nonce,
                $obj->nonce,
                $this->encryptionKey
            );
        }
        return serialize($obj);
    }//end encryptData()

    /**
     * Deciphers the encrypted session data.
     *
     * @param string $serialized The enciphered session data.
     *
     * @return string The deciphered session data.
     */
    protected function decryptData(string $serialized): string
    {
        if (is_null($this->encryptionKey)) {
            return $serialized;
        }
        try {
            $obj = unserialize($serialized);
        } catch (\Error $e) {
            return '';
        }
        if (! isset($obj->nonce) || ! isset($obj->cipher) || ! isset($obj->ciphertext)) {
            return '';
        }
        switch ($obj->cipher) {
            case 'chacha20':
                try {
                    $data = sodium_crypto_aead_chacha20poly1305_ietf_decrypt(
                        $obj->ciphertext,
                        $obj->nonce,
                        $obj->nonce,
                        $this->encryptionKey
                    );
                } catch (\Error $e) {
                    return '';
                }
                break;
            default:
                try {
                    $data = sodium_crypto_aead_aes256gcm_decrypt(
                        $obj->ciphertext,
                        $obj->nonce,
                        $obj->nonce,
                        $this->encryptionKey
                    );
                } catch (\Error $e) {
                    return '';
                }
        }
        if (is_string($data)) {
            return $data;
        }
        return '';
    }//end decryptData()

    /**
     * If a row for the session does not already exist
     *  one is created. Called by the write() method.
     *
     * @param string $sid The session ID to create a row for.
     *
     * @return void
     */
    protected function createSessionRow($sid): void
    {
        if (is_null($this->pdo)) {
            return;
        }
        if (! $this->validateSessionId($sid)) {
            return;
        }
        $sql = 'INSERT INTO sessiondb(sid,location) VALUES(?,?)';
        $q = $this->pdo->prepare($sql);
        $arr = array($sid, $this->location);
        $q->execute($arr);
    }//end createSessionRow()

    /**
     * Start a fresh session with unique session ID
     *
     * @param null|int $userid       The userid. Defaults to null.
     * @param string   $accountType  Defaults to null.
     *
     * @return string|bool The session ID of new session, or false on failure.
     */
    protected function sessionFreshStart($userid = null, $accountType = null)
    {
        if (is_null($this->pdo)) {
            return false;
        }
        $success = false;
        $attempt = 0;
        while (! $success) {
            $sid = $this->create_sid();
            try {
                $this->pdo->beginTransaction();
                $sql = 'SELECT count(created) AS count FROM sessiondb WHERE sid=?';
                $q = $this->pdo->prepare($sql);
                $arr = array($sid);
                $q->execute($arr);
                if ($rs = $q->fetchAll()) {
                    if (isset($rs[0]->count) && $rs[0]->count === 0) {
                        if (is_null($userid)) {
                            $sql = 'INSERT INTO sessiondb(sid,location) VALUES (?,?)';
                            $arr[] = $this->location;
                        } else {
                            if (empty($accountType)) {
                                $sql = 'INSERT INTO sessiondb(sid,userid,location) VALUES (?,?,?)';
                                $arr[] = $userid;
                                $arr[] = $this->location;
                            } else {
                                $sql = 'INSERT INTO sessiondb(sid,userid,accounttype,location) VALUES (?,?,?,?)';
                                $arr[] = $userid;
                                $arr[] = $accountType;
                                $arr[] = $this->location;
                            }
                        }
                        $q = $this->pdo->prepare($sql);
                        $q->execute($arr);
                        $success = true;
                    } else {
                        $attempt++;
                    }
                } else {
                    $attempt++;
                }
            } catch (\Exception $e) {
                $this->pdo->rollBack();
                $attempt++;
            }
            if ($attempt > 4) {
                break;
            }
        }
        if ($success) {
            $this->pdo->commit();
        } else {
            return false;
        }
        if (isset($sid)) {
            ini_set('session.use_strict_mode', '0');
            if (session_status() === PHP_SESSION_ACTIVE) {
                session_abort();
            }
            session_id($sid);
            session_start();
            return $sid;
        }
        return false;
    }//end sessionFreshStart()

    /**
     * Creates a 16 byte session ID.
     *
     * The purpose of the filter is that I am paranoid about what happens if the
     *  random_bytes() function is not truly random on some platforms. It should
     *  make it still difficult to predict even if there is a flaw.
     *
     * @return string The generated session ID.
     */
    public function create_sid(): string
    {
        $rnd = random_bytes(16);
        $n = random_int(0, 15);
        $filter = str_shuffle(substr(hash('sha256', $rnd, true), $n, 16));
        return bin2hex($rnd^$filter);
    }//end create_sid()

    /**
     * Opens the connection. The parameters are not used but are here because the interface specifies them.
     *
     * @param string $savePath    This parameter is here only for interface compliance.
     * @param string $sessionName This parameter is here only for interface compliance.
     *
     * @return bool
     */
    public function open(string $savePath, string $sessionName): bool
    {
        /* DSN */
        $driver = 'mysql';
        $dbname = 'phpsessions';
        $host = 'localhost';
        $dbuser = 'dbuser';
        $dbpass = 'dbpass';
        if (isset($this->dsn->driver) && in_array($this->dsn->driver, array('mysql', 'pgsql'))) {
            $driver = $this->dsn->driver;
        }
        if (isset($this->dsn->dbname) && is_string($this->dsn->dbname)) {
            $dbname = $this->dsn->dbname;
        }
        if (isset($this->dsn->host) && is_string($this->dsn->host)) {
            $host = $this->dsn->host;
        }
        if (isset($this->dsn->user) && is_string($this->dsn->user)) {
            $dbuser = $this->dsn->user;
        }
        if (isset($this->dsn->password) && is_string($this->dsn->password)) {
            $dbpass = $this->dsn->password;
        }
        if (extension_loaded('sodium')) {
            $hash = hash_hmac('sha256', hash('sha256', $dbname . $dbuser, true), hash('sha256', $dbpass, true));
            $this->encryptionKey = substr($hash, 4, 16);
            $this->aesgcm = sodium_crypto_aead_aes256gcm_is_available();
        }

        $connect = array();
        $connect[] = $driver . ':dnname=' . $dbname;
        $connect[] = 'host=' . $host;
        $connect[] = 'charset=utf8';
        if ($driver !== 'mysql') {
            $connect[] = 'user=' . $dbuser;
            $connect[] = 'password=' . $dbpass;
        }
        $string = implode(';', $connect);
        try {
            if ($driver === 'mysql') {
                $pdo = new \PDO($string, $dbuser, $dbpass);
            } else {
                $pdo = new \PDO($string);
            }
            $pdo->setAttribute(\PDO::ATTR_DEFAULT_FETCH_MODE, \PDO::FETCH_OBJ);
        } catch (\PDOException $silent) {
            return false;
        }
        if (function_exists('sodium_memzero')) {
            sodium_memzero($dbpass);
        }
        return true;
    }//end open()

    /**
     * Does not actually do anything, but interface requires it.
     *
     * @return true
     */
    public function close(): bool
    {
        return true;
    }//end close()

    /**
     * Returns the serialized data associated with a session ID.
     *
     * @param string $sid The session ID.
     *
     * @return string The serialized data,
     */
    public function read(string $sid): string
    {
        if (! $this->validateSessionId($sid) || is_null($this->pdo)) {
            return '';
        }
        // ten minutes
        $ts = time();
        $now = date('Y-m-d H:i:s', $ts);
        $expireCheck = date('Y-m-d H:i:s', $ts + 600);
        // for cache
        $key = $this->cachePrefix . $sid;
        if (! is_null($this->cache)) {
            $data = $this->cache->get($key);
            if (! empty($data)) {
                return $this->decryptData($data);
            }
        }
        // get it from the database
        $sql = 'SELECT serialized AS data,expires FROM sessiondb WHERE sid=? AND expires > ?';
        $q = $this->pdo->prepare($sql);
        $arr = array($sid, $now);
        $q->execute($arr);
        if ($rs = $q->fetchAll()) {
            if (isset($rs[0]->data)) {
                if (! is_null($this->cache)) {
                    $cacheLife = 900;
                    if ($rs[0]->expires < $expireCheck) {
                        $cacheLife = 300;
                    }
                    $this->cache->set($key, $rs[0]->data, $cacheLife);
                }
                return $this->decryptData($rs[0]->data);
            }
        }
        return '';
    }//end read()

    /**
     * Write the serialized session data.
     *
     * @param string $sid  The session ID.
     * @param string $data The serialized session data.
     *
     * @return bool
     */
    public function write(string $sid, string $data): bool
    {
        if (! $this->validateSessionId($sid) || is_null($this->pdo)) {
            return false;
        }
        // make sure the row exists
        $this->createSessionRow($sid);
        // ten minutes
        $timeToLive = time() + 600;
        $expires = date('Y-m-d H:i:s', $timeToLive);
        
        $data = $this->encryptData($data);
        $ts = time();
        $access = date('Y-m-d H:i:s', $ts);
        $ex = $ts + (3 * 86400);
        $expires = date('Y-m-d H:i:s', $ex);
        try {
            $this->pdo->beginTransaction();
            $sql = 'UPDATE sessiondb SET serialized=? WHERE sid=? AND expires > $expires';
            $q = $this->pdo->prepare($sql);
            $arr = array($data, $access, $sid, $expires);
            $q->execute($arr);
            $this->pdo->commit();
        } catch (\Exception $e) {
            $this->pdo->rollBack();
            return false;
        }
        // cache it for next access
        if (! is_null($this->cache)) {
            $key = $this->cachePrefix . $sid;
            $this->cache->set($key, $data, 300);
        }
        return true;
    }//end write()

    /**
     * Destroys saved session data. Actually only deletes what is cached and
     *  sets the expires to very near future.
     *
     * @param string $sid The session ID.
     *
     * @return bool
     */
    public function destroy(string $sid): bool
    {
        if (! $this->validateSessionId($sid) || is_null($this->pdo)) {
            return false;
        }
        // ten minutes
        $timeToLive = time() + 600;
        $expires = date('Y-m-d H:i:s');
        try {
            $this->pdo->beginTransaction();
            $sql = 'UPDATE sessiondb SET expires=?,userid=NULL,accounttype=NULL WHERE sid=?';
            $q = $this->pdo->prepare($sql);
            $arr = array($expires, $sid);
            $q->execute($arr);
            $this->pdo->commit();
        } catch (\PDOException $e) {
            $this->pdo->rollBack();
            return false;
        }
        // delete cached
        if (! is_null($this->cache)) {
            $key = $this->cachePrefix . $sid;
            $this->cache->delete($key);
        }
        return true;
    }//end destroy()

    /**
     * Garbage Collection
     *
     * @param int $maxlifetime The time in seconds a session is to be alive for.
     *
     * @return bool
     */
    public function gc(int $maxlifetime): bool
    {
        if (is_null($this->pdo)) {
            return false;
        }
        $time = time() - $maxlifetime;
        $date = date('Y-m-d H:i:s', $time);
        $sql = 'SELECT sid FROM sessiondb WHERE created < ?';
        $arr = array($date);
        $q = $this->pdo->prepare($sql);
        $q->execute($arr);
        if ($rs = $q->fetchAll()) {
            foreach ($rs as $result) {
                $this->destroy($result->sid);
            }
        }
        $time = time();
        $date = date('Y-m-d H:i:s', $time);
        $sql = 'DELETE FROM sessiondb WHERE expires < ?';
        $arr = array($date);
        $q = $this->pdo->prepare($sql);
        $q->execute($arr);
        return true;
    }//end gc()

    /**
     * Regenerate a session id. If the previous session ID was valid and has data,
     *  it is preserved in the new session ID. Otherwise a fresh session ID is
     *  used with no data associated with it.
     *
     * @return void
     */
    public function sessionRegenerate(): void
    {
        if (is_null($this->pdo)) {
            return;
        }
        // don't regenerate soon to expire
        $ts = time() + 1200;
        $expires = date('Y-m-d H:i:s', $ts);
        $oldid = session_id();
        if ($this->validateSessionId($oldid)) {
            $sql = 'SELECT userid,serialized FROM sessiondb WHERE sid=? AND expires > ?';
            $q = $this->pdo->prepare($sql);
            $arr = array($oldid, $expires);
            $q->execute($arr);
            if ($rs = $q->fetchAll()) {
                if (isset($rs[0])) {
                    $userid = $rs[0]->userid;
                    $data = $rs[0]->serialized;
                }
            }
            if (isset($userid) && isset($data)) {
                $newid = $this->sessionFreshStart($userid);
                if (is_string($newid)) {
                    $this->write($newid, $data);
                }
                $this->destroy($oldid);
                return;
            }
        }
        $this->sessionFreshStart();
    }//end sessionRegenerate()

    /**
     * Start a session.
     *
     * If a session already exists but is more that 69 days old, a fresh
     *  session is created.
     * If a session already exists but is more than three days, a fresh
     *  session is created with old serialized data.
     *
     * @return void
     */
    protected function sessionStart(): void
    {
        if (is_null($this->pdo)) {
            throw new \InvalidArgumentException('PDO connection for session database has failed.');
        }
        $ts = time();
        $genesis = $ts - ($this->maxDays * 86400);
        $minCreated = date('Y-m-d H:i:s', $genesis);
        $exodus = $ts - (15 * 60);
        $regenTrigger = date('Y-m-d H:i:s', $exodus);
        if (session_status() !== PHP_SESSION_ACTIVE) {
            session_start();
        }
        // deal with old sessions
        $sid = session_id();
        if ($this->validateSessionId($sid)) {
            $sql = 'SELECT created,location FROM sessiondb WHERE sid=?';
            $arr = array($sid);
            $q = $this->pdo->prepare($sql);
            $q->execute($arr);
            if ($rs = $q->fetchAll()) {
                if (isset($rs[0])) {
                    if ($rs[0]->created < $minCreated) {
                        $this->sessionFreshStart();
                        return;
                    }
                    if ($rs[0]->created < $regenTrigger) {
                        $this->sessionRegenerate();
                        return;
                    }
                    if (strcmp($rs[0]->location, $this->location) !== 0) {
                        $this->sessionRegenerate();
                        return;
                    }
                    // login pages can define this to force a regeneration of
                    //  session ID when the login page itself loads so that a
                    //  stolen session ID never matches the session ID that is
                    //  then used on the login page.
                    if (defined('LOGIN_PAGE')) {
                        $this->sessionRegenerate();
                        return;
                    }
                }
            }
        }
        $this->sessionFreshStart();
    }//end sessionStart()

    /**
     * User login. Whenever a user login is successful, calling this function
     *  will generate a fresh session id.
     *
     * Associates userid with session
     *
     * @param int         $userid      The userid.
     * @param null|string $accountType Optional. The account type.
     *
     * @return void
     */
    public function userLogin(int $userid, $accountType = null): void
    {
        $oldid = session_id();
        $olddata = '';
        if ($this->validateSessionId($oldid)) {
            $olddata = $this->read($oldid);
        }
        $this->sessionFreshStart($userid, $accountType);
        $newid = session_id();
        if (! empty($olddata)) {
            $this->write($newid, $olddata);
        }
        // nuke old session
        if (! empty($oldid)) {
            $this->destroy($oldid);
        }
    }//end userLogin()

    /**
     * Get userid
     *
     * @return null|int The user ID, null for not logged in.
     */
    public function getUserID()
    {
        if (is_null($this->pdo)) {
            throw new \InvalidArgumentException('PDO connection is not available.');
        }
        $sid = session_id();
        if ($this->validateSessionId($sid)) {
            $sql = 'SELECT userid FROM sessiondb WHERE sid=? AND userid NOT NULL';
            $arr = array($sid);
            $q = $this->pdo->prepare($sql);
            $q->execute($arr);
            if ($rs = $q->fetchAll()) {
                if (isset($rs[0]->userid)) {
                    return intval($rs[0]->userid);
                }
            }
        }
        return null;
    }//end getUserID()

    /**
     * Get account type
     *
     * @return string|null
     */
    public function getAccountType()
    {
        if (is_null($this->pdo)) {
            throw new \InvalidArgumentException('PDO connection is not available.');
        }
        $sid = session_id();
        if ($this->validateSessionId($sid)) {
            $sql = 'SELECT accounttype FROM sessiondb WHERE sid=? AND userid NOT NULL';
            $arr = array($sid);
            $q = $this->pdo->prepare($sql);
            $q->execute($arr);
            if ($rs = $q->fetchAll()) {
                if (isset($rs[0]->accounttype) && ! empty($rs[0]->accounttype)) {
                    return (string) $rs[0]->accounttype;
                }
            }
        }
        return null;
    }//end getAccountType()

    /**
     * User logout. Whenever a user logs out, calling this function destroys the old
     *  session data and generates a fresh session id.
     *
     * @return void
     */
    public function userLogout(): void
    {
        $oldid = session_id();
        $this->sessionFreshStart();
        if (! empty($oldid)) {
            $this->destroy($oldid);
        }
    }//end userLogout()

    /**
     * Global user logout
     *
     * Destroys all sessions for a given userid
     *
     * @return bool
     *
     */
    public function globalUserLogout(): bool
    {
        if (is_null($this->pdo)) {
            return false;
        }
        $userid = $this->getUserID();
        if ($userid === 0) {
            return false;
        }
        // get all sessions
        $sql = 'SELECT sid FROM sessiondb WHERE userid=?';
        $q = $this->pdo->prepare($sql);
        $arr = array($userid);
        $q->execute($arr);
        if ($rs = $q->fetchAll()) {
            foreach ($rs as $result) {
                $this->destroy($result->sid);
            }
        }
        return true;
    }//end globalUserLogout()

    /**
     * The constructor.
     *
     * @param \stdClass                            $dsn   The database connection information.
     * @param null|\Psr\SimpleCache\CacheInterface $cache Optional. A cache object that implements
     *                                                    PSR-16.
     */
    public function __construct(\stdClass $dsn, $cache = null)
    {
        $this->dsn = $dsn;
        if (! is_null($cache)) {
            $this->cache = $cache;
        }
        if (isset($_SERVER['REMOTE_ADDR'])) {
            $this->location = $_SERVER['REMOTE_ADDR'];
        }
        ini_set('session.use_only_cookies', '1');
        ini_set('session.use_strict_mode', '1');
        ini_set('session.cookie_secure', '1');
        ini_set('session.cookie_httponly', '1');
        // garbare collection
        ini_set('session.gc_probability', '1');
        ini_set('session.gc_divisor', '100');
        $gcSeconds = (string) ($this->maxDays * 86400);
        ini_set('session.gc_maxlifetime', $gcSeconds);
        // below will throw exception if PDO connection doesn't work
        session_set_save_handler($this, true);
        $this->sessionStart();
    }//end __construct()
}//end class

?>