<?php
declare(strict_types=1);

/**
 * PHP session account methods.
 *
 * @package PasswordHandler
 * @author  Alice Wonder <paypal@domblogger.net>
 * @license https://opensource.org/licenses/MIT MIT
 * @link    https://gitlab.com/Pipfrosch/passwordhandler
 *
 * The purpose of this interface is to define methods related to user accounts
 *  and session management.
 *
 * The idea is that a class that implements \SessionHandlerInterface and \SessionIdInterface
 *  could also implement this interface.
 *
 * It would be my preference to use a PHP-FIG maintained interface for this but one
 *  does not yet exist.
 */

namespace Pipfrosch\PasswordHandler;

/**
 * The interface
 */
interface SessionAccountInterface
{
    /**
     * Generate a fresh session ID.
     *
     * @return void
     */
    public function sessionRegenerate();

    /**
     * Set the user ID and account type on user login.
     *
     * This method should create a fresh session ID before setting the
     * userid and account type.
     *
     * @param int         $userid      The ID number associated with the user account.
     * @param null|string $accountType Optional. The type of account. Defaults to null.
     *
     * @return void
     */
    public function userLogin(int $userid, $accountType = null);

    /**
     * Log a user out. This should unset any session variables related to the user id and
     *  account type a trigger a session ID regeneration.
     *
     * @return void
     */
    public function userLogout();

    /**
     * Global user logout. Logs out all sessions associated with the logged in user.
     *
     * @return bool
     */
    public function globalUserLogout();

    /**
     * Get the user ID associated with the session. Should return NULL if the
     *  user is not logged in.
     *
     * @return null|int The user id.
     */
    public function getUserID();

    /**
     * Get the account type associated with the session. Should return NULL if
     *  the user is not logged in, may return NULL if the user is logged in but
     *  is not a privileged user. Should return a string for privileged account
     *  types.
     *
     * @return null|string The account type.
     */
    public function getAccountType();
}//end interface

?>