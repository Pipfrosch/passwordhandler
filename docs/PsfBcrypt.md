PSF Bcrypt Password Hasher / Validator
======================================

The intent is to use this from within the CoreWrapper class by specifying the
integer `52` as the first argument when instantiating the CoreWrapper class.

This class exists for those who prefer to use bcrypt as their password hashing
algorithm but do not want passwords longer than 72 characters to be truncated.

This class generates a password hash string using what is known as the
[PHC String Format](https://github.com/P-H-C/phc-string-format/blob/master/phc-sf-spec.md)
(PSF). As far as I know, PSF is not on track to become an IETF standard, but
it should be seen as the logical successor to the
[Modular Crypt Format](https://passlib.readthedocs.io/en/stable/modular_crypt_format.html)
(MCF) that many of us have become intimately familiar with.

This is what a typical pipfrosch-bcrypt hash looks like in my implementation:

    $psf-bcrypt$t=14$0kWGLtBu2BAGH7PdCm6k/w$QcjbbHETBZAmj/PI+q67tIm3foyH0JodcqmSdKBP9CdKb0w5QC4d9OhMIeIwc9K0

The above broken into it's components:

* `psf-bcrypt` — That is the `<id>` string, identifying the hash algorithm
  used.
* `t=14` — That is the `<parameters>` string, containing the computational
  cost parameter needed to regenerate the identical hash from the same
  password.
* `0kWGLtBu2BAGH7PdCm6k/w` — That is the `<salt>` string. This class requires
  the salt be a minimum of 12 bytes that are base64 encoded. The default in this
  class is 16 bytes but the class will validate passwords against a hash that
  only used 12 bytes or as large as 24 bytes.
* `QcjbbHET[[SNIP]]IeIwc9K0` — That is the actual hash of the password. With
  `psf-bcrypt` I am specifying base64 encoding. I am also specifying a minimum
  hash length of 32 bytes (256 bits) and a maximum of 64 bytes (512 bits). The
  class default is 48 bytes (384 bits).

Please note that when present, the `=` padding on the right side of a base64
encoded string should be removed. Also note that with PSF (and MCF) the `$`
separator is present at the beginning of a field, thus the hash which is the
last field is not followed by a `$`.


Password Hashing Note
---------------------

This implementation hashes the password once with SHA-384 (and the salt) before
sending the password to the Scrypt function. This is done so that regardless of
what characters are in the password, it is only printable 7-bit ASCII when sent
to the `crypt()` function for hashing, avoiding any possible bugs related to
the password length or control characters or multi-byte characters.

The Bcrypt implementation here is thus not compatible with other Bcrypt
implementations but that is okay because it has a unique identifier so it is
clear that hashes made with this class will not validate passwords when using
a different Bcrypt implementation.

It does however create a sort-of lock-in, you will need to use this class to
validate passwords against the hashes.


Using the PsfBcrypt Class
-------------------------

This class only contains static methods so it should not be instantiated. There
is no class constructor.

Make sure the class file is where your [PSR-4](https://www.php-fig.org/psr/psr-4/)
autoloader can find it, or explicitly require the class file in your PHP
script, then:

    use \Pipfrosch\PasswordHandler\PsfBcrypt as PsfBcrypt;

The class has two public methods, one for generating the `psf-bcrypt` hash
string and one for validating a password against a `psf-bcrypt` hash string.


Create a PSF Bcrypt hash string
-------------------------------

To create a PSF hash string:

    $hashString = PsfBcrypt::passwordHasher(string $password[, int $strengthFactor[, array $options]]): string

The variable `$hashString` will now contain a string in PSF format containing
the default parameters, a generated salt unique to the hash, and the resulting
hash. The options `$strengthFactor` and `$options` are described in the
CoreWrapper documentation.

That static method takes the following arguments:

### `string $password` — Required

The password to be hashed. No validation is done as to the quality of the
password, that is beyond the scope of this class.

### `int $strengthFactor` — Optional

An integer value between `0` and `2` inclusive. Used to allow the class to
determine defaults used for the scrypt hashing parameters. For a more
detailed explanation of this parameter, see the CoreWrapper documentation.

### `array $options` — Optional

Used to specify the computational cost. For a more detailed explanation, see
the CoreWrapper documentation. Please note that specifying options with the
`$options` array will override defaults set as a result of the
`$strengthFactor` option.

### HASH LENGTH NOTE

If you do not want a 48 byte hash, you can add an option `hashLength` to the
`$options` array to specify a different length. It must be an integer between
32 and 64 inclusive.


Validate a Password Against a PSF Scrypt hash string
----------------------------------------------------

To validate a password against a PSF Scrypt hash string:

    $test = PsfBcrypt::passwordVerify($password, $hashString);

The variable `$test` will be a boolean `true` if the password validates and a
boolean `false` otherwise.

The static method takes two arguments, both are requires:

### `string $password` — Required

The password you wish to verify against the hash.

### `string $hashString` — Required

The PSF formatted hash string containing the parameters, salt, and password
hash.

### `passwordVerify()` \InvalidArgumentException Note

This method will under certain circumstances throw an `\InvalidArgumentException`:

* If the `$hashString` is not for a `psf-bcrypt` hash.
* If the `$hashString` is not complete.
* If the specified salt is not base64 encoded and at least 12 bytes or is
  larger than 24 bytes.
* If the hash component is not base64 encoded and at least 32 bytes or is
  larger than 64 bytes.

It would be a good idea to make sure your code handles exceptions.

Throwing an `\InvalidArgumentException` may not technically be the right thing
to do when the needed PECL module is not loaded, I need to look into that.

