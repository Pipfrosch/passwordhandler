PHP Core Password Function Wrapper
==================================

In the PHP manual section on [Password Hashing](https://www.php.net/manual/en/book.password.php)
there are four functions they classify as Password Hasing Functions:

* [password_get_info](https://www.php.net/manual/en/function.password-get-info.php)
* [password_needs_rehash](https://www.php.net/manual/en/function.password-needs-rehash.php)
* [password_hash](https://www.php.net/manual/en/function.password-hash.php)
* [password_verify](https://www.php.net/manual/en/function.password-verify.php)

This class serves the purpose of providing wrappers to those classes that work
in a very similar fashion but with better coverage and implementation.


Instantiating the Wrapper
-------------------------

    use \Pipfrosch\PasswordHandler\CoreWrapper as CoreWrapper;
    $CW = new CoreWrapper()

To use the wrappers:

    $CW->password_get_info(string $hash): array
    $CW->password_needs_rehash(string $hash[, int $algo [, array $options]]): bool
    $CW->password_hash(string $password[, int $algo [, array $options]]): string
    $CW->password_verify(string $password, string $hash): bool

The argument syntax is the same as the core functions they are intended to be
substitute. Note the constructor also takes some optional arguments, and that
`password_hash()` supports a couple hash string formats not available in the
core function, and the `password_verify()` function supports several hash
string functions not supported in the core function.


Hashing Algorithms
------------------

The following hashing algorithms are supported by CoreWrapper:

* Bcrypt -- Algorithm ID `1` -- This algorithm is supported on all installs of
  PHP 7.0+ and is the default.
* Argon2i -- Algorithm ID `2` -- This algorithm is supported in PHP 7.2+ that
  is compiled against libargon2 and in PHP 7.0+ that has the sodium extension
  loaded.
* Argon2id -- Algorithm ID `3` -- This algorithm is supported in PHP 7.3+ that
  is compiled against libargon2 and in PHP 7.0+ that has the sodium extension
  loaded.
* pipfrosch-scrypt -- Algorithm ID `51` This algorithm is supported in PHP 7.0+
  that has the PECL scrypt extension loaded.
* pipfrosch-bcrypt -- Algorithm ID `52` This algorithm is supported on all
  installs of PHP 7.0+. The only benefit to this algorithm over the standard
  bcrypt is that passwords longer than 72 characters are not effectively
  truncated.

There are no plans to add additional hash algorithm support *unless* additional
hashing algorithms are added to PHP itself. The `pipfrosch-scrypt` algorithm
was added purely to give easy access to `scrypt()` and the `pipfrosch-bcrypt`
algorithm was added purely to give a `bcrypt` implementation that does not
truncate passwords beyond 72 characters in length.

### Algorithm ID Integers

PHP defines the constant `PASSWORD_BCRYPT` to 1. If PHP 7.2+ has been compiled
against libargon2 then the constant `PASSWORD_ARGON2I` is defined to 2. If PHP
7.3+ has been compiled against libargon2 then the constant `PASSWORD_ARGON2ID`
is defined to 3.

As the `pipfrosch-scrypt` and `pipfrosch-bcrypt` algorithms are custom to this
set of classes, they do not have PHP constants defined to correspond with their
custom Algorithm ID.

The integer Algorithm ID is used as the optional first argument to the class
constructor to set the default hashing algorithm, the optional second argument
to the `$CW->password_hash()` wrapper when you want to use a hashing algorithm
other than what you instantiated the constructor to use, and the optional
second argument to the `$CW->password_needs_rehash()` wrapper when you want to
specify comparison against the defaults for an algorithm other than what you
instantiated the constructor to use.


Hash Validation Algorithms
--------------------------

In addition to the above hashing algorithms and all hashes created with the
core `crypt()` function, this class is able to verify hashes created with:

* __phpass__ -- A userland algorithm that uses both a salt and a configured
  number of MD5 rounds. Both the `P` identifier used by phpass itself and the
  `H` identifier used by BBCode 3 are supported.
* __Drupal 7__ -- A Drupal custom variable of `phpass` that usually uses SHA-512
  instead of MD5 for the hashing algorithm, though other algorithms may be used.
* __reference crypt__ -- hashes created using the PECL scrypt module reference
  `scrypt.php` class.
* __unsalted single round__ -- hashes created using an unsalted single round of
  *  __MD5__
  *  __SHA1__
  *  __SHA-256__
  *  __SHA-384__
  *  __SHA-512__
  *  __RipeMD-128__
  *  __RipeMD-160__
  *  __RipeMD-256__
  *  __RipeMD-320__

For the raw hashes, both hex and base64 encoded are supported, the latter
requiring the same alphabet as the `base64_encode()` function.


The `$options` Array
--------------------

The optional third argument to core `password_hash()` and the core
`password_needs_rehash()` is an array of hashing options.

### Bcrypt and pipfrosch-bcrypt

Those hashing algorithms only take a single argument, the `cost`:

    $options = array(
        'cost => N
    );

Where `N` is an integer. For `bcrypt` the integer must be between 4 and 31
inclusive. For pipfrosch-bcrypt `N` must be between 12 and 31 inclusive. If you
increment the cost by 1, it doubles the computational difficulty.

The CoreWrapper class will use safe defaults that are a function of time if you
do not specify the `$options` array.

### Argon2i and Argon2id and pipfrosch-scrypt

Those hashing algorithms take three arguments. You do not need to specify all
of them to specify some of them:

    $options = array(
        'memory_cost' => M,
        'time_cost'   => C,
        'threads'     => T
    );

Where `M` is the number of [kibibytes](https://en.wikipedia.org/wiki/Kibibyte)
that can be used during hashing, `C` is the CPU difficulty (incrementing by 1
doubles it), and `T` is the number of threads.

The conventional wisdom I hear for setting those, set `T` to 1.5 times the
number of threads you have for your CPU, M to the maximum amount of memory you
are willing to dedicate to password hashing, and C to just low enough that it
does not bother users.

If you feel so inclined to tune the options to your specific server hardware
and server load, by all means feel free to do so, but keep in mind that you
will need to re-tune then every time your typical server load and/or hardware
changes. That conventional wisdom is not
[KISS](https://en.wikipedia.org/wiki/KISS_principle) (Keep it Short and Simple).

To make setting those parameters KISS, the PasswordHandler class implements
what I call the `$StrengthFactor` when calling the constructor.


The Strength Factor
-------------------

When instantiating the constructor, the second optional argument is the
`$StrengthFactor` -- an integer between 0 and 2 inclusive. The default value is
`0`.

* `0` -- Paranoid level of security.
* `1` -- Really Paranoid level of security.
* `2` -- Black Helicopters, the NSA may try to crack my passwords security

For most servers, a level of `0` provides adequate hashing security to your
passwords.

If you can use a level of `1` without it being too much of a drag on your server
and without being too inconvenient for your users, use it.

If the information protected by the passwords could cause serious damage to
your users if a system cracker gained access to it, then use a level of `2` and
implement 2FA and upgrade your hardware if it is not fast enough.

What the Strength Factor does is influence the default options that are used,
so that you do not need to specify an `$options` array to get stronger
settings.

If any cypherpunks out there think my defaults at a Strength Factor of `0` are
too low, PLEASE feel free to contact me. Same if my defaults at a Strength
Factor of 2 are not high enough.

The source to CoreWrapper shows how the defaults are calculated, using the
calendar year as a factor.


The `password_get_info` and `password_needs_rehash` Wrappers
------------------------------------------------------------

These functions are provided by the `CoreWrapper` class completely with
userland code that uses the same options as the core functions.

With `password_get_info()` the core function only provides meaningful
information to hashes that can be created with the core function
`password_hash()`.

With my userland implementation, for many hash formats not created by the
`password_hash()` function it will at least provide the name of the hash format
that it detects. The `algo` number is reported as `0` for hashes that can not
be generated with my wrapper of `password_hash()` and the `$options` array will
be empty, but at least the name of the detected algorithm will be there.

With my userland implementation of `password_needs_rehash()` it will always
return true if the hash format is one that can not be created with either the
core `password_hash()` or with my wrapper to `password_hash()`, but for hashes
that *can* be created with my wrapper to `password_hash()` it compares the
current options to the defaults within my class rather than the PHP core
defaults. It also supports the two hash formats that I have added.


The `password_verify()` wrapper
-------------------------------

In PHP Core, this function is only able to validate password hashes in the
formats created by the `crypt()` function or with the `password_hash()`
function.

The wrapper in this class also supports the hash formats created by:

* phpass / BBCode 3
* Drupal 7
* raw (single round no salt) MD5
* raw (single round no salt) SHA-1
* raw (single round no salt) SHA-2
* raw (single round no salt) RipeMD
* pipfrosch-scrypt (w/ PECL scrypt extension)
* pipfrosch-bcrypt

If your build of PHP is not built against `libargon2` but you do have the
sodium extension installed (either as a PECL module or as a core module in
PHP 7.2+) this wrapper will validate Argon2i and Argon2id password hashes
as well.

When the `$hash` is directly supported by PHP core `password_verify()` this
wrapper passes the `$password` and `$hash` to the PHP core function and
returns the result. It polyfills when the core function does not support
validation of the `$hash`.


The `password_hash()` wrapper
-----------------------------

In PHP Core, this function is always able to generate bcrypt hashes. As of
PHP 7.2, if your PHP was compiled against libargon2 with the compile time
switch

    --with-password-argon2

Then the `password_hash()` function will also be able to create Argon2I hashes.
As of PHP 7.3, that same switch also adds support for Argon2ID.

In PHP 7.4, even if your PHP is not compiled with that switch, if you have the
sodium extension installed it will also support both Argon2I and Argon2ID. The
developers have chosen not to support Argon2D as it is not very well suited for
the needs of web applications, and I agree with that assessment, it is too
vulnerable to side-channel attacks when there is not physical security to the
proximity of the hardware.

This wrapper provides Argon2i and Argon2id support for PHP 7.0, 7.1, 7.2, and
7.3 when they are not compiled against libargon2. Compiling against libargon2
is preferable but that is not always an option.

This wrapper also provides support for my custom pipfrosch-scrypt hash format
(assuming you have PECL scrypt module installed) as well as my custome
pipfrosch-bcrypt hash format.

### Specifying Hashing Algorithm

PHP always has the constant `PASSWORD_BCRYPT` defined to `1`. PHP also always
the constant `PASSWORD_DEFAULT` defined, and presently it is defined also
defined to `1`. `1` is the integer argument to indicate the bcrypt hashing
algorithm.

With PHP 7.2+ compiled against libargon2, the `PASSWORD_ARGON2I` constant
will be defined to `2`. With PHP 7.3+ compiled against libargon2, the
`PASSWORD_ARGON2ID` constant will be defined to `3`.

Just as with the core `password_hash()` function, you can specify which
algorithm you want by passing the appropriate integer as the second
argument to the $CW->password_hash() function (the first argument of
course being the password to hash).

Additionally, you can also specify your preferred algorithm by the integer
value as the first argument to the class constructor:

    $CW = new CoreWrapper(3);

or

    $CW = new CoreWrapper(PASSWORD_ARGON2ID);

That would result in Argon2id being the algorithm that is used when you call
the $CW->password_hash() wrapper without needing to specify it to the
wrapper function, though you can still specify a different algorithm to the
wrapper if you want to.

For my custom `pipfrosch-scrypt` format, use the integer `51`. For my custom
`pipfrosch-bcrypt` format, use the integer `52`.

I chose such a high integer because it will likely be decades before an
official hashing algorithm is assigned that integer, if ever.


The Constructor
===============

The constrctor takes three optional arguments:

* `int $passwordDefault` An integer to define the hash algorithm to use with the
  `$CW->password_hash()` function when one is not specified to that function.
  It also specifies the algorithm that `password_needs_rehash()` will compare
  against by default. The algorithm integers are described [above](#hashing-algorithms)
* `int $strengthFactor -- An integer between 0 and 2 inclusive specifying how
  strong you want the defaults. The strength factor integers are described
  [above](#the-strength-factor)
* `bool $unitTesting` -- When set to true, Argon2i and Argon2id are provided by
  sodium extension instead of built-in. Only useful for unit testing. If your
  PHP is built against libargon2, do not set this to `true` on production
  servers. If your PHP is not built against libargon2, this option is
  meaningless anyway.


Example Usage
-------------
  
The code:
  
    $CW = new CoreWrapper();
  
As no arguments are called, it will use bcrypt by default with the class `0`
strength factor defaults.

The code:

    $CW = new CoreWrapper(3, 2);

It will use Argon2id by default with the class `2` strength factor defaults.

The code:

    $CW = new CoreWrapper(51, 1);

It will use pipfrosch-scypt with the class `1` strength factor defaults.