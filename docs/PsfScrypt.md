PSF Scrypt Password Hasher / Validator
======================================

The intent is to use this from within the CoreWrapper class by specifying the
integer `51` as the first argument when instantiating the CoreWrapper class.

This class requires the [PECL scrypt](https://pecl.php.net/package/scrypt)
module. It benefits from [PHP Sodium](https://www.php.net/manual/en/book.sodium.php)
extension though that extension is not strictly required. The benefit from
that extension is the `sodium_memzero()` function, which if available, is
used by this class to erase the contents of the `$password` variable when
it is no longer needed.

This class generates a password hash string using what is known as the
[PHC String Format](https://github.com/P-H-C/phc-string-format/blob/master/phc-sf-spec.md)
(PSF). As far as I know, PSF is not on track to become an IETF standard, but
it should be seen as the logical successor to the
[Modular Crypt Format](https://passlib.readthedocs.io/en/stable/modular_crypt_format.html)
(MCF) that many of us have become intimately familiar with.

I could not find an existing implementation of scrypt that used PSF (or even
MCF for that matter) that I could actually get to actually work, so I took the liberty of
creating a PSF implementation for this class using the `<id>` string `psf-scrypt`. This is
what a typical scrypt hash looks like in my implementation:

    $psf-scrypt$m=24,t=4,p=2$0kWGLtBu2BAGH7PdCm6k/w$QcjbbHETBZAmj/PI+q67tIm3foyH0JodcqmSdKBP9CdKb0w5QC4d9OhMIeIwc9K0

The above broken into it's components:

* `psf-scrypt` — That is the `<id>` string, identifying the hash algorithm
  used.
* `m=24,t=4,p=2` — That is the `<parameters>` string, containing the computational
  cost parameters needed to regenerate the identical hash from the same
  password and salt:
  * `m=24` indicated a memory cost of 24 megabytes (24576 kikibytes)
  * `t=4` indicates a time cost of 4 (2^4 = 16 CPU cost factor)
  * `p=2` indicates the level of parallelism used (2 threads)
* `0kWGLtBu2BAGH7PdCm6k/w` — That is the `<salt>` string. This class requires
  the salt be a minimum of 12 bytes that are base64 encoded. The default in this
  class is 16 bytes but the class will validate passwords against a hash that
  only used 12 bytes or as large as 24 bytes.
* `QcjbbHET[[SNIP]]IeIwc9K0` — That is the actual hash of the password. While
  scrypt itself does not specify how the hash is to be encoded, with
  `psf-scrypt` I am specifying base64 encoding. I am also specifying a minimum
  hash length of 32 bytes (256 bits) and a maximum of 64 bytes (512 bits). The
  class default is 48 bytes (384 bits).

Please note that when present, the `=` padding on the right side of a base64
encoded string should be removed. Also note that with PSF (and MCF) the `$`
separator is present at the beginning of a field, thus the hash which is the
last field is not followed by a `$`.


Password Hashing Note
---------------------

This implementation hashes the password once with SHA-384 (and the salt) before
sending the password to the Scrypt function. This is done so that regardless of
what characters are in the password, it is only printable 7-bit ASCII when sent
to the `scrypt()` function for hashing, avoiding any possible bugs that might
exist such as were found in the many different blowfish implementations.

The Scrypt implementation here is thus not compatible with other Scrypt
implementations but that is okay because it has a unique identifier so it is
clear that hashes made with this class will not validate passwords when using
a different Scrypt implementation.

It does however create a sort-of lock-in, you will need to use this class to
validate passwords against the hashes.


Using the PsfScrypt Class
-------------------------

This class only contains static methods so it should not be instantiated. There
is no class constructor.

Make sure the class file is where your [PSR-4](https://www.php-fig.org/psr/psr-4/)
autoloader can find it, or explicitly require the class file in your PHP
script, then:

    use \Pipfrosch\PasswordHandler\PsfScrypt as PsfScrypt;

The class has two public methods, one for generating the `psf-scrypt` hash
string and one for validating a password against a `psf-scrypt` hash string.


Create a PSF Scrypt hash string
-------------------------------

To create a PSF hash string:

    $hashString = PsfScrypt::passwordHasher(string $password[, int $strengthFactor[, array $options]]): string

The variable `$hashString` will now contain a string in PSF format containing
the default parameters, a generated salt unique to the hash, and the resulting
hash. The options `$strengthFactor` and `$options` are described in the
CoreWrapper documentation.

That static method takes the following arguments:

### `string $password` — Required

The password to be hashed. No validation is done as to the quality of the
password, that is beyond the scope of this class.

### `int $strengthFactor` — Optional

An integer value between `0` and `2` inclusive. Used to allow the class to
determine defaults used for the scrypt hashing parameters. For a more
detailed explanation of this parameter, see the CoreWrapper documentation.

### `array $options` — Optional

Used to specify the memory cost, CPU cost, and parallelism. For a more
detailed explanation, see the CoreWrapper documentation. Please note that
specifying options with the `$options` array will override defaults set as
a result of the `$strengthFactor` option.

### HASH LENGTH NOTE

If you do not want a 48 byte hash, you can add an option `hashLength` to the
`$options` array to specify a different length. It must be an integer between
32 and 64 inclusive.

### `passwordHasher()` \InvalidArgumentException Note

This method will under certain circumstances throw an `\InvalidArgumentException`:

* If the PECL scrypt module is not available.
* If one of the cost parameters is too large.

Using the default parameters, the cost parameters will never be too large.
However there is a possibility that the needed scrypt module is not loaded.
It would be a good idea to make sure your code handles exceptions.

Throwing an `\InvalidArgumentException` may not technically be the right thing
to do when the needed PECL module is not loaded, I need to look into that.


Validate a Password Against a PSF Scrypt hash string
----------------------------------------------------

To validate a password against a PSF Scrypt hash string:

    $test = PsfScrypt::passwordVerify($password, $hashString);

The variable `$test` will be a boolean `true` if the password validates and a
boolean `false` otherwise.

The static method takes two arguments, both are requires:

### `string $password` — Required

The password you wish to verify against the hash.

### `string $hashString` — Required

The PSF formatted hash string containing the parameters, salt, and password
hash.

### `passwordVerify()` \InvalidArgumentException Note

This method will under certain circumstances throw an `\InvalidArgumentException`:

* If the PECL scrypt module is not available.
* If the `$hashString` is not for a `psf-scrypt` hash.
* If the `$hashString` is not complete.
* If the specified parameters are too large.
* If the specified salt is not base64 encoded and at least 12 bytes or is
  larger than 24 bytes.
* If the hash component is not base64 encoded and at least 32 bytes or is
  larger than 64 bytes.

It would be a good idea to make sure your code handles exceptions.

Throwing an `\InvalidArgumentException` may not technically be the right thing
to do when the needed PECL module is not loaded, I need to look into that.

