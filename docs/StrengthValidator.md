The StrengthValidator Class
===========================

Quality of this class is Alpha. It will go through changes.

This class allows for checking the quality of a user-submitted password.

It does not return a quality, but rather, either the password passes the checks
or it does not. Quantifying the quality of a password can be misleading in my
opinion.

The class does several different checks on a password, including length and the
use of the `/usr/bin/pwscore` utility amongst others.

The class has a blacklist for pop culture references, right now that list is
too small to be practical. It will be split into a separate file and expanded.

If the [Pspell](https://www.php.net/manual/en/book.pspell.php) module is
available, the class will check the password against that pspell dictionary.
At this time the class does not allow specification of a custom pspell
dictionary, that needs to be done.

Long term, the plan to create a PECL module for the `pwscore` utility so that
the `exec()` function is only needed for systems that do not have that module.
I do not like using `exec()` in PHP code. Work on that binary module has not
yet begun, and use of `exec()` will remain as an option.

When instantiating the class, there is a single boolean option - whether or not
the password is for an admin account. When set to `true` then the class is a
little stricter with the required `pwscore` value.


    use \Pipfrosch\PasswordHandler\StrengthValidator as StrengthValidator;
    $SV = new StrengthValidator(false); // or true for admin accounts


public function addToBlacklist($input): void
--------------------------------------------

Adds a phrase (string) or a list of phrases (array) to the blacklist. That
function will likely be nuked when I move the black list into its own file.


public function validatePasswordStrength(string $password, string $username = '')
---------------------------------------------------------------------------------

Validates the strength of a password. Optional username may be specified as the
second argument, which is of benefit to the `pwscore` utility.

It returns a boolean `true` if the password appears to be strong enough, or an
array of one or more error messages indicating issues found with the password
that cause it to be rejected.


public function returnHashIfPasswordStrong(string $password, string $username = '', int $algo = 1, int $paranoiaLevel = 0)
--------------------------------------------------------------------------------------------------------------------------

If the password is strong enough, it returns a string hash of the password that
can be inserted into the database.

The optional third argument lets you specify the integer corresponding with the
hashing algorithm (defaults to `1` which corresponds with bcrypt) and also lets
you specify the level of paranoia you wish to use when generating the hash:

* __0__  -- Standard paranoia.
* __1__  -- Black helicopter paranoia.
* __2__  -- The NSA is out to get me paranoia.

If this function returns a hash, the password passed the quality check. If it
returns an array, the array contains one or more reasons why the password was
rejected.